# 0. Libraries
# --------------------------------------------------------------------------------------
from ast import arguments
from cgi import print_arguments
from black import out
import pandas as pd
import numpy as np
from tqdm import tqdm
# from pyrsistent import T

from source.engine.FTP_engine import FTPModel as FTP
from source.engine import utils as ut
from datetime import datetime

from sklearn.utils.extmath import cartesian

# %load_ext Cython

# import psutil, os
# p = psutil.Process(os.getpid())

# p.set_nice(10)


# 1. DATA INPUT
# --------------------------------------------------------------------------------------
path = 'datasets/'

# 1.1. Data de embarcaciones (descargas)
file = 'df_embar.xlsx'
df_embar = pd.read_excel(path + file, converters={'Matricula':str})

# 1.2. Data de prioridad de embarcaciones
file = 'df_prio_embar.xlsx'
df_prio_embar = pd.read_excel(path + file, converters={'Matricula':str})

# 1.3. Data de prioridad de destinos
file = 'df_prio_dest.csv'
df_prio_dest = pd.read_csv(path + file, sep=';')

# 1.4. Data de capacidad de planta
file = 'df_cap_plant_all.csv'
df_cap_plant = pd.read_csv(path + file, sep=';')

# TODO: Delete this
# df_cap_plant.loc[df_cap_plant['Planta'] == 'Malabrigo', ['2018-I', '2018-II', '2019-I', '2019-II', '2020-I', '2020-II', '2021-I', '2021-II']] = df_cap_plant.loc[df_cap_plant['Planta'] == 'Malabrigo', ['2018-I', '2018-II', '2019-I', '2019-II', '2020-I', '2020-II', '2021-I', '2021-II']] * 1.115
# df_cap_plant.loc[df_cap_plant['Planta'] == 'Chimbote', ['2018-I', '2018-II', '2019-I', '2019-II', '2020-I', '2020-II', '2021-I', '2021-II']] = df_cap_plant.loc[df_cap_plant['Planta'] == 'Chimbote', ['2018-I', '2018-II', '2019-I', '2019-II', '2020-I', '2020-II', '2021-I', '2021-II']] * 1.123 * 1.15384615
# df_cap_plant.loc[df_cap_plant['Planta'] == 'Vegueta', ['2018-I', '2018-II', '2019-I', '2019-II', '2020-I', '2020-II', '2021-I', '2021-II']] = df_cap_plant.loc[df_cap_plant['Planta'] == 'Vegueta', ['2018-I', '2018-II', '2019-I', '2019-II', '2020-I', '2020-II', '2021-I', '2021-II']] * 1.17

# 1.5. Data de máximo de días con capacidad máxima
file = 'df_max_dias_pico.csv'
df_max_dias_pico = pd.read_csv(path + file, sep=';')
df_max_dias_pico.fillna(0, inplace=True)

# 1.6. Data de porcentaje de velocidad en dia de descanso
file = 'df_porc_veloc_descanso.csv'
df_porc_veloc_descanso = pd.read_csv(path + file, sep=';')
df_porc_veloc_descanso.fillna(0, inplace=True)

# 1.7. Registro de carenas
file = 'df_carenas.xlsx'
df_carenas = pd.read_excel(path + file, converters={'Matricula':str})

# 1.8. Mal tiempo
file = 'df_cierre_puertos.csv'
df_mal_tiempo = pd.read_csv(path + file, sep=';')
df_mal_tiempo = df_mal_tiempo[df_mal_tiempo['TIEMPO CERRADO (horas)'] > 24]
df_mal_tiempo['CIERRE'] = pd.to_datetime(df_mal_tiempo['CIERRE'])

# 1.9. Descarga Competencia
file = 'df_descarga_competencia.xlsx'
df_descarga_competencia = pd.read_excel(path + file, converters={'Matricula':str})

# 1.10. Cbod competencia
file = 'df_cbod_competencia.xlsx'
df_cbod_competencia = pd.read_excel(path + file, converters={'Matricula':str})

# 2. DATA PARA ESCENARIOS
# --------------------------------------------------------------------------------------
path = "datasets/escenarios/"

# 2.1. Listado de Embarcaciones a Omitir
file = "df_emb_omitir_second_comb.xlsx"
df_emb_omitir = pd.read_excel(path + file, converters={"Matricula": str})

# 2.2. Listado de Embarcaciones para conversión a Frío
file = "df_conversion_frio.xlsx"
df_conversion_frio = pd.read_excel(path + file, converters={"Matricula": str})

# 2.3. TASA 45
file = "df_tasa45.xlsx"
df_tasa45 = pd.read_excel(path + file, converters={"Matricula": str})


# 3. DATA VARIABLE
# --------------------------------------------------------------------------------------

# 3.1. Número de EPS (Flota TASA) --> 48, 47, ..., 42
file = "df_num_embar.csv"
df_num_embar = pd.read_csv(path + file, sep=";")

# 3.2. Número de EPS con Frío (float con frío) --> 19, 20, 21, ..., 23
file = "df_num_eps_frio.csv"
df_num_eps_frio = pd.read_csv(path + file, sep=";")

# 3.3. Flag TASA 45
FLG_45 = [0]

# 3.4. Escenario Estrategia de Terceros (A, B, ..., F)
file = "df_scenario_estrategia_terceros.csv"
df_scenario_estrategia_terceros = pd.read_csv(path + file, sep=";")

# 3.5. Capacidades para probar (por planta)
path = "money/"
file = "df_new_capacidades.xlsx"
df_scenario_cap = pd.read_excel(path + file)

# 4. TABLA PARA CONVERSION A DINERO
# --------------------------------------------------------------------------------------

path = 'money/'
file = 'input_planta.xlsx'

df_money = pd.read_excel(path + file, converters={"Matricula": str})
df_money['Costo Extraccion Unitario'] = 0
# file = 'df_capex_embar.xlsx'
file = 'df_costos_totales_eps.xlsx'
df_costos_embar = pd.read_excel(path + file, converters={"Matricula": str})

file = 'df_cuota_nacional.xlsx'

df_cuota_nacional = pd.read_excel(path + file)

file = 'df_capex_plantas.xlsx'

df_capex_plantas = pd.read_excel(path + file)

file = 'df_capex_frio.xlsx'

df_capex_frio = pd.read_excel(path + file, converters={"Matricula": str})
# df_capex_frio["CAPEX FRIO"] = 0
# df_capex_frio['CAPEX FRIO'] = 0 # TODO: Delete
# file = 'df_resto_tasa.xlsx'
file = 'df_resto_tasa_actualizado.xlsx'
df_resto_tasa = pd.read_excel(path + file, converters={"Matricula": str})

file = 'df_costo_extraccion.csv'
df_costo_extraccion = pd.read_csv("datasets/df_costo_extraccion.csv", sep=';')


df_master_eps = pd.read_excel("datasets/master_eps.xlsx", converters={"Matricula": str})

path = 'outputs/'
file = 'df_ultimo_dia_temporadas.xlsx'
df_ultimo_dia_temporadas = pd.read_excel(path + file)
# 5. TABLA CON TODOS LOS ESCENARIOS
# --------------------------------------------------------------------------------------


lst_vel_malabrigo = (
    df_scenario_cap.loc[df_scenario_cap["Malabrigo"].notna(), "Malabrigo"]
    .unique()
    .tolist()
)
lst_vel_chimbote = (
    df_scenario_cap.loc[df_scenario_cap["Chimbote"].notna(), "Chimbote"]
    .unique()
    .tolist()
)
lst_vel_samanco = (
    df_scenario_cap.loc[df_scenario_cap["Samanco"].notna(), "Samanco"].unique().tolist()
)
lst_vel_supe = (
    df_scenario_cap.loc[df_scenario_cap["Supe"].notna(), "Supe"].unique().tolist()
)
lst_vel_vegueta = (
    df_scenario_cap.loc[df_scenario_cap["Vegueta"].notna(), "Vegueta"].unique().tolist()
)
lst_vel_callao = (
    df_scenario_cap.loc[df_scenario_cap["Callao"].notna(), "Callao"].unique().tolist()
)
lst_vel_pisco_s = (
    df_scenario_cap.loc[df_scenario_cap["Pisco S"].notna(), "Pisco S"].unique().tolist()
)

df_all_vel = pd.DataFrame(
    cartesian(
        (
            lst_vel_malabrigo,
            lst_vel_chimbote,
            lst_vel_samanco,
            lst_vel_supe,
            lst_vel_vegueta,
            lst_vel_callao,
            lst_vel_pisco_s,
        )
    ),
    columns=[
        "cap_malabrigo",
        "cap_chimbote",
        "cap_samanco",
        "cap_supe",
        "cap_vegueta",
        "cap_callao",
        "cap_pisco_s",
    ],
)

df_scenarios = df_all_vel.copy()

lst_estrat_terce = (
    df_scenario_estrategia_terceros["Estrategia Terceros"].unique().tolist()
)

# Estrategia Terceros
df_scenarios = pd.concat(
        [df_scenarios] * (len(lst_estrat_terce)), ignore_index=True
    )
df_scenarios["estrategia"] = np.repeat(
        lst_estrat_terce, len(df_scenarios.index) / len(lst_estrat_terce)
    )

# Cantidad de embarcaciones de TASA
df_scenarios = pd.concat([df_scenarios] * (len(df_num_embar.index)), ignore_index=True)
df_scenarios["num_embar"] = np.repeat(df_num_embar['Cantidad'].to_numpy(), len(df_scenarios.index) / len(df_num_embar.index))

# Embarcaciones para conversión a frío
df_scenarios = pd.concat([df_scenarios] * (len(df_num_eps_frio.index)), ignore_index=True)
df_scenarios["num_eps_frio"] = np.repeat(
    df_num_eps_frio['Cantidad'].to_numpy(), len(df_scenarios.index) / len(df_num_eps_frio.index)
)

# TASA 45
df_scenarios = pd.concat([df_scenarios] * (len(FLG_45)), ignore_index=True)
df_scenarios["flg_45"] = np.repeat(
    FLG_45, len(df_scenarios.index) / len(FLG_45)
)

tqdm.pandas(desc=r'% de avance')
# ddf_scenarios = dask.dataframe.from_pandas(df_scenarios.head(n=10), npartitions=10)
# df_scenarios = df_scenarios.head(n=3)

# 6. INSTANCIA DE LA CLASE FTP Y PREDICT
# --------------------------------------------------------------------------------------

# ftp_model = FTP()
# ftp_model.predict(df_embar, df_prio_embar, df_prio_dest, df_cap_plant, df_carenas, df_mal_tiempo, df_max_dias_pico, df_porc_veloc_descanso, df_descarga_competencia, df_cbod_competencia, df_emb_omitir, df_conversion_frio, df_tasa45, df_money)



# 7. RUNNING
# --------------------------------------------------------------------------------------

# if __name__ == '__main__':
#     manager = multiprocessing.Manager()

#     start_time = datetime.now()
#     output = ddf_scenarios.map_partitions(lambda part: part.apply(lambda x: ftp_model.run(x['num_embar'], x['num_eps_frio'], x['flg_45'], x['estrategia'], x['cap_malabrigo'], x['cap_chimbote'], x['cap_samanco'], x['cap_supe'], x['cap_vegueta'], x['cap_callao'], x['cap_pisco_s']), axis=1), meta=tuple).compute()
#     time_elapsed = datetime.now() - start_time

#     df_scenarios['ebit'] = [i for i in output]

#     df_scenarios.sort_values('ebit', inplace=True, ascending=False)

#     df_scenarios['ID'] = np.arange(0, len(df_scenarios.index), 1) + 1
#     print(output)
#     df_scenarios.to_csv('outputs/output_test_26_.csv', index=False)

#     print(f'Time elapsed (hh:mm:ss.ms) {time_elapsed}')
#     print("Proceso finalizado.")

# for i in range(len(output)):
#     output[i]['ID_ESCENARIO'] = i

# df_final = pd.concat([output[i] for i in range(len(output))])
# df_final.to_csv('outputs/df_final_1.csv')
# print(output)
# pd.DataFrame([i for i in output]).to_csv('outputs/output_test.csv', index=False)
# ---------------------------------------------

# df_vx = vx.from_pandas(df_scenarios)




# if __name__ == '__main__':
#     manager = multiprocessing.Manager()
#     output = df_vx.apply(ftp_model.run, arguments=[
#     df_vx.num_embar, 
#     df_vx.num_eps_frio, 
#     df_vx.flg_45, 
#     df_vx.estrategia,
#     df_vx.cap_malabrigo,
#     df_vx.cap_chimbote,
#     df_vx.cap_samanco,
#     df_vx.cap_supe,
#     df_vx.cap_vegueta,
#     df_vx.cap_callao,
#     df_vx.cap_pisco_s
#     ], vectorize=False)
#     output = output.evaluate()
#     print(output)

# ftp_model = FTP()
# ftp_model.predict(df_embar, df_prio_embar, df_prio_dest, df_cap_plant, df_carenas, df_mal_tiempo, df_max_dias_pico, df_porc_veloc_descanso, df_descarga_competencia, df_cbod_competencia, df_emb_omitir, df_conversion_frio, df_tasa45, df_money, df_costos_embar, df_cuota_nacional, df_capex_plantas, df_capex_frio)


# # ---------------------------------------------

# start_time = datetime.now()
# vect_ftp_model = np.vectorize(ftp_model.run)

# output = vect_ftp_model(
#             df_scenarios['num_embar'].to_numpy(), 
#             df_scenarios['num_eps_frio'].to_numpy(), 
#             df_scenarios['flg_45'].to_numpy(), 
#             df_scenarios['estrategia'].to_numpy(), 
#             df_scenarios['cap_malabrigo'].to_numpy(),
#             df_scenarios['cap_chimbote'].to_numpy(), 
#             df_scenarios['cap_samanco'].to_numpy(),
#             df_scenarios['cap_supe'].to_numpy(),
#             df_scenarios['cap_vegueta'].to_numpy(),
#             df_scenarios['cap_callao'].to_numpy(), 
#             df_scenarios['cap_pisco_s'].to_numpy())
# print(output)
# time_elapsed = datetime.now() - start_time

# print(f'Time elapsed (hh:mm:ss.ms) {time_elapsed}')
# print(output)
# pd.DataFrame([i for i in output]).to_csv('outputs/output_test_2.csv', index=False)

# print("Proceso finalizado.")
# df_final = pd.concat([output[i] for i in range(len(output))])
# df_final.to_csv('outputs/df_final_2.csv')

# ---------------------------------------------------

## TODO: Pandas apply
start_time = datetime.now()
ftp_model = FTP()
ftp_model.predict(df_embar, df_prio_embar, df_prio_dest, df_cap_plant, df_carenas, df_mal_tiempo, df_max_dias_pico, df_porc_veloc_descanso, df_descarga_competencia, df_cbod_competencia, df_emb_omitir, df_conversion_frio, df_tasa45, df_money, df_costos_embar, df_cuota_nacional, df_capex_plantas, df_capex_frio, df_resto_tasa, df_costo_extraccion, df_master_eps, df_ultimo_dia_temporadas)

ftp_model.to_pickle('models/FTP_MODEL_VERSION_13.pkl')

# ftp_model = ut.read_pickle('models/FTPModel.pkl')


# ftp_model_run = np.vectorize(ftp_model.run)

# kpis = df_scenarios.progress_apply(lambda x: ftp_model.run(x['num_embar'], x['num_eps_frio'], x['flg_45'], x['estrategia'], x['cap_malabrigo'], x['cap_chimbote'], x['cap_samanco'], x['cap_supe'], x['cap_vegueta'], x['cap_callao'], x['cap_pisco_s']), axis=1)

time_elapsed = datetime.now() - start_time
print(f'Time elapsed (hh:mm:ss.ms) {time_elapsed}')

# tuples_kpis = [i for i in kpis]
# df_kpis = pd.DataFrame(tuples_kpis, columns=['ingresos', 'mtto_omision', 'ebit_pv', 'vr_pv', 'vr_year4', 'produccion_hp', 'produccion_ap', 'CF', 'CV', 'costos_mp_extrac', 'costos_mp_terc', 'mtto_frio', 'npv', 'capex_total', 'gastos_cierre', 'ebit_prom', 'market_share', 'cumpli_cuota_propia', 'mp_descargado_propios', 'mp_descargado_terceros'])

# df_kpis.index = df_scenarios.index
# df_scenarios = pd.merge(df_scenarios, df_kpis, left_index=True, right_index=True)

# df_scenarios.sort_values('npv', inplace=True, ascending=False)

# df_scenarios['ID_ESCENARIO'] = np.arange(0, len(df_scenarios.index), 1) + 1
# print(df_kpis)
# df_scenarios.to_excel('outputs/Resumen_Escenarios_New_lodos.xlsx')

# TODO:
# print(f'Time elapsed (hh:mm:ss.ms) {time_elapsed}')
# print("Proceso finalizado.")
# df_final = pd.concat([output[i] for i in range(len(output))])
# df_final.to_csv('outputs/df_final_2.csv')



# if __name__ == "__main__":  # confirms that the code is under main function
#     from pandarallel import pandarallel
#     # import pandas as pd
#     # import numpy as np

#     # from source.engine.FTP_engine import FTPModel as FTP
#     # from datetime import datetime

#     # from sklearn.utils.extmath import cartesian
#     # import dask.dataframe
#     pandarallel.initialize()
#     start_time = datetime.now()
#     ftp_model = FTP()
#     ftp_model.predict(df_embar, df_prio_embar, df_prio_dest, df_cap_plant, df_carenas, df_mal_tiempo, df_max_dias_pico, df_porc_veloc_descanso, df_descarga_competencia, df_cbod_competencia, df_emb_omitir, df_conversion_frio, df_tasa45)

#     run = ftp_model.run
#     output = df_scenarios.parallel_apply(run, args=('num_embar', 'num_eps_frio', 'flg_45', 'estrategia', 'cap_malabrigo', 'cap_chimbote', 'cap_samanco', 'cap_supe', 'cap_vegueta', 'cap_callao', 'cap_pisco_s'), axis=1)

#     time_elapsed = datetime.now() - start_time
#     print(f'Time elapsed (hh:mm:ss.ms) {time_elapsed}')
#     print("Proceso finalizado.")
#     print(output)


## TODO: Iterrows
# ftp_model = FTP()
# ftp_model.predict(df_embar, df_prio_embar, df_prio_dest, df_cap_plant, df_carenas, df_mal_tiempo, df_max_dias_pico, df_porc_veloc_descanso, df_descarga_competencia, df_cbod_competencia, df_emb_omitir, df_conversion_frio, df_tasa45, df_money)
# start_time = datetime.now()
# output = []
# for index, row in df_scenarios.iterrows():
#     valor = ftp_model.run(row['num_embar'], row['num_eps_frio'], row['flg_45'], row['estrategia'], row['cap_malabrigo'], row['cap_chimbote'], row['cap_samanco'], row['cap_supe'], row['cap_vegueta'], row['cap_callao'], row['cap_pisco_s'])
#     output.append(valor)

# time_elapsed = datetime.now() - start_time
# print(f'Time elapsed (hh:mm:ss.ms) {time_elapsed}')
# print(output)

# pd.DataFrame(output).to_csv('outputs/test_iterrrows.csv')

## TODO: Dask Apply

# ftp_model = FTP()
# ftp_model.predict(df_embar, df_prio_embar, df_prio_dest, df_cap_plant, df_carenas, df_mal_tiempo, df_max_dias_pico, df_porc_veloc_descanso, df_descarga_competencia, df_cbod_competencia, df_emb_omitir, df_conversion_frio, df_tasa45, df_money)


# time_elapsed = datetime.now() - start_time
# print(f'Time elapsed (hh:mm:ss.ms) {time_elapsed}')
# print(output)

# pd.DataFrame(output).to_csv('outputs/dask_apply.csv')

## TODO: Comprenhension

# ftp_model = FTP()
# ftp_model.predict(df_embar, df_prio_embar, df_prio_dest, df_cap_plant, df_carenas, df_mal_tiempo, df_max_dias_pico, df_porc_veloc_descanso, df_descarga_competencia, df_cbod_competencia, df_emb_omitir, df_conversion_frio, df_tasa45, df_money)

# start_time = datetime.now()

# output = [ftp_model.run(num_embar, num_eps_frio, flg_45, estrategia, cap_malabrigo, cap_chimbote, cap_samanco, cap_supe, cap_vegueta, cap_callao, cap_pisco_s) for num_embar, num_eps_frio, flg_45, estrategia, cap_malabrigo, cap_chimbote, cap_samanco, cap_supe, cap_vegueta, cap_callao, cap_pisco_s in zip(df_scenarios['num_embar'], df_scenarios['num_eps_frio'], df_scenarios['flg_45'], df_scenarios['estrategia'], df_scenarios['cap_malabrigo'], df_scenarios['cap_chimbote'], df_scenarios['cap_samanco'], df_scenarios['cap_supe'], df_scenarios['cap_vegueta'], df_scenarios['cap_callao'], df_scenarios['cap_pisco_s'])]

# time_elapsed = datetime.now() - start_time
# print(f'Time elapsed (hh:mm:ss.ms) {time_elapsed}')
# print(output)
# pd.DataFrame(output).to_csv('outputs/comprension.csv')


## TODO: Itertuples

# ftp_model = FTP()
# ftp_model.predict(df_embar, df_prio_embar, df_prio_dest, df_cap_plant, df_carenas, df_mal_tiempo, df_max_dias_pico, df_porc_veloc_descanso, df_descarga_competencia, df_cbod_competencia, df_emb_omitir, df_conversion_frio, df_tasa45, df_money)
# start_time = datetime.now()
# output = []
# for row in df_scenarios.itertuples():
#     valor = ftp_model.run(row.num_embar, row.num_eps_frio, row.flg_45, row.estrategia, row.cap_malabrigo, row.cap_chimbote, row.cap_samanco, row.cap_supe, row.cap_vegueta, row.cap_callao, row.cap_pisco_s)
#     output.append(valor)

# time_elapsed = datetime.now() - start_time
# print(f'Time elapsed (hh:mm:ss.ms) {time_elapsed}')
# print(output)
# pd.DataFrame(output).to_csv('outputs/Itertuples.csv')


# TODO: Multiprocessing
# print("Multiprocessing")
# ftp_model = FTP()
# ftp_model.predict(df_embar, df_prio_embar, df_prio_dest, df_cap_plant, df_carenas, df_mal_tiempo, df_max_dias_pico, df_porc_veloc_descanso, df_descarga_competencia, df_cbod_competencia, df_emb_omitir, df_conversion_frio, df_tasa45, df_money)

# def multi_run_wrapper(args):
#    return ftp_model.run(*args)

# start_time = datetime.now()
# if __name__ == '__main__':
#     from multiprocessing import Pool
#     pool = Pool(8)
#     output = pool.map(multi_run_wrapper,list(df_scenarios[['num_embar', 'num_eps_frio', 'flg_45', 'estrategia', 'cap_malabrigo', 'cap_chimbote', 'cap_samanco', 'cap_supe', 'cap_vegueta', 'cap_callao', 'cap_pisco_s']].to_records(index=False)))
#     print(output)
#     time_elapsed = datetime.now() - start_time
#     print(f'Time elapsed (hh:mm:ss.ms) {time_elapsed}')
