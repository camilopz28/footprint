# ***Propuesta Footprint***

*Lenguaje a utilizar:*
- Python

*Metodología*

1. Se plantea apoyarse de las lógicas del proyecto footprint anterior, pero incluyendo los nuevos requerimientos.
2. La solución consta de una herramienta que permita hacer simulaciones de manera individual y/o conjunta de la forma más automática posible.

*Ouput*

> Generación de una nueva columna a la data que indique la planta de retorno.


## ***Preguntas Footprint***

1. ¿De qué manera necesitan recibir el output?
2. ¿Los aprox 400 escenarios nacen de la combinación de la flexibilidad en: Plantas x Embarcaciones + Capacidad?
3. ¿Cuál será el tiempo de actualización de las prioridades de embarcaciones? 
4. Explicar acerca de la base de datos de las embarcaciones

Notas sueltas:

* Distribución de barcos y en que planta

* ¿Cuantas toneladas se descargó en una planta?

* Entregable: Simulador

* La lógica siempre es la misma:

* Opción1 luego opción 2, luego opción 3

* En orden cronológico es día por día

* Capacidad disponible de la banca

# ***Data para completar***

![Destino](data_a_completar.PNG)

***Datos:***

 1. El identificador de la embarcación
 2. La cantidad de materia prima descargada.
 3. La fecha de arribo.
 4. La zona de la primera cala.

***Objetivo:*** Colocar la planta de retorno de acuerdo a un conjunto de parámetros.

# ***Prioridad de destinos***

![Prioridad de destinos](destinos_prioridad.PNG)

De acuerdo a que si el barco es con frío/ sin frío o de terceros, va a tener un orden de prioridad en base a la zona de su primera cala.

Además, se tiene un orden de prioridad para cada barco dependiendo a la categoría Frío/sin Frío/Tercero, etc.

# ***Características del destino***

![Características de destino](car_destino.PNG)

> Datos: Máximo días seguidos, Horas de descanso

* Cada planta va a una velocidad diferente (con esto se calcula la capacidad máxima)
* Se regoge la cantidad de materia prima descargada 
* Flag si la materia prima es mayor o igual al 90%
* Definir si el contador que se inicializará después se reseteará en ese día.
* Identificar si el contador va a incrementarse
* Cálculo del contador
* Cálculo de la capacidad máxima para el día siguiente de operación en función del contador.

Contador es al final del día

![Prioridad de destinos](orden_barcos.PNG)

# ***Nomenclatura***

![Nomenclatura](nomenclatura.PNG)

* Primeros 2 dígitos: Número de barcos funcionando.
* El siguiente dígito representa el número de plantas.
* El valor de 1 significa que está cerrado.
* Sin KAPEX: Tal cual se está ahora
* Con KAPEX: Ejm: Comprar una cocina, Pasar de 4 a 5 líneas de producción, Repotenciar Malabrigo, Chimbote, Vegueta.

Nota:

> KAPEX: Inversión en capital

Nuevas Preguntas:

* ¿El tiempo de esperar del barco interviene en algún momento? ¿Importa?


* Barco por barco, día por día

# ***Procedimiento***

1. ¿Cual es el primer barco del día? Se apoya con el orden de prioridad de los barcos.
2. ¿Qué tipo de barco es?
3. ¿A dónde se puede mover ese tipo de barco según su primera cala?
4. ¿Su planta de primera prioridad puede recibirlo? Si -> Descargo, No- > Siguiente
5. Actualizar capacidad disponible de las plantas (Capacidad máxima del día - Total descarga hasta el momento)
6. Tomar el siguiente barco y repetir el proceso hasta terminar todos los barcos del día.
7. Revisar la capacidad máxima de plantas para el día siguiente.
8. Comenzar con el primer barco del día siguiente.

Nota:
> Si en la planta no descarga nada el contador se va a cero 
> Solo Centro Norte

* Planta entra en Loop: No puede terminar su proceso.

## ***Subtareas:***

1. Elegir el destino del barco.
2. Actualizar capacidad.
3. Completar la cuota de pesca.

Notas:

> Pesca en Bolsa

> Tener en cuenta la cuota de los barcos y la cuota original.

> Qué pasa cuando se reduzca plantas y/o barcos?

> El propio puede esperar

> Los que se queden al final sean los barcos con Frío (CHD)

> Fecha: 3 de enero

> CHD: Consumo humano directo.

# ***Con x plantas menos***

1. No pasa nada, no hay impacto en la descarga
2. Impacto la descarga de terceros
3. Impacto en la descarga propia (lo define el orden de prioridad de los barcos)
4. Afecte la descarga de ambos (propios + terceros)

Replicar la decisión de que si no he terminado de pescar lo que pesqué, continúo pescando.

Al final de temporada:

1. Comparar descarga calculada por el modelo VS descarga real de la temporada. descarga calculada por el modelo <= descarga real de la temporada.
2. Según 1. Hacer subtarea de cómo capturar más pesca (aumentando el esfuerzo pesquero - más días de pesca).

3. Cuándo terminaron de pescar los barcos y hasta cuándo puedo pescar. Hacer que los barcos pesquen más en esos días.

## ***Pendientes***

* Validar con Legal

## ***Consideraciones***

1. Ir día por día

2. Suponer que todas las plantas arrancan con cero de descarga.

3. En vez de la hora voy por el orden de prioridad.

## ***Preguntas***

1. ¿La capacidad del 90% es independientemente de la planta?
2. ¿Es necesario el cálculo de *día suma al contador*?

## ***NEXT***

<!-- Enfocar como programación lineal. Maximizar el volumen descargado. -->

1. Para el gremio se reduce los supuestos a 80%

2. Si un barco no pezca en 2 años en consecutivo pierde su lincencia: 1 vez su capacidad de bodega 400.


MIO: 40 %
BLENDING: 25 %
FOOTPRINT: 25 %
CAPACIDADES: 10 %

Visto bueno de las premisas.

Comparar la zona de pesca con la zona de primera cala.

Validar con:
1. Lucho
2. Ernesto
3. Juancho


La matrícula, el numero entre los guiones

Barco corta distancia- Larga distancia

Ver el efecto del preservante.