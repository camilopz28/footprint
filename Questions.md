# ***Questions about Data***

1. En la columna *Zonas de pesca* de la data de descargas totales existen $54$ registros vacíos.
2. En la data de descargas totales existen *Zonas de pesca* existen 9 zonas de pesca, pero en las prioridades de destino solo se utilizan del 2 al 6. ¿Filtrar solo aquellos del 2 al 6, para embarcaciones propias?
3. Para terceros, cómo utilizar correctamente el ID de zona de pesca? Por ejemplo, uno que tiene zona de pesca 6 (Pisco) (En prioridades Pisco N, Pisco S).
   

## ***Acerca del subproceso 4***:

- ***Supuesto de eliminar un barco.***
- ***¿Para cada una de las temporadas?***

1. Idenfificar dias de fin de temporada para cada barco de tasa. ***(Check)***
2. Calcular y comparar subtotales al primer dia con barcos parqueados. ***(Subtotales al primer día)***
3. Calcular pesca pendiente. (***¿Pesca pendiente total?***)
4. Aumentar esfuerzo pesquero con barcos parqueados (Según rendimiento de barcos activos a ese dia) (***¿Barcos con mayor rendimiento? Rendimiento = Descarga del barco de toda la temporada / Capacidad de total de la temporada***).
5. Correr subtarea de asignar puertos (***Los barcos se ordenan de acuerdo a su rendimiento?***, ***¿Qué pasa con el orden de prioridad?***).
6. Correr subtarea de capacidad de plantas (Check).
7. Repetir para el dia sigueinte ().
   
## ***Consultas***

1. ¿ Se puede descargar parte del volumen?
2. Pisco Norte cerrar?


<!-- Velocidad antigua -->
<!-- 182
191
62
82
137
195
122
71 -->
