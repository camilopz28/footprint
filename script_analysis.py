import pandas as pd
import numpy as np

import plotly.express as px
import matplotlib.pyplot as plt
import seaborn as sns
import plotly.graph_objects as go

from source.engine.FTP_engine import FTPModel as FTP


# path = "outputs/"
# file = "output_sugerido_8.csv"

# df = pd.read_csv(path + file)

# name_fecha = 'fecha_produccion'
# df.loc[:, name_fecha] = pd.to_datetime(
#             df.loc[:, name_fecha], format="%Y/%m/%d"
#         )
# df['y-m'] = df[name_fecha].apply(lambda x: '{0}-{1}'.format(x.year, x.isocalendar()[1]))

# name_tempo = 'temporada'
# df.reset_index(inplace=True)
# df.rename(columns={'index':'id'}, inplace=True)

# df['rend'] = df['volumen'] / df['CBOD'] 
# # df['cant'] = 1


# # data = df[df['temporada'] == '2018-I']

# # fig = px.scatter(data, x="CBOD", y="volumen", animation_frame="y-m", animation_group="Embarc",
# #            size="cant", color="Zona de Pesca", #hover_name="country",
# #         #    log_x=True, 
# #            size_max=55, 
# #         #    range_x=[100,100000], range_y=[25,90]
# #            )
# dff = df.groupby(['Zona de Pesca', 'sort_tipo_embarc','y-m'], as_index=False).agg(
#     volumen_prom=('volumen', 'mean'),
#     volumen_tot=('volumen', 'sum'),
#     cantidad=('Embarc', 'count'),
#     rendimiento_prom=('rend', 'mean')
# )

# fig = px.scatter(dff, x="rendimiento_prom", y="volumen_prom", animation_frame="y-m", animation_group="Zona de Pesca",
#            size="cantidad", color="sort_tipo_embarc", #hover_name="country",
#         #    log_x=True, 
#            size_max=55, 
#         #    range_x=[100,100000], range_y=[25,90]
#            )

# fig.show()

path = "outputs/"
file = "output_sugerido_13.csv"

df = pd.read_csv(path + file)
df = df[df['temporada'] == '2018-I']
name_fecha = 'fecha_produccion'
df.loc[:, name_fecha] = pd.to_datetime(
            df.loc[:, name_fecha], format="%Y/%m/%d"
        )
df['y-m'] = df[name_fecha].apply(lambda x: '{0}-{1}'.format(x.year, x.isocalendar()[1]))

name_tempo = 'temporada'

df.reset_index(inplace=True)
df.rename(columns={'index':'id'}, inplace=True)

df['rend'] = df['volumen'] / df['CBOD'] 

cols = ['y-m', 'planta', 'Zona de Pesca']
df = df.groupby(cols, as_index=False, dropna=False)['volumen'].sum()


fig = px.bar(df, x="planta", y="volumen", color="planta",
  animation_frame="y-m", animation_group="Zona de Pesca", )
fig.show()