import pandas as pd
import numpy as np

from source.engine.FTP_engine import FTPModel as FTP
from sklearn.utils.extmath import cartesian
from source.engine.utils import yellow, green, red, log


path = "datasets/"

# -- 1. DATA INPUT
# -------------------------------------------------------------------

file = "df_embar.xlsx"
df_embar = pd.read_excel(path + file, converters={"Matricula": str})

# Data de prioridad de embarcaciones
file = "df_prio_embar.xlsx"
df_prio_embar = pd.read_excel(path + file, converters={"Matricula": str})

# Data de prioridad de destinos
file = "df_prio_dest.csv"
df_prio_dest = pd.read_csv(path + file, sep=";")

# Data de capacidad de planta
file = "df_cap_plant_all.csv"
df_cap_plant = pd.read_csv(path + file, sep=";")

# Data de máximo de días con capacida máxima
file = 'df_max_dias_pico.csv'
df_max_dias_pico = pd.read_csv(path + file, sep=';')
# A dataframe that contains the maximum number of days that a plant can work at maximum capacity.
df_max_dias_pico.fillna(0, inplace=True)

# Data de porcentaje de velocidad en dia de descanso
file = 'df_porc_veloc_descanso.csv'
df_porc_veloc_descanso = pd.read_csv(path + file, sep=';')
df_porc_veloc_descanso.fillna(0, inplace=True)
temporadas = ['2018-I', '2018-II', '2019-I', '2019-II', '2020-I', '2020-II', '2021-I', '2021-II']
df_porc_veloc_descanso[temporadas] = df_porc_veloc_descanso[temporadas].clip(upper=0.8999)

# Registro de carenas
file = "df_carenas.xlsx"
df_carenas = pd.read_excel(path + file, converters={"Matricula": str})

# Mal tiempo
file = 'df_cierre_puertos.csv'
df_mal_tiempo = pd.read_csv(path + file, sep=';')
df_mal_tiempo = df_mal_tiempo[df_mal_tiempo['TIEMPO CERRADO (horas)'] > 24]
df_mal_tiempo['CIERRE'] = pd.to_datetime(df_mal_tiempo['CIERRE'])

path = "datasets/escenarios/"

# Embarcaciones a Omitir
file = 'df_emb_omitir.xlsx'
df_emb_omitir = pd.read_excel(path + file, converters={"Matricula": str})

# Embarcaciones conversión a Frío
file = 'df_conversion_frio.xlsx'
df_conversion_frio = pd.read_excel(path + file, converters={"Matricula": str})

# TASA 45
file = 'df_tasa45.xlsx'
df_tasa45 = pd.read_excel(path + file, converters={"Matricula": str})
matricula_45 = df_tasa45['Matricula'].to_numpy()

# -- 2. DATA SCENARIOS
# -------------------------------------------------------------------

# Escenario Velocidades
file = "df_scenario_cap.csv"
df_scenario_cap = pd.read_csv(path + file, sep=";")

lst_vel_malabrigo = (
    df_scenario_cap.loc[df_scenario_cap["Malabrigo"].notna(), "Malabrigo"]
    .unique()
    .tolist()
)
lst_vel_chimbote = (
    df_scenario_cap.loc[df_scenario_cap["Chimbote"].notna(), "Chimbote"]
    .unique()
    .tolist()
)
lst_vel_samanco = (
    df_scenario_cap.loc[df_scenario_cap["Samanco"].notna(), "Samanco"].unique().tolist()
)
lst_vel_supe = (
    df_scenario_cap.loc[df_scenario_cap["Supe"].notna(), "Supe"].unique().tolist()
)
lst_vel_vegueta = (
    df_scenario_cap.loc[df_scenario_cap["Vegueta"].notna(), "Vegueta"].unique().tolist()
)
lst_vel_callao = (
    df_scenario_cap.loc[df_scenario_cap["Callao"].notna(), "Callao"].unique().tolist()
)
lst_vel_pisco_s = (
    df_scenario_cap.loc[df_scenario_cap["Pisco S"].notna(), "Pisco S"].unique().tolist()
)
lst_vel_pisco_n = (
    df_scenario_cap.loc[df_scenario_cap["Pisco N"].notna(), "Pisco N"].unique().tolist()
)


df_all_vel = pd.DataFrame(
    cartesian(
        (
            lst_vel_malabrigo,
            lst_vel_chimbote,
            lst_vel_samanco,
            lst_vel_supe,
            lst_vel_vegueta,
            lst_vel_callao,
            lst_vel_pisco_s,
            lst_vel_pisco_n,
        )
    ),
    columns=[
        "Malabrigo",
        "Chimbote",
        "Samanco",
        "Supe",
        "Vegueta",
        "Callao",
        "Pisco S",
        "Pisco N",
    ],
)


# Escenario Estrategia de Terceros
file = "df_scenario_estrategia_terceros.csv"
df_scenario_estrategia_terceros = pd.read_csv(path + file, sep=";")

lst_estrat_terce = (
    df_scenario_estrategia_terceros["Estrategia Terceros"].unique().tolist()
)


# Escenario EPS omitidas
file = "df_num_embar.csv"
df_num_embar = pd.read_csv(path + file, sep=";")
num_eps = df_num_embar["Cantidad"].unique().tolist()

# Flota con Frío
file = "df_num_eps_frio.csv"
df_num_eps_frio = pd.read_csv(path + file, sep=";")
num_eps_frio = df_num_eps_frio["Cantidad"].unique().tolist()

# -- FLOW ----------------------------------------------------------------------------------------


mode = "Run All"


if mode == "Run All":
    df_scenarios = df_all_vel.copy()
    # Estrategia de Terceros    
    df_scenarios = pd.concat(
        [df_scenarios] * (len(lst_estrat_terce)), ignore_index=True
    )
    df_scenarios["estrategia_terceros"] = np.repeat(
        lst_estrat_terce, len(df_scenarios.index) / len(lst_estrat_terce)
    )

    # Cantidad de embarcaciones de TASA
    df_scenarios = pd.concat([df_scenarios] * (len(num_eps)), ignore_index=True)
    df_scenarios["num_eps"] = np.repeat(num_eps, len(df_scenarios.index) / len(num_eps))

    # Embarcaciones con frío
    df_scenarios = pd.concat([df_scenarios] * (len(num_eps_frio)), ignore_index=True)
    df_scenarios["num_eps_frio"] = np.repeat(
        num_eps_frio, len(df_scenarios.index) / len(num_eps_frio)
    )

    # TASA 45
    df_scenarios = pd.concat([df_scenarios] * (len(matricula_45)), ignore_index=True)
    df_scenarios["matricula_45"] = np.repeat(
        matricula_45, len(df_scenarios.index) / len(matricula_45)
    )
    df_scenarios = df_scenarios.sample(n=len(df_scenarios.index))
    for index, row in df_scenarios.iterrows():
        try:
            df_embar_snr = df_embar.copy()
            df_prio_embar_snr = df_prio_embar.copy()
            df_prio_dest_snr = df_prio_dest.copy()
            df_cap_plant_snr = df_cap_plant.copy()
            df_carenas_snr = df_carenas.copy()
            capacidades = row[
                [
                    "Malabrigo",
                    "Chimbote",
                    "Samanco",
                    "Supe",
                    "Vegueta",
                    "Callao",
                    "Pisco S",
                    "Pisco N",
                ]
            ]
            matriculas_excluir = df_emb_omitir.head(48 - row['num_eps']).loc[:, 'Matricula'].tolist()
            matriculas_frio = df_conversion_frio.head(row['num_eps_frio']).loc[:, 'Matricula'].tolist()
            cbod_frio = df_conversion_frio.head(row['num_eps_frio']).loc[:, 'CBOD con Frio'].tolist()
            df_prio_embar_snr.loc[df_prio_embar_snr['Matricula'].isin(matriculas_frio), 'CBOD'] = cbod_frio

            input_matricula_45 = row['matricula_45']
            ftp_model = FTP(name_escenario_prio_embar=row['estrategia_terceros'], matriculas_excluir=matriculas_excluir, matricula_45=input_matricula_45)
            df_cap_plant_snr[ftp_model.temporadas_elegidas] = np.tile(capacidades.to_numpy().reshape(-1, 1), (1, 8))

            df_embar_flagged, df_update_planta, df_update_vol = ftp_model.predict(
                df_embar=df_embar_snr,
                df_prio_embar=df_prio_embar_snr,
                df_prio_dest=df_prio_dest_snr,
                df_cap_plant=df_cap_plant_snr,
                df_carenas=df_carenas_snr,
                df_mal_tiempo=df_mal_tiempo,
                df_max_dias_pico=df_max_dias_pico,
                df_porc_veloc_descanso=df_porc_veloc_descanso 
            )

            df_embar_flagged.to_csv(f'outputs/_scenario_output_sugerido__{index + 1}.csv', index=False)
            log(yellow(f'Escenario: {index + 1}'), green("PASS"))
        except:
            log(yellow(f'Escenario: {index + 1}'), red("ERROR"))
            # df_update_planta.to_csv(f'outputs/scenario_update_planta__{index + 1}.csv', index=False)
            # df_update_vol.to_csv(f'outputs/scenario_update_vol__{index + 1}.csv', index=False)