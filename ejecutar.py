# --------------------------------------------------------------------------------------
from ast import arguments
from cgi import print_arguments
from black import out
import pandas as pd
import numpy as np
from tqdm import tqdm
from source.engine.FTP_engine import FTPModel as FTP
from source.engine import utils as ut
from datetime import datetime
import multiprocessing

from multiprocessing import Pool
from sklearn.utils.extmath import cartesian
tqdm.pandas(desc=r'% de avance')

# 1. LECTURA DE ESCENARIOS
df_scenarios = pd.read_parquet("datasets/df_combis_total.parquet").iloc[[0]]
# df_scenarios = pd.read_parquet("datasets/df_combis_total.parquet")[10064:20128]
# df_scenarios = pd.read_parquet("datasets/df_combis_total.parquet")[20128:30192]
# df_scenarios = pd.read_parquet("datasets/df_combis_total.parquet")[30192:40202]
# df_scenarios = pd.read_parquet("datasets/df_combis_total.parquet")[40256:50406]

df_scenarios["cap_chimbote"] = 1
df_scenarios["cap_supe"] = 1
df_scenarios["cap_vegueta"] = 1
df_scenarios["cap_samanco"] = 1
df_scenarios["num_embar"] = [[None, None, None, None]]



# 2. IMPORTACIÓN DEL CEREBRO
ftp_model_current = ut.read_pickle('models/FTP_MODEL_VERSION_13.pkl')

# ftp_model_current.porc_cuota_tasa = 0.14475

# --------------------------------------------------------------------------------------
def aplicar_modelo_a_fila(fila):
    resultado = ftp_model_current.execute_running_paralelo(fila)
    return resultado

# 3. RUNNING
# --------------------------------------------------------------------------------------

if __name__ == '__main__':
    start_time = datetime.now()

    num_processes = 1
    # ------------------------------------------------------------------------------------
    # 3.1 SPLIT DATAFRAME
    particiones = [df_scenarios.iloc[i:i + len(df_scenarios) // num_processes] for i in range(0, len(df_scenarios), len(df_scenarios) // num_processes)]

    columns=['names', 'ingresos', 'mtto_omision', 'ebit_pv', 'vr_pv', 'vr_year4', 'produccion_hp', 'produccion_ap', 'CF', 'CV', 'costos_mp_extrac', 'costos_mp_terc', 'mtto_frio', 'dd', 'capex_total', 'gastos_cierre', 'cf_prom', 'market_share', 'market_terceros', 'cumpli_cuota_propia', 'mp_descargado_propios', 'mp_descargado_terceros', 'npv', 'cash_flow_pv', 'cf_vr_pv', 'depreciacion', 'capex', 'nueva_depreciacion', 'cant_dias_adicionales', 'vol_total_adicional', 'ebit_prom']
    # Iniciar el procesamiento en paralelo
    with Pool(num_processes) as pool:
        resultados = pool.map(aplicar_modelo_a_fila, particiones)
    
    resultado_final = pd.concat([pd.DataFrame(i.tolist(), columns=columns) for i in resultados])
    now = datetime.now()
    time_elapsed = now - start_time
    print(f'Time elapsed (hh:mm:ss.ms) {time_elapsed}')

    # 3.2. EXPORTACIÓN DE RESULTADOS
    # --------------------------------------------------------------------------------------

    print(resultado_final)
    resultado_final.to_excel(f'outputs/Resumen_Escenarios_{now.strftime("%Y_%m_%d-%I_%M_%S_%p")}_validation.xlsx')