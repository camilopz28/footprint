# 0. Libraries
# --------------------------------------------------------------------------------------
from ast import arguments
from cgi import print_arguments
from black import out
import pandas as pd
import numpy as np
from tqdm import tqdm
# from pyrsistent import T

from source.engine.FTP_engine import FTPModel as FTP
from source.engine import utils as ut
from datetime import datetime

from sklearn.utils.extmath import cartesian
import multiprocessing

from multiprocessing import Pool

# Función para generar combinaciones de barcos en grupos de 1, 2, 3 y 4 de manera eficiente
def generar_combinaciones_eficientes(barcos, grupo_actual, combinacion_actual, todas_combinaciones):
    if grupo_actual == 0:
        todas_combinaciones.append(combinacion_actual)
        return
    
    if len(barcos) < grupo_actual:
        return
    
    # Generar combinaciones que incluyen el primer barco
    generar_combinaciones_eficientes(barcos[1:], grupo_actual - 1, combinacion_actual + [barcos[0]], todas_combinaciones)
    
    # Generar combinaciones que no incluyen el primer barco
    generar_combinaciones_eficientes(barcos[1:], grupo_actual, combinacion_actual, todas_combinaciones)

def get_df_combinarions_ships(lista_barcos):
    todas_combinaciones = []
    for grupo in range(1, 5):
        generar_combinaciones_eficientes(lista_barcos, grupo, [], todas_combinaciones)

    df = pd.DataFrame(todas_combinaciones, columns=[f"Grupo_{i+1}" for i in range(4)])
    df['combined'] = df.apply(lambda row: [row['Grupo_1'], row['Grupo_2'], row['Grupo_3'], row['Grupo_4']], axis=1)
    return df['combined']

# TODO: Aquí modificar
tqdm.pandas(desc=r'% de avance')

# df_scenarios = pd.read_excel('datasets/escenarios/df_escenarios_automaticos.xlsx')#.head(4)#.iloc[[0, 1, 2, 3]]#.iloc[[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]]#.iloc[[0, 1, 2, 3, 4, 5]]

df_scenarios = pd.read_parquet("datasets/df_combis_total.parquet").sample(16, random_state=42)
# 6. IMPORTACIÓN DEL CEREBRO
# --------------------------------------------------------------------------------------

# ftp_model = ut.read_pickle('models/FTP_MODEL_VERSION_10.pkl')


# def apply_parallel(df, func, num_processes):
#     df_split = np.array_split(df, num_processes)
#     # pool.start()
#     pool = multiprocessing.Pool(processes=num_processes)
#     temp = pool.map(func, df_split)
#     # df = pd.concat(temp)
#     df_res = pd.DataFrame(temp, columns=['ingresos', 'mtto_omision', 'ebit_pv', 'vr_pv', 'vr_year4', 'produccion_hp', 'produccion_ap', 'CF', 'CV', 'costos_mp_extrac', 'costos_mp_terc', 'mtto_frio', 'npv', 'capex_total', 'gastos_cierre', 'cf_prom', 'market_share', 'market_terceros', 'cumpli_cuota_propia', 'mp_descargado_propios', 'mp_descargado_terceros', 'cash_flow', 'cash_flow_pv', 'cf_vr_pv', 'depreciacion', 'capex', 'nueva_depreciacion', 'cant_dias_adicionales', 'vol_total_adicional', 'ebit_prom'])
#     df_res.index = df.index
#     df_res_fin = pd.merge(df, df_res, left_index=True, right_index=True)
#     pool.close()
#     pool.join()
#     return df_res_fin

# 7. RUNNING
# --------------------------------------------------------------------------------------
if __name__ == '__main__':
    start_time = datetime.now()

    # kpis = df_scenarios.progress_apply(lambda x: ftp_model.run(x['num_embar'], x['num_eps_frio'], x['flg_45'], x['estrategia'], x['cap_malabrigo'], x['cap_chimbote'], x['cap_samanco'], x['cap_supe'], x['cap_vegueta'], x['cap_callao'], x['cap_pisco_s'], x['flg_barco_nuevo']), axis=1)
    num_processes = 4
    # lists_df_scenarios = [df_s for df_s]

    ftp_model = ut.read_pickle('models/FTP_MODEL_VERSION_11.pkl')
    # df_kpis = apply_parallel(df_scenarios, ftp_model.execute_running, num_processes)

    # ---------------------------------------
    # Dividir el DataFrame en partes iguales para cada proceso
    particiones = [df_scenarios.iloc[i:i + len(df_scenarios) // num_processes] for i in range(0, len(df_scenarios), len(df_scenarios) // num_processes)]

    columns=['names', 'ingresos', 'mtto_omision', 'ebit_pv', 'vr_pv', 'vr_year4', 'produccion_hp', 'produccion_ap', 'CF', 'CV', 'costos_mp_extrac', 'costos_mp_terc', 'mtto_frio', 'dd', 'capex_total', 'gastos_cierre', 'cf_prom', 'market_share', 'market_terceros', 'cumpli_cuota_propia', 'mp_descargado_propios', 'mp_descargado_terceros', 'npv', 'cash_flow_pv', 'cf_vr_pv', 'depreciacion', 'capex', 'nueva_depreciacion', 'cant_dias_adicionales', 'vol_total_adicional', 'ebit_prom']
    # Iniciar el procesamiento en paralelo
    with Pool(num_processes) as pool:
        resultados = pool.map(ftp_model.execute_running_paralelo, particiones)

    # Combinar los resultados en un nuevo DataFrame
    # resultado_final = pd.DataFrame([list(serie.item()) for serie in resultados], columns=columns)

    resultado_final = pd.concat([pd.DataFrame(i.tolist(), columns=columns) for i in resultados])
    # resultado_final = pd.concat([df_scenarios, resultado_final], axis=1)
    # ---------------------------------------


    now = datetime.now()
    time_elapsed = now - start_time
    print(f'Time elapsed (hh:mm:ss.ms) {time_elapsed}')


    # tuples_kpis = [i for i in kpis]
    # df_kpis = pd.DataFrame(tuples_kpis, columns=['ingresos', 'mtto_omision', 'ebit_pv', 'vr_pv', 'vr_year4', 'produccion_hp', 'produccion_ap', 'CF', 'CV', 'costos_mp_extrac', 'costos_mp_terc', 'mtto_frio', 'npv', 'capex_total', 'gastos_cierre', 'cf_prom', 'market_share', 'market_terceros', 'cumpli_cuota_propia', 'mp_descargado_propios', 'mp_descargado_terceros', 'cash_flow', 'cash_flow_pv', 'cf_vr_pv', 'depreciacion', 'capex', 'nueva_depreciacion', 'cant_dias_adicionales', 'vol_total_adicional', 'ebit_prom'])

    # df_kpis.index = df_scenarios.index
    # df_scenarios = pd.merge(df_scenarios, df_kpis, left_index=True, right_index=True)

    # df_scenarios.sort_values('cash_flow', inplace=True, ascending=False)

    # df_scenarios['ID_ESCENARIO'] = np.arange(0, len(df_scenarios.index), 1) + 1

    # 8. EXPORTACIÓN DE RESULTADOS
    # --------------------------------------------------------------------------------------

    print(resultado_final)
    resultado_final.to_excel(f'outputs/Resumen_Escenarios_{now.strftime("%Y_%m_%d-%I_%M_%S_%p")}_validation.xlsx')