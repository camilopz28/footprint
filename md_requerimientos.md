# ***Proyecto Footprint: Requerimientos del Modelo***

> Por parte del modelo, este proyecto está constituido por cuatro subprocesos.

### ***Sub Proceso 1: Prioridad de Embarcaciones***

1. Cada uno de los barcos (entre Terceros, Propios Con Frío y Propios sin Frío) tiene una respectiva prioridad. (Priorización por segmento)
2. Dentro de los barcos terceros se tiene la distinción entre Exclusivo, Preferente y Eventual.
3. De acuerdo al tipo de barco y la zona de pesca se tienen prioridades de planta. (Elección de destino disponible)

### ***Sub Proceso 2: Recepción en Planta***

1. Se busca si la planta de primera prioridad puede recibirlo, en caso negativo, el barco va a la segunda prioridad y así sucesivamente.
2. Se actualiza la capacidad disponible de cada planta.
3. Se continúa con las demás embarcaciones.

---
**Nota**

* La capacidad de planta depende de la velocidad que se utilice y el número de horas que opera en el día.

---

### ***Sub Proceso 3: Actualización de capacidad de planta***

1. Cuando hayan terminado todos los barcos del día, se actualiza la capacidad de planta identificando si se llegó a descargar más del 90% de la capacidad máxima de manera consecutiva por *d* días. (Validación de capacidad máxima.)
2. Se realiza el mismo proceso para los siguientes días hasta acabar la temporada.

---
**Nota**

* Se utiliza un variable que represanta el número de días seguidos con el que se puede operar al 90% de la capacidad máxima.

---

### ***Sub Proceso 4: Días de pesca adicionales***

1. Con el supuesto de tener un barco menos, se calcula el volumen que tiene que ser rellenado.
2. A los barcos con mejor rendimiento se les hace pescar días adicionales para rellenar el volumen faltante.


## ***Requerimientos adicionales***

1. La velocidad que se utiliza para calcular la capacidad máxima tiene que depender del año, es decir la velocidad dependerá del año y de la planta. (check)
2. Las horas de descanso no son fijas, sino dependerán del número de días siguientes al primer descanso.
   - *Primer día 14*
   - Segundo día 18
   - Tercer día 19
3. Feedback de Luis Marsano (Post Análisis).

Nota:

* Reducción de capacidad en algunas plantas (en fecha específica,  avance de porcentaje en cuota).
* Split por zona de pesca, de acuerdo al volumen disminuido.

Adicionar días perdidos.

Disponibilidad de averías, disponibilidad por mal tiempo.
Proyectar la disponibilidad.


Preguntar de capacidad de bodega.

Real VS Simulación. Cuándo cerraron puerto.


Pendientes:

- Data Input para 2022-I 
 1. Prioridad de embarcaciones (en caso haya habido una EP nueva en 2022-I).
 2. CBOD de competencia (para cuando este activado el switch fuera de temporada).
 3. Descarga competencia 2022-I (para cuando este activado el switch fuera de temporada).
 4. Capacidad de las plantas en 2022-I.
 5. Porcentaje de velocidad de descanso por plantas en 2022-I.
 6. Máximo de días pico por plantas en 2022-I.
 7. Cierre de puertos (mal tiempo) en 2022-I.
 8. Descarga de TASA + terceros a nivel de marea en 2022-I.
 9. Registro de carenas en 2022-I

- Data para escenarios
 1. Rendimientos para 2022-I (precio, split calidad, capex ...)
 2. Cuota Nacional, Descarga Nacional para 2022-I

* Las prioridades de destino son estáticas.

- Definición de escenarios de nueva corrida lodos