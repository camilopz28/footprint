import pandas as pd
import numpy as np

import pickle

def get_n_embarc():
    pass

def update_cont():
    pass

def merge_embarc(df_embar, df_prio_embar):
    df = pd.merge(df_embar, df_prio_embar, how='left', on='name_embarc')
    return df

def merge_zona(df_embar, df_prio_dest):
    df = pd.merge(df_embar, df_prio_dest, how='left', on='name_zona_1_cala')
    return df

def drop_no_priorities(df):
    """
    Drop df with zero values
    """
    df = df.loc[:, (df.isna().sum(axis=0) < len(df))]
    return df

def read_pickle(filename):
    with open(filename, 'rb') as input:
        obj = pickle.load(input)
    return obj

def green(s):
    return '\033[1;32m%s\033[m' % s


def yellow(s):
    return '\033[1;33m%s\033[m' % s


def red(s):
    return '\033[1;31m%s\033[m' % s


def log(*m):
    print(" ".join(map(str, m)))


def log_exit(*m):
    log(red("ERROR:"), *m)
    exit(1)


def check_value(input):
    try:
        log(red(f'Temporada: {input}'), green("PASS"))
    except ModuleNotFoundError:
        log(red("FAIL"), "Error")

def check_value_scenario(input):
    try:
        log(yellow(f'Escenario: {input}'), green("PASS"))
    except ModuleNotFoundError:
        log(red("FAIL"), "Error")