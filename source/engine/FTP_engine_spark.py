import pandas as pd
import numpy as np
from pyspark.sql import SparkSession
from pyspark.sql.functions import col

class FTPModel():
    """"""

    def __init__(
        self,
        name_fecha="fecha_produccion",
        id_embar="id_embarcacion",
        name_tip_embar="Tipo Tr",
        name_orden_prio="Orden_de_Prioridad",
        id_zona_pesca="zona_de_pesca_id",
        name_planta="planta",
        name_cap_max="capacidad_max",
        name_vel_plant="Velocidad",
        name_mat_prima_descar="volumen",
        name_cap_disp="capacidad_dispo",
        name_vol_actual="vol_actual",
        name_disponibilidad="disponible",
        name_logical_diario="logical_diario",
        name_reset_diario="reset_diario",
        name_dia_puede_sumar="puede_sumar_dia",
        name_contador="contador",
        name_planta_sugerida="planta_sugerida",
        name_sort_tipo_embarc="sort_tipo_embarc",
        name_year="periodo",
        name_ind_prop="Ind Propiedad",
        no_plantas=["ATICO", "MATARANI", "ILO"],
        no_zonas_pesca_id=[7.0, 8.0, 9.0],
        list_plantas_terceros=[
            "SAMANCO",
            "SUPE",
            "VEGUETA",
            "PISCO NORTE",
            "PISCO SUR",
            "CALLAO",
            "CHIMBOTE",
            "MALABRIGO",
        ],
        id_zona_pesca_terceros=[10.0, 11.0, 12.0, 13.0, 14.0, 5.0, 3.0, 2.0],
        name_temporada="temporada",
        name_cbod="CBOD",
        dict_orden_embarc={
            "A": [
                "3 Exclusivo",
                "3 Preferente",
                "3 Eventual",
                "Propio sin Frio",
                "Propio con Frio",
                "3 Otros",
            ],
            "B": [
                "3 Exclusivo",
                "3 Preferente",
                "Propio sin Frio",
                "Propio con Frio",
                "3 Eventual",
                "3 Otros",
            ],
            "C": [
                "3 Exclusivo",
                "Propio sin Frio",
                "Propio con Frio",
                "3 Preferente",
                "3 Eventual",
                "3 Otros",
            ],
            "D": [
                "3 Exclusivo",
                "Propio sin Frio",
                "3 Preferente",
                "Propio con Frio",
                "3 Eventual",
                "3 Otros",
            ],
            "E": [
                "Propio sin Frio",
                "3 Exclusivo",
                "Propio con Frio",
                "3 Preferente",
                "3 Eventual",
                "3 Otros",
            ],
            "F": [
                "Propio sin Frio",
                "Propio con Frio",
                "3 Exclusivo",
                "3 Preferente",
                "3 Eventual",
                "3 Otros",
            ],
            "G": [
                "3 Exclusivo",
                "3 Preferente",
                "3 Eventual",
                "Propio sin Frio",
                "3 Otros",
                "Propio con Frio",
            ],
            "H": [
                "3 Exclusivo",
                "3 Preferente",
                "3 Eventual",
                "3 Otros",
                "Propio sin Frio",
                "Propio con Frio",
            ],
        },
        temporadas_elegidas=['2018-I', '2018-II', '2019-I', '2019-II', '2020-I', '2020-II', '2021-I', '2021-II', '2022-I'],
        n_embar = 48,
        n_ep_frio = 19,
        ratio_cap_max=0.9,
        g_ratio=0.0,
        r_ratio=0.0957,
        porc_cuota_tasa = 0.1397542,
        ratio_out_temp = 1,
        orden_frio={
            "A":[
                "TASA 420",
                "TASA 413",
                "TASA 314",
                "TASA 23"
            ]
        }

    ):
        """
        A class used to represent a brain which replicates unloading and capture of anchovy.

        ...

        Attributes
        ----------


        Methods
        -------

        """
        self.name_fecha = name_fecha
        self.id_embar = id_embar
        self.name_tip_embar = name_tip_embar
        self.name_orden_prio = name_orden_prio
        self.id_zona_pesca = id_zona_pesca
        self.name_planta = name_planta
        self.name_cap_max = name_cap_max
        self.name_vel_plant = name_vel_plant
        self.name_mat_prima_descar = name_mat_prima_descar
        self.name_cap_disp = name_cap_disp
        self.name_vol_actual = name_vol_actual
        self.name_disponibilidad = name_disponibilidad
        self.name_logical_diario = name_logical_diario
        self.name_reset_diario = name_reset_diario
        self.name_dia_puede_sumar = name_dia_puede_sumar
        self.name_contador = name_contador
        self.name_planta_sugerida = name_planta_sugerida
        self.name_sort_tipo_embarc = name_sort_tipo_embarc
        self.name_year = name_year
        self.name_ind_prop = name_ind_prop
        self.name_no_plantas = no_plantas
        self.no_zonas_pesca_id = no_zonas_pesca_id
        self.list_plantas_terceros = list_plantas_terceros
        self.id_zona_pesca_terceros = id_zona_pesca_terceros
        self.name_temporada = name_temporada
        self.name_cbod = name_cbod
        self.dict_orden_embarc = dict_orden_embarc
        self.temporadas_elegidas = temporadas_elegidas
        self.n_embar = n_embar
        self.n_ep_frio = n_ep_frio
        self.ratio_cap_max = ratio_cap_max
        self.g_ratio = g_ratio
        self.r_ratio = r_ratio
        self.porc_cuota_tasa = porc_cuota_tasa
        self.ratio_out_temp = ratio_out_temp
        self.orden_frio = orden_frio

    def preprocessing_df_embar(
        self,
        df_embar,
        name_volumen="Pesc Desc",
        fecha_name="Fecha Produccion",
        id_zona_pesca="Zona de Pesca",
        name_planta="Descrip Planta",
        id_embarc="Matricula",
    ):
        """
        Realiza el preprocesamiento de la data de descargas.
        """
        df_sin_nulos = df_embar.filter(col(id_zona_pesca).isNotNull())
        mapeo_columnas = {
            "nombre": "nombre_completo",
            "edad": "edad_persona"
        }

        # Renombrar las columnas usando un bucle
        df_renombrado = df_sin_nulos
        for columna_antigua, columna_nueva in mapeo_columnas.items():
            df_renombrado = df_renombrado.withColumnRenamed(columna_antigua, columna_nueva)