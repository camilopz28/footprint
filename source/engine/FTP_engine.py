from tkinter import E
from matplotlib.pyplot import flag
import pandas as pd
import numpy as np
import functools
from itertools import product

from source.engine.utils import drop_no_priorities
import pickle

from joblib import Parallel, delayed
from tqdm import tqdm
# load_ext Cython

class FTPModel():
    """"""

    def __init__(
        self,
        name_fecha="fecha_produccion",
        id_embar="id_embarcacion",
        name_tip_embar="Tipo Tr",
        name_orden_prio="Orden_de_Prioridad",
        id_zona_pesca="zona_de_pesca_id",
        name_planta="planta",
        name_cap_max="capacidad_max",
        name_vel_plant="Velocidad",
        name_mat_prima_descar="volumen",
        name_cap_disp="capacidad_dispo",
        name_vol_actual="vol_actual",
        name_disponibilidad="disponible",
        name_logical_diario="logical_diario",
        name_reset_diario="reset_diario",
        name_dia_puede_sumar="puede_sumar_dia",
        name_contador="contador",
        name_planta_sugerida="planta_sugerida",
        name_sort_tipo_embarc="sort_tipo_embarc",
        name_year="periodo",
        name_ind_prop="Ind Propiedad",
        no_plantas=["ATICO", "MATARANI", "ILO"],
        no_zonas_pesca_id=[7.0, 8.0, 9.0],
        list_plantas_terceros=[
            "SAMANCO",
            "SUPE",
            "VEGUETA",
            "PISCO NORTE",
            "PISCO SUR",
            "CALLAO",
            "CHIMBOTE",
            "MALABRIGO",
        ],
        id_zona_pesca_terceros=[10.0, 11.0, 12.0, 13.0, 14.0, 5.0, 3.0, 2.0],
        name_temporada="temporada",
        name_cbod="CBOD",
        dict_orden_embarc={
            "A": [
                "3 Exclusivo",
                "3 Preferente",
                "3 Eventual",
                "Propio sin Frio",
                "Propio con Frio",
                "3 Otros",
            ],
            "B": [
                "3 Exclusivo",
                "3 Preferente",
                "Propio sin Frio",
                "Propio con Frio",
                "3 Eventual",
                "3 Otros",
            ],
            "C": [
                "3 Exclusivo",
                "Propio sin Frio",
                "Propio con Frio",
                "3 Preferente",
                "3 Eventual",
                "3 Otros",
            ],
            "D": [
                "3 Exclusivo",
                "Propio sin Frio",
                "3 Preferente",
                "Propio con Frio",
                "3 Eventual",
                "3 Otros",
            ],
            "E": [
                "Propio sin Frio",
                "3 Exclusivo",
                "Propio con Frio",
                "3 Preferente",
                "3 Eventual",
                "3 Otros",
            ],
            "F": [
                "Propio sin Frio",
                "Propio con Frio",
                "3 Exclusivo",
                "3 Preferente",
                "3 Eventual",
                "3 Otros",
            ],
            "G": [
                "3 Exclusivo",
                "3 Preferente",
                "3 Eventual",
                "Propio sin Frio",
                "3 Otros",
                "Propio con Frio",
            ],
            "H": [
                "3 Exclusivo",
                "3 Preferente",
                "3 Eventual",
                "3 Otros",
                "Propio sin Frio",
                "Propio con Frio",
            ],
        },
        temporadas_elegidas=['2018-I', '2018-II', '2019-I', '2019-II', '2020-I', '2020-II', '2021-I', '2021-II', '2022-I'],
        n_embar = 48,
        n_ep_frio = 19,
        ratio_cap_max=0.9,
        g_ratio=0.0,
        r_ratio=0.0957,
        # porc_cuota_tasa = 0.1397542,
        porc_cuota_tasa = 0.14475,
        ratio_out_temp = 1,
        orden_frio={
            "A":[
                "TASA 420",
                "TASA 413",
                "TASA 314",
                "TASA 23"
            ]
        }

    ):
        """
        A class used to represent a brain which replicates unloading and capture of anchovy.

        ...

        Attributes
        ----------


        Methods
        -------

        """
        self.name_fecha = name_fecha
        self.id_embar = id_embar
        self.name_tip_embar = name_tip_embar
        self.name_orden_prio = name_orden_prio
        self.id_zona_pesca = id_zona_pesca
        self.name_planta = name_planta
        self.name_cap_max = name_cap_max
        self.name_vel_plant = name_vel_plant
        self.name_mat_prima_descar = name_mat_prima_descar
        self.name_cap_disp = name_cap_disp
        self.name_vol_actual = name_vol_actual
        self.name_disponibilidad = name_disponibilidad
        self.name_logical_diario = name_logical_diario
        self.name_reset_diario = name_reset_diario
        self.name_dia_puede_sumar = name_dia_puede_sumar
        self.name_contador = name_contador
        self.name_planta_sugerida = name_planta_sugerida
        self.name_sort_tipo_embarc = name_sort_tipo_embarc
        self.name_year = name_year
        self.name_ind_prop = name_ind_prop
        self.name_no_plantas = no_plantas
        self.no_zonas_pesca_id = no_zonas_pesca_id
        self.list_plantas_terceros = list_plantas_terceros
        self.id_zona_pesca_terceros = id_zona_pesca_terceros
        self.name_temporada = name_temporada
        self.name_cbod = name_cbod
        self.dict_orden_embarc = dict_orden_embarc
        self.temporadas_elegidas = temporadas_elegidas
        self.n_embar = n_embar
        self.n_ep_frio = n_ep_frio
        self.ratio_cap_max = ratio_cap_max
        self.g_ratio = g_ratio
        self.r_ratio = r_ratio
        self.porc_cuota_tasa = porc_cuota_tasa
        self.ratio_out_temp = ratio_out_temp
        self.orden_frio = orden_frio

    def preprocessing_df_embar(
        self,
        df_embar,
        name_volumen="Pesc Desc",
        fecha_name="Fecha Produccion",
        id_zona_pesca="Zona de Pesca",
        name_planta="Descrip Planta",
        id_embarc="Matricula",
    ): # MARCO
        """
        Realiza el preprocesamiento de la data de descargas.
        """
        mask = df_embar[id_zona_pesca].notna()
        df_embar = df_embar[mask]
        df_embar.rename(
            columns={
                name_volumen: self.name_mat_prima_descar,
                fecha_name: self.name_fecha,
                id_zona_pesca: self.id_zona_pesca,
                name_planta: self.name_planta,
                id_embarc: self.id_embar,
            },
            inplace=True,
        )
        df_embar["arrival_date"] = pd.to_datetime(
            df_embar["Fecha Arribo"] + " " + df_embar["Hora Arribo"]
        )
        df_embar["zarpe_date"] = pd.to_datetime(
            df_embar["Fecha Zarpe"] + " " + df_embar["Hora Zarpe"]
        )
        df_embar["duration_marea"] = (
            df_embar["arrival_date"] - df_embar["zarpe_date"]
        ).dt.total_seconds() / 3600
        df_embar.drop(
            columns=[
                "Fecha Arribo",
                "Hora Arribo",
                "Fecha Zarpe",
                "Hora Zarpe",
                "arrival_date",
                "zarpe_date",
            ],
            inplace=True,
        )
        mask = ~df_embar[self.id_zona_pesca].isin(self.no_zonas_pesca_id)
        df_embar = df_embar[mask]
        df_embar.loc[:, self.id_embar] = (
            df_embar[self.id_embar]
            .str.split(r"-", n=-1, expand=True)
            .loc[:, 1]
            .to_numpy()
        )
        mask = df_embar[self.name_planta].isin(self.name_no_plantas)
        df_embar = df_embar[~mask]
        df_embar.loc[:, self.name_fecha] = pd.to_datetime(
            df_embar.loc[:, self.name_fecha], format="%Y/%m/%d"
        )
        df_embar.sort_values(self.name_fecha, inplace=True)
        df_embar.reset_index(drop=True, inplace=True)

        list_plantas_terceros = self.list_plantas_terceros
        id_zona_pesca_terceros = self.id_zona_pesca_terceros

        for i in range(len(id_zona_pesca_terceros)):
            mask = (df_embar[self.name_planta] == list_plantas_terceros[i]) & (
                df_embar[self.name_ind_prop] == "Tercera"
            )
            df_embar.loc[mask, self.id_zona_pesca] = id_zona_pesca_terceros[i]

        df_embar[self.name_mat_prima_descar] = df_embar.groupby(
            [self.id_embar, self.name_fecha]
        )[self.name_mat_prima_descar].transform("sum")
        df_embar.drop_duplicates(subset=[self.id_embar, self.name_fecha], inplace=True)
        df_embar[self.name_year] = df_embar[self.name_fecha].dt.year

        df_embar.sort_values(self.name_fecha, inplace=True)
        df_embar["diferences"] = np.c_[
            np.array([0]),
            np.diff(df_embar[self.name_fecha].dt.date.to_numpy())
            .astype("timedelta64[s]")
            .reshape(1, -1),
        ].flatten()
        name_dif_min = "dif_minutes"
        df_embar.loc[:, name_dif_min] = df_embar["diferences"].dt.total_seconds() / 60

        name_flg_tempo = "flg_temporada"
        df_embar[name_flg_tempo] = 0
        mask = df_embar[name_dif_min] > 43200
        df_embar.loc[mask, name_flg_tempo] = 1

        vals = np.arange(0, len(self.temporadas_elegidas))

        flgs = df_embar[name_flg_tempo].to_numpy()
        flgs = np.cumsum(flgs)
        df_embar[name_flg_tempo] = flgs

        conditions = [df_embar[name_flg_tempo] == i for i in vals]
        df_embar[self.name_temporada] = np.select(conditions, self.temporadas_elegidas)
        df_embar.drop(
            ["flg_temporada", "dif_minutes", "diferences"],
            axis=1,
            inplace=True,
        )
        mask = df_embar[self.name_ind_prop] == "Tercera"
        self.ids_embar_prop = df_embar.loc[~mask, self.id_embar].unique().tolist()
        return df_embar

    def filter_by_temporada(self, df_embar): # MARCO
        """
        Filtra la data de descargas por cada temporada.
        """
        mask = df_embar[self.name_temporada].isin(self.temporadas_elegidas)
        return df_embar[mask]

    def preprocessing_df_prio_embar(
        self,
        df_prio_embar,
        id_embar="Matricula",
        name_orden_prio="Orden",
        name_tipo="Tipo",
    ): # MARCO
        """
        Realiza el preprocesamiento de la data de prioridad de embarcaciones
        """
        df_prio_embar.rename(
            columns={
                id_embar: self.id_embar,
                name_orden_prio: self.name_orden_prio,
                name_tipo: self.name_tip_embar,
            },
            inplace=True,
        )
        df_prio_embar[self.id_embar] = df_prio_embar[self.id_embar].astype(str)
        mask = df_prio_embar[self.name_tip_embar].isin(["3 Exclusivo", "3 Preferente", "3 Eventual", "3 Otros"])
        self.df_maestro_cbod = df_prio_embar.loc[~mask, [self.id_embar, self.name_cbod]]
        return df_prio_embar

    def preprocessing_df_prio_dest(
        self,
        df_prio_dest,
        id_zona_pesca="ID_zona",
        name_tip_embar="Tipo Embarc",
    ): # MARCO
        """
        Realiza el preprocesamiento de la data de prioridad de plantas.
        """
        df_prio_dest.rename(
            columns={
                id_zona_pesca: self.id_zona_pesca,
                name_tip_embar: self.name_tip_embar,
            },
            inplace=True,
        )
        df_prio_dest[self.id_zona_pesca] = df_prio_dest[self.id_zona_pesca].astype(
            float
        )
        self.all_zonas_pesca = df_prio_dest[self.id_zona_pesca].unique().tolist()
        self.cols_prio = df_prio_dest.columns[
            df_prio_dest.columns.str.startswith("Prioridad")
        ]
        to_replace = {"Prop SF": "Propio sin Frio", "Prop CF": "Propio con Frio"}
        df_prio_dest.replace({self.name_tip_embar: to_replace}, inplace=True)
        to_replace = {"Pisco Norte": "Pisco N", "Pisco Sur": "Pisco S"}
        df_prio_dest.replace(to_replace, inplace=True)
        return df_prio_dest

    def preprocessing_df_cap_plant(
        self,
        df_cap_plant,
        name_planta="Planta",
    ): # MARCO
        """
        Realiza el preproceamiento de las capacidades de las plantas
        """
        df_cap_plant.rename(
            columns={
                name_planta: self.name_planta,
            },
            inplace=True,
        )
        df_cap_plant.loc[:, self.temporadas_elegidas] = df_cap_plant.loc[:, self.temporadas_elegidas].clip(lower=0.000001)
        return df_cap_plant

    def preprocessing_competence(self, df_descarga_competencia, df_cbod_competencia): # MARCO
        """
        Realiza el preprocesamiento de la data de la competencia.
        """
        df_descarga_competencia.loc[:, 'Matricula'] = (
            df_descarga_competencia['Matricula']
            .str.split(r"-", n=-1, expand=True)
            .loc[:, 1]
            .to_numpy())
        df_cbod_competencia.loc[:, 'Matricula'] = (
                    df_cbod_competencia['Matricula']
                    .str.split(r"-", n=-1, expand=True)
                    .loc[:, 1]
                    .to_numpy())
        df_cbod_competencia.rename(columns={'C.Bod':'CBOD'}, inplace=True)

        df_descarga_competencia = df_descarga_competencia[df_descarga_competencia['ZONA'] == 'Zona Norte']
        df_descarga_competencia = df_descarga_competencia[df_descarga_competencia['Fecha'] > '26/01/2018']

        df_descarga_competencia['Fecha'] = pd.to_datetime(
                    df_descarga_competencia.loc[:, 'Fecha'], format="%Y/%m/%d"
                )

        df_descarga_competencia.sort_values('Fecha', inplace=True)
        df_descarga_competencia["diferences"] = np.c_[
            np.array([0]),
            np.diff(df_descarga_competencia['Fecha'].dt.date.to_numpy())
            .astype("timedelta64[s]")
            .reshape(1, -1),
        ].flatten()
        name_dif_min = "dif_minutes"
        df_descarga_competencia.loc[:, name_dif_min] = df_descarga_competencia["diferences"].dt.total_seconds() / 60

        name_flg_tempo = "flg_temporada"
        df_descarga_competencia[name_flg_tempo] = 0
        mask = df_descarga_competencia[name_dif_min] > 43200
        df_descarga_competencia.loc[mask, name_flg_tempo] = 1

        vals = np.arange(0, len(self.temporadas_elegidas))

        flgs = df_descarga_competencia[name_flg_tempo].to_numpy()
        flgs = np.cumsum(flgs)
        df_descarga_competencia[name_flg_tempo] = flgs
        conditions = [df_descarga_competencia[name_flg_tempo] == i for i in vals]
        df_descarga_competencia['Temporada'] = np.select(conditions, self.temporadas_elegidas)

        df_descarga_competencia = pd.merge(df_descarga_competencia, df_cbod_competencia, on=['Matricula', 'Temporada'], how='left')
        df_descarga_competencia = df_descarga_competencia[~(df_descarga_competencia['Planta'] == 'TECNOLOGICA DE ALIMENTOS S.A.')]

        cols = ['Puerto', 'TM Descargadas', 'Temporada', 'CBOD', 'Fecha']
        df_descarga_competencia = df_descarga_competencia[cols]
        df_descarga_competencia['rendimiento'] = df_descarga_competencia['TM Descargadas'] / df_descarga_competencia['CBOD']
        
        to_replace = {
        'BAYOVAR':2,
        'COISHCO':3,
        'CALLAO':5,
        'MALABRIGO':2,
        'CHIMBOTE':3,
        'CHANCAY':5,    
        'PARACAS (Pisco)':6,
        'CARQUIN':4,
        'VEGUETA':4,
        'SUPE':4,
        'TAMBO DE MORA':6,
        }
        df_descarga_competencia.replace({'Puerto':to_replace}, inplace=True)
        df_descarga_competencia.rename(columns={'Puerto':self.id_zona_pesca}, inplace=True)
        return df_descarga_competencia

    def set_priority_embar(self, df_embar, df_prio_embar, estrategia, matriculas_conver_frio, cbod_frio): # MARCO
        """
        Obtiene el orden de prioridad de las EPS y la modifica según la conversión de barcos a frío.
        """
        df_prio_embar_copy = df_prio_embar.copy()
        df_embar_copy = df_embar.copy()

        for i_cont, matricula_conversion_frio in enumerate(matriculas_conver_frio):
            print(matricula_conversion_frio)
            mask = df_prio_embar_copy[self.id_embar].isin([matricula_conversion_frio])
            ratio_decremento = cbod_frio[i_cont] / df_prio_embar_copy.loc[mask, "CBOD"]

            df_prio_embar_copy.loc[mask, "CBOD"] = cbod_frio[i_cont]
            mask = df_embar_copy[self.id_embar].isin([matricula_conversion_frio])
            df_embar_copy.loc[mask, "volumen"] = df_embar_copy.loc[mask, "volumen"] * ratio_decremento
        # try:
        #     ratio_decremento = self.cbod_frio[0] / df_prio_embar_copy.loc[
        #         df_prio_embar_copy[self.id_embar].isin(matriculas_conver_frio), "CBOD"
        #     ].item() 
        # except:
        #     ratio_decremento = 1
        # df_prio_embar_copy.loc[
        #     df_prio_embar_copy[self.id_embar].isin(matriculas_conver_frio), "CBOD"
        # ] = self.cbod_frio

        df_prio_embar_copy.loc[
            df_prio_embar_copy[self.id_embar].isin(matriculas_conver_frio), self.name_tip_embar
        ] = 'Propio con Frio'

        df_prio_embar_copy[self.name_sort_tipo_embarc] = pd.Categorical(
            df_prio_embar_copy[self.name_tip_embar],
            self.dict_orden_embarc[estrategia],
        )
        to_replace = {
            "3 Exclusivo": "Terceros",
            "3 Preferente": "Terceros",
            "3 Eventual": "Terceros",
            "3 Otros": "Terceros",
        }
        df_prio_embar_copy.replace({self.name_tip_embar: to_replace}, inplace=True)
        df_prio_embar_copy[self.id_embar] = df_prio_embar_copy[self.id_embar].astype(str)
        df_embar_modified = pd.merge(df_embar_copy, df_prio_embar_copy, how="left", on=self.id_embar)

        # mask = df_embar_modified[self.id_embar].isin(self.matriculas_conver_frio)
        # df_embar_modified.loc[mask, self.name_mat_prima_descar] = df_embar_modified.loc[mask, self.name_mat_prima_descar] *  ratio_decremento
        return df_embar_modified

    def get_1_cala(self, df_embar, df_prio_dest): # MARCO
        """
        Obtiene el id de la zona de pesca y el tipo de embarcacion
        """
        return pd.merge(
            df_embar,
            df_prio_dest,
            how="left",
            on=[self.id_zona_pesca, self.name_tip_embar],
        )

    def get_twin_ships(self, df_embar, df_twin_ships): # NO HACER
        """
        Hace match con los barcos gemelos."""
        return pd.merge(
            df_embar,
            df_twin_ships,
            how="left",
            on=[self.id_zona_pesca, self.name_tip_embar],
        )

    def update_current_vol(self, df_update_planta): # CESAR
        """
        Actualiza el volumen actual de la planta.
        """
        df_update_planta_copy = df_update_planta.copy()
        df_update_planta_copy.loc[:, self.name_disponibilidad] = (
            df_update_planta_copy.loc[:, self.name_vol_actual]
            < df_update_planta_copy.loc[:, self.name_cap_max]
        )
        return df_update_planta_copy

    def get_last_cala_day_ep(self, df_embar_post, temporada): # CESAR
        """
        Calcula el último día de cala de cada embarcación en una temporada específica.
        """
        mask = (df_embar_post[self.name_ind_prop] == "Propia") & (
            df_embar_post[self.name_temporada] == temporada
        )
        df_last_cala = (
            df_embar_post.loc[
                mask, [self.id_zona_pesca, self.id_embar, self.name_fecha]
            ]
            .groupby([self.id_zona_pesca, self.id_embar], as_index=False)
            .agg(
                ult_dia_cala=(self.name_fecha, np.max),
                cant_dias_cala=(self.name_fecha, "nunique"),
            )
        )
        return df_last_cala

    def estimate_vol(self, df_rendi, cols): # CESAR
        """
        Llena el volumen correspondiente a los días de pesca adicionales.
        """
        groups = [self.id_zona_pesca]
        df_rendi[cols] = df_rendi.groupby(groups)[cols].transform(
            lambda x: x.fillna(x.mean())
        )
        return df_rendi

    def estimate_aditional_fishing(self, df_embar, temporada, df_carenas): # CESAR
        """
        Genera tabla con las fechas de día de pesca adicional y sus respectivos volúmenes a nivel zona de pesca - EP.
        """
        # Fill negative values by Nan
        df_embar.loc[df_embar["duration_marea"] < 0, "duration_marea"] = np.nan
        df_embar["duration_marea"].fillna(
            df_embar["duration_marea"].mean(), inplace=True
        )
        df_last_cala = self.get_last_cala_day_ep(df_embar_post=df_embar, temporada=temporada)
        last_day = df_last_cala["ult_dia_cala"].max()

        df_carenas = df_carenas[df_carenas["VARADO"] == temporada]

        # df_barco_nuevo = df_embar[df_embar['id_embarcacion'] == '22029']
        # df_barco_nuevo[self.id_embar] = '22030'
        # df_barco_nuevo['volumen'] = df_barco_nuevo['volumen'] * 2

        # df_embar = pd.concat([df_embar, df_barco_nuevo], axis=0, ignore_index=True)

        embar_carenas = df_carenas["Matricula"].unique()
        mask = (df_embar[self.id_embar].isin(self.ids_embar_prop)) & (
            df_embar[self.name_temporada] == temporada
        )
        df_estimate = pd.pivot_table(
            df_embar[mask],
            values="volumen",
            aggfunc="sum",
            index=[self.id_zona_pesca, self.id_embar],
            columns=self.name_fecha,
        ).reset_index()
        df_marea_duration = pd.pivot_table(
            df_embar[mask],
            values="duration_marea",
            aggfunc="sum",
            index=[self.id_zona_pesca, self.id_embar],
            columns=self.name_fecha,
        ).reset_index()
        # TODO: Validar orden
        df_estimate = df_estimate[
            ~df_estimate["id_embarcacion"].isin(embar_carenas)
        ].reset_index(drop=True)
        df_marea_duration = df_marea_duration[
            ~df_marea_duration["id_embarcacion"].isin(embar_carenas)
        ].reset_index(drop=True)
        cols = df_estimate.columns.difference(
            [self.id_zona_pesca, self.id_embar, self.name_cbod], sort=False
        )
        cols_rendi = df_estimate.columns.difference(
            [self.id_zona_pesca, self.id_embar, self.name_cbod], sort=False
        ).to_list()
        df_estimate = pd.merge(
            df_estimate, self.df_maestro_cbod, on=self.id_embar, how="left"
        )
        df_rendi = df_estimate.copy()
        df_rendi[cols] = df_estimate[cols] / df_estimate[
            self.name_cbod
        ].to_numpy().reshape(-1, 1)
        # Rendimiento
        df_rendi[cols_rendi] = df_rendi[cols_rendi] / (
            df_marea_duration[cols_rendi] / 24
        )

        df_estimate = self.estimate_vol(df_rendi, cols)
        df_estimate[cols] = (
            df_estimate[cols]
            * df_estimate[self.name_cbod].to_numpy().reshape(-1, 1)
            * 0.92
        )

        df_estimate = pd.merge(
            df_estimate,
            df_last_cala,
            how="left",
            on=[self.id_embar, self.id_zona_pesca]
        )
        df_estimate["dias_faltantes"] = last_day - df_estimate["ult_dia_cala"]
        cols = df_estimate.columns.difference(
            [
                self.id_zona_pesca,
                self.id_embar,
                self.name_cbod,
                "ult_dia_cala",
                "cant_dias_cala",
                "dias_faltantes",
            ]
        )
        df_estimate[cols].fillna(0, inplace=True) # TODO: Revisar NaN
        df_temp1 = df_estimate[cols]
        df_temp1[cols] = cols.tolist()

        mask = df_temp1[cols] > df_estimate["ult_dia_cala"].to_numpy().reshape(-1, 1)
        df_estimate[self.name_fecha] = (
            mask.where(mask)
            .stack()
            .reset_index()
            .groupby("level_0")["level_1"]
            .agg(list)
        )
        cols = df_estimate.columns.difference(
            cols.tolist()
            + ["ult_dia_cala", "cant_dias_cala", "dias_faltantes"]
        )
        df_vol = df_estimate[mask]

        df_vol[last_day] = df_vol[last_day].fillna(0)
        df_vol[cols_rendi] = np.where((mask == True) & (df_vol[cols_rendi].isna()), 0, df_vol[cols_rendi])
        df_vol["nuevos_vols"] = df_vol.stack().groupby(level=0).apply(list).tolist()
        df_vol[[self.id_zona_pesca, self.id_embar]] = df_estimate[[self.id_zona_pesca, self.id_embar]].copy()
        df_estimate = df_estimate[cols].explode(self.name_fecha, ignore_index=True)

        temp_vols = df_vol[[self.id_zona_pesca, self.id_embar, 'nuevos_vols']].explode('nuevos_vols', 
            ignore_index=True
        )
        df_last_day_embar = df_last_cala.groupby(self.id_embar, as_index=False)['ult_dia_cala'].max()
        df_estimate[self.name_mat_prima_descar] = temp_vols['nuevos_vols']
        df_estimate = df_estimate[df_estimate[self.name_mat_prima_descar] > 0]
        df_estimate = pd.merge(df_estimate, df_last_day_embar[[self.id_embar, 'ult_dia_cala']], on=self.id_embar, how='left')
        df_estimate = df_estimate[df_estimate[self.name_fecha] > df_estimate['ult_dia_cala']]
        del df_estimate['ult_dia_cala']
        df_estimate.reset_index(inplace=True, drop=True)
        df_estimate[self.name_temporada] = temporada
        df_estimate[self.name_ind_prop] = 'Propia'
        df_estimate[self.name_mat_prima_descar] = df_estimate[self.name_mat_prima_descar].clip(upper=df_estimate[self.name_cbod])
        return df_estimate

    def estimate_fishin_new_ship(self, df_embar, temporada, df_carenas): # CESAR
        """
        Genera tabla con las fechas de día de pesca adicional y sus respectivos volúmenes a nivel zona de pesca - EP.
        """
        # Fill negative values by Nan
        df_embar.loc[df_embar["duration_marea"] < 0, "duration_marea"] = np.nan
        df_embar["duration_marea"].fillna(
            df_embar["duration_marea"].mean(), inplace=True
        )
        df_last_cala = self.get_last_cala_day_ep(df_embar_post=df_embar, temporada=temporada)
        last_day = df_last_cala["ult_dia_cala"].max()

        df_carenas = df_carenas[df_carenas["VARADO"] == temporada]

        # df_barco_nuevo = df_embar[df_embar['id_embarcacion'] == '22029']
        # df_barco_nuevo[self.id_embar] = '22030'
        # df_barco_nuevo['volumen'] = df_barco_nuevo['volumen'] * 2

        # df_embar = pd.concat([df_embar, df_barco_nuevo], axis=0, ignore_index=True)

        # embar_carenas = df_carenas["Matricula"].unique()
        mask = (df_embar[self.id_embar].isin(self.ids_embar_prop)) & (
            df_embar[self.name_temporada] == temporada
        )
        df_temp = df_embar[mask].reset_index(drop=True)

        if len(df_temp.index) == 0:
            return pd.DataFrame()

        df_estimate = pd.pivot_table(
            df_temp,
            values="volumen",
            aggfunc="sum",
            index=[self.id_zona_pesca, self.id_embar],
            columns=self.name_fecha,
        ).reset_index()
        df_marea_duration = pd.pivot_table(
            df_temp,
            values="duration_marea",
            aggfunc="sum",
            index=[self.id_zona_pesca, self.id_embar],
            columns=self.name_fecha,
        ).reset_index()
        # TODO: Validar orden
        # df_estimate = df_estimate[
        #     ~df_estimate["id_embarcacion"].isin(embar_carenas)
        # ].reset_index(drop=True)
        # df_marea_duration = df_marea_duration[
        #     ~df_marea_duration["id_embarcacion"].isin(embar_carenas)
        # ].reset_index(drop=True)
        cols = df_estimate.columns.difference(
            [self.id_zona_pesca, self.id_embar, self.name_cbod], sort=False
        )
        cols_rendi = df_estimate.columns.difference(
            [self.id_zona_pesca, self.id_embar, self.name_cbod], sort=False
        ).to_list()
        df_estimate = pd.merge(
            df_estimate, self.df_maestro_cbod, on=self.id_embar, how="left"
        )
        df_rendi = df_estimate.copy()
        df_rendi[cols] = df_estimate[cols] / df_estimate[
            self.name_cbod
        ].to_numpy().reshape(-1, 1)
        # Rendimiento
        df_rendi[cols_rendi] = df_rendi[cols_rendi] / (
            df_marea_duration[cols_rendi] / 24
        )

        df_estimate = self.estimate_vol(df_rendi, cols)
        df_estimate[cols] = (
            df_estimate[cols]
            * df_estimate[self.name_cbod].to_numpy().reshape(-1, 1)
            * 0.92
        )

        df_estimate = pd.merge(
            df_estimate,
            df_last_cala,
            how="left",
            on=[self.id_embar, self.id_zona_pesca]
        )
        df_estimate["dias_faltantes"] = last_day - df_estimate["ult_dia_cala"]
        cols = df_estimate.columns.difference(
            [
                self.id_zona_pesca,
                self.id_embar,
                self.name_cbod,
                "ult_dia_cala",
                "cant_dias_cala",
                "dias_faltantes",
            ]
        )
        df_estimate[cols].fillna(0, inplace=True) # TODO: Revisar NaN
        df_temp1 = df_estimate[cols]
        df_temp1[cols] = cols.tolist()

        # mask = df_temp1[cols] > df_estimate["ult_dia_cala"].to_numpy().reshape(-1, 1)
        mask = df_temp1[cols] > df_estimate["ult_dia_cala"].min()
        df_estimate[self.name_fecha] = (
            mask.where(mask)
            .stack()
            .reset_index()
            .groupby("level_0")["level_1"]
            .agg(list)
        )
        cols = df_estimate.columns.difference(
            cols.tolist()
            + ["ult_dia_cala", "cant_dias_cala", "dias_faltantes"]
        )
        df_vol = df_estimate[mask]

        df_vol[last_day] = df_vol[last_day].fillna(0)
        df_vol[cols_rendi] = np.where((mask == True) & (df_vol[cols_rendi].isna()), 0, df_vol[cols_rendi])
        df_vol["nuevos_vols"] = df_vol.stack().groupby(level=0).apply(list).tolist()
        df_vol[[self.id_zona_pesca, self.id_embar]] = df_estimate[[self.id_zona_pesca, self.id_embar]].copy()
        df_estimate = df_estimate[cols].explode(self.name_fecha, ignore_index=True)

        temp_vols = df_vol[[self.id_zona_pesca, self.id_embar, 'nuevos_vols']].explode('nuevos_vols', 
            ignore_index=True
        )
        df_last_day_embar = df_last_cala.groupby(self.id_embar, as_index=False)['ult_dia_cala'].max()
        df_estimate[self.name_mat_prima_descar] = temp_vols['nuevos_vols']
        df_estimate = df_estimate[df_estimate[self.name_mat_prima_descar] > 0]
        df_estimate = pd.merge(df_estimate, df_last_day_embar[[self.id_embar, 'ult_dia_cala']], on=self.id_embar, how='left')
        # df_estimate = df_estimate[df_estimate[self.name_fecha] > df_estimate['ult_dia_cala']]
        del df_estimate['ult_dia_cala']
        df_estimate.reset_index(inplace=True, drop=True)
        df_estimate[self.name_temporada] = temporada
        df_estimate[self.name_ind_prop] = 'Propia'
        df_estimate[self.name_mat_prima_descar] = df_estimate[self.name_mat_prima_descar].clip(upper=df_estimate[self.name_cbod])
        return df_estimate

    def create_update_planta(self, df_cap_plant, df_embar):  # CESAR
        """
        Crea dataframe para almacenar la actualización diaria de cada planta.
        """
        plantas = df_cap_plant[self.name_planta].unique().tolist()
        all_dates = df_embar[self.name_fecha].unique()
        df_update_planta = pd.DataFrame(
            list(product(all_dates, plantas)),
            columns=[self.name_fecha, self.name_planta],
        )
        df_update_planta = pd.merge(
            df_update_planta, df_cap_plant, how="left", on=self.name_planta
        )
        df_update_planta.loc[:, self.name_vol_actual] = 0
        df_update_planta.loc[:, self.name_logical_diario] = True
        df_update_planta = pd.merge(
            df_update_planta,
            df_embar[[self.name_fecha, self.name_temporada]].drop_duplicates(),
            how="left",
            on=self.name_fecha,
        )

        df_update_planta[self.name_cap_max] = (
            np.where(
                df_update_planta[self.name_temporada] == self.temporadas_elegidas[0],
                df_update_planta[self.temporadas_elegidas[0]],
                np.where(
                    df_update_planta[self.name_temporada] == self.temporadas_elegidas[1],
                    df_update_planta[self.temporadas_elegidas[1]],
                    np.where(
                        df_update_planta[self.name_temporada] == self.temporadas_elegidas[2],
                        df_update_planta[self.temporadas_elegidas[2]],
                        np.where(
                            df_update_planta[self.name_temporada] == self.temporadas_elegidas[3],
                            df_update_planta[self.temporadas_elegidas[3]],
                            np.where(
                                df_update_planta[self.name_temporada] == self.temporadas_elegidas[4],
                                df_update_planta[self.temporadas_elegidas[4]],
                                np.where(
                                    df_update_planta[self.name_temporada] == self.temporadas_elegidas[5],
                                    df_update_planta[self.temporadas_elegidas[5]],
                                    np.where(
                                        df_update_planta[self.name_temporada] == self.temporadas_elegidas[6],
                                        df_update_planta[self.temporadas_elegidas[6]],
                                        np.where(df_update_planta[self.name_temporada] == self.temporadas_elegidas[7], 
                                        df_update_planta[self.temporadas_elegidas[7]], df_update_planta[self.temporadas_elegidas[8]])
                                        
                                    )
                                )
                            )
                        ),
                    ),
                ),
            )
        )

        df_update_planta = self.update_current_vol(df_update_planta)
        return df_update_planta

    def get_matriculas_excluir(self, df_emb_omitir, df_master_eps):  # CESAR
        """
        De acuerdo a la cantidad de flota total, obtiene las matrículas de las embarcaciones que
        deben ser excluidas.
        """
        # n_emb = self.num_embar
        # n_emb_excluir = 48 - n_emb
        # self.matriculas_excluir =  df_emb_omitir[:n_emb_excluir].loc[:, 'Matricula'].tolist()
        df_master_eps_copy = df_master_eps.copy()
        mask = df_master_eps_copy["Nombre"].isin(self.num_embar)
        # self.matriculas_excluir =  df_master_eps_copy.loc[mask, 'Matricula'].tolist()
        return df_master_eps_copy.loc[mask, 'Matricula'].tolist()

    def get_matriculas_conversion_frio(self, df_conversion_frio):  # CESAR
        """
        De acuerdo a la cantidad de flota total, obtiene las matrículas de las embarcaciones que
        deben ser excluidas.
        """
        n_ep_frio = self.num_eps_frio
        new_n_ep_frio = n_ep_frio - 19

        df_conversion_frio_copy = df_conversion_frio.copy()
        orden_frio={
            "A":[
                "TASA 420",
                "TASA 413",
                "TASA 314",
                "TASA 23"
            ]
        }
        
        # df_conversion_frio_copy["Tasa Sin Frio"] = pd.Categorical(
        #             df_conversion_frio_copy["Tasa Sin Frio"],
        #             orden_frio["A"],
        #         )
        # df_conversion_frio_copy.sort_values("Tasa Sin Frio")
        # self.matriculas_conver_frio =  df_conversion_frio_copy[:new_n_ep_frio].loc[:, 'Matricula'].tolist()
        # self.cbod_frio = df_conversion_frio_copy[:new_n_ep_frio].loc[:, 'CBOD DEEP CHILL'].tolist()
        return df_conversion_frio_copy[:new_n_ep_frio].loc[:, 'Matricula'].tolist(), df_conversion_frio_copy[:new_n_ep_frio].loc[:, 'CBOD DEEP CHILL'].tolist()

    def get_matricula_tasa_45(self, df_tasa45):  # CESAR
        """
        De acuerdo a la cantidad de flota total, obtiene las matrículas de las embarcaciones que
        deben ser excluidas.
        """
        flag_45 = self.flg_45
        self.matricula_45 =  [df_tasa45.loc[flag_45, 'Matricula']]
        return None
    
    def predict(self, df_embar_original, df_prio_embar, df_prio_dest, df_cap_plant_original, df_carenas, df_mal_tiempo, df_max_dias_pico, df_porc_veloc_descanso, df_descarga_competencia, df_cbod_competencia, df_emb_omitir, df_conversion_frio, df_tasa45, df_money, df_costos_embar, df_cuota_nacional, df_capex_plantas, df_capex_frio, df_resto_tasa, df_costo_extraccion, df_master_eps, df_ultimo_dia_temporadas):  # CAMILO
        """
        Calcula los inputs necesarios para encontrar la planta de retorno.
        """
        self.df_emb_omitir_copy = df_emb_omitir.copy()
        self.df_master_eps = df_master_eps.copy()
        self.df_conversion_frio_copy = df_conversion_frio.copy()
        # self.df_tasa45_copy = df_tasa45.copy()
        self.df_costos_embar = df_costos_embar.copy()
        self.df_money_copy = df_money.copy()
        self.df_cuota_nacional_copy = df_cuota_nacional.copy()
        self.df_capex_plantas = df_capex_plantas.copy()
        self.df_capex_frio = df_capex_frio.copy()
        self.df_resto_tasa = df_resto_tasa.copy()
        self.df_costo_extraccion = self.clean_extraccion(df_costo_extraccion)
        self.df_ultimo_dia_temporadas = df_ultimo_dia_temporadas.copy()

        df_embar = self.preprocessing_df_embar(df_embar_original)[[self.name_fecha, self.id_embar, self.id_zona_pesca, self.name_mat_prima_descar, self.name_temporada, 'duration_marea', self.name_ind_prop, self.name_planta]]
        mask = df_embar["Ind Propiedad"] == 'Propia'
        self.df_vol_propio_real = df_embar[mask].groupby([self.name_temporada, self.id_embar], as_index=False)["volumen"].sum()
        
        self.df_prio_embar_copy = self.preprocessing_df_prio_embar(df_prio_embar)
        
        df_estimate = pd.concat([self.estimate_aditional_fishing(df_embar, temporada, df_carenas) for temporada in self.temporadas_elegidas]).drop_duplicates([self.name_fecha, self.id_embar])
        df_descarga_competencia = self.preprocessing_competence(df_descarga_competencia, df_cbod_competencia)
        
        mask = df_embar[self.id_embar] == '22029'
        df_barco_nuevo = pd.concat([self.estimate_fishin_new_ship(df_embar[mask], temporada, df_carenas) for temporada in self.temporadas_elegidas]).drop_duplicates([self.name_fecha, self.id_embar])
        df_barco_nuevo[self.id_embar] = '22030'
        # df_barco_nuevo['volumen'] = df_barco_nuevo['volumen'] * 1.496

        df_barco_nuevo['Ind Propiedad'] = 'Propia'
        df_barco_nuevo['flag_dia_adicional'] = 1
        del df_barco_nuevo['CBOD']
        del df_barco_nuevo['Ind Propiedad']
        self.df_barco_nuevo = df_barco_nuevo
        
        df_out_temp = df_descarga_competencia[~df_descarga_competencia['Fecha'].isin(df_embar['fecha_produccion'].unique())]
        df_out_temp = df_out_temp.groupby(['Temporada', 'Fecha', self.id_zona_pesca], as_index=False)['rendimiento'].mean()
        cantidad_embarcaciones = df_embar[df_embar['Ind Propiedad'] == 'Propia'].groupby(['temporada', 'fecha_produccion'], as_index=False)['id_embarcacion'].nunique().groupby(['temporada'], as_index=False).agg(
            cant_embar=('id_embarcacion', 'median'),
            ultimo_dia=('fecha_produccion', 'max')
        )
        cantidad_embarcaciones['cant_embar'] = cantidad_embarcaciones['cant_embar'].astype(int)
        df_out_temp.rename(columns={'Temporada':'temporada'}, inplace=True)
        df_out_temp = pd.merge(df_out_temp, cantidad_embarcaciones, on=['temporada'], how='left')
        df_out_temp = df_out_temp[df_out_temp['Fecha'] > df_out_temp['ultimo_dia']].reset_index(drop=True)

        ep_cbod = df_estimate[['id_embarcacion', 'CBOD']].drop_duplicates(['id_embarcacion', 'CBOD']).sort_values('CBOD', ascending=False).reset_index(drop=True)
        
        df_out_temp[ep_cbod['id_embarcacion'].unique().tolist()] = ep_cbod['CBOD'].unique().tolist()
        df_out_temp[ep_cbod['id_embarcacion'].unique().tolist()] = df_out_temp[ep_cbod['id_embarcacion'].unique().tolist()] * df_out_temp['rendimiento'].to_numpy().reshape(-1, 1) * self.ratio_out_temp
        
        df_out_temp = pd.melt(df_out_temp, id_vars=['temporada', 'Fecha', 'zona_de_pesca_id'], value_vars=ep_cbod['id_embarcacion'].unique().tolist(), var_name='id_embarcacion', value_name='volumen')
        df_out_temp = df_out_temp[~df_out_temp[['id_embarcacion', 'Fecha']].duplicated()]

        df_out_temp = df_out_temp.sample(n=1200, random_state=123).sort_values('Fecha').reset_index(drop=True)

        df_out_temp['Ind Propiedad'] = 'Propia'
        df_out_temp['flag_dia_adicional'] = 1

        df_out_temp.rename(columns={'Fecha':'fecha_produccion'}, inplace=True)
        df_out_temp['es_adicional'] = 1

        df_embar.drop(['duration_marea'], axis=1, inplace=True)
        df_embar['flag_dia_adicional'] = 0
        df_estimate['flag_dia_adicional'] = 1
        # df_embar = pd.concat([df_embar, df_estimate], ignore_index=True)
        df_embar = pd.concat([df_embar, df_estimate, df_out_temp], ignore_index=True)
        df_embar.drop(columns=['CBOD'], inplace=True)
        df_embar = df_embar[df_embar[self.name_fecha].notna()]
        
        self.df_prio_dest_copy = self.preprocessing_df_prio_dest(df_prio_dest)

        df_cap_plant = self.preprocessing_df_cap_plant(df_cap_plant_original)
        df_embar = self.filter_by_temporada(df_embar=df_embar)
        
        df_porc_veloc_descanso[self.temporadas_elegidas] = df_porc_veloc_descanso[self.temporadas_elegidas].clip(upper=0.8999)
        self.df_porc_veloc_descanso = df_porc_veloc_descanso
        self.df_max_dias_pico = df_max_dias_pico

        df_update_planta = self.create_update_planta(df_cap_plant, df_embar)
        
        df_mal_tiempo['dates_cierre'] = pd.to_datetime(df_mal_tiempo['CIERRE'].dt.date)
        to_replace = {'VEGUETA':'Vegueta', 'SUPE':'Supe', 'SAMANCO':'Samanco', 'CALLAO':'Callao', 'PISCO':'Pisco S'}
        df_mal_tiempo.replace({'PUERTO':to_replace}, inplace=True)
        df_mal_tiempo.drop_duplicates(['PUERTO', 'dates_cierre'], inplace=True)
        df_update_planta = pd.merge(df_update_planta, df_mal_tiempo[['PUERTO', 'dates_cierre']], left_on=[self.name_planta, self.name_fecha], right_on=['PUERTO', 'dates_cierre'], how='left')

        df_update_planta.loc[df_update_planta['PUERTO'].notna(), self.name_cap_max] = 0.000001
        df_update_planta.drop(columns=['PUERTO', 'dates_cierre'], inplace=True)
        loop_cols = [
            self.name_fecha,
            self.id_embar,
            self.name_mat_prima_descar,
            self.name_temporada,
            self.id_zona_pesca,
            'flag_dia_adicional',
            # 'es_adicional',
            # self.name_planta
        ] 
        # + self.cols_prio.tolist()
        out_cols = df_embar.columns.difference(loop_cols)
        
        self.df_embar_out = df_embar[out_cols]
        df_embar = df_embar[loop_cols]
        df_embar.loc[:, self.name_planta_sugerida] = np.nan

        self.dict_df_embar = {
            tempor: df_embar[df_embar[self.name_temporada] == tempor]
            for tempor in self.temporadas_elegidas
        }
        self.dict_update_plant = {
            tempor: df_update_planta[df_update_planta[self.name_temporada] == tempor]
            for tempor in self.temporadas_elegidas
        }
        self.dict_update_model_vol = {
            tempor: df_embar.loc[df_embar[self.name_temporada] == tempor, [self.name_fecha, self.name_mat_prima_descar]].drop_duplicates(self.name_fecha)
            for tempor in self.temporadas_elegidas
        }
        del df_embar
        del df_update_planta

        return None

    def predict_plant(self, temporada): # CAMILO
        """
        Calcula la planta de retorno
        """
        # Para Escenarios
        matriculas_excluir = self.get_matriculas_excluir(self.df_emb_omitir_copy, self.df_master_eps)
        matriculas_conver_frio, cbod_frio = self.get_matriculas_conversion_frio(self.df_conversion_frio_copy)
        # self.get_matricula_tasa_45(self.df_tasa45_copy)

        df_prio_embar_copy = self.df_prio_embar_copy.copy()
        
        df_embar_tprd = self.dict_df_embar[temporada]
        # print("df_embar_tprd", df_embar_tprd.shape, self.num_embar, temporada, df_embar_tprd['volumen'].sum())
        
        df_barco_nuevo = self.df_barco_nuevo.copy()
        mask = df_barco_nuevo[self.name_temporada] == temporada
        df_barco_nuevo_copy = df_barco_nuevo[mask]

        lista_barco_nuevo = []
        if self.flg_barco_nuevo == 1:
            df_embar_tprd = pd.concat([df_embar_tprd, df_barco_nuevo_copy], axis=0, ignore_index=True)
            lista_barco_nuevo = ['22030']
        df_embar_tprd = self.set_priority_embar(df_embar_tprd, df_prio_embar_copy, self.estrategia, matriculas_conver_frio, cbod_frio)

        ## Switch Fríos como Sin Fríos
        # mask_tipo = df_embar_tprd[self.name_tip_embar] == 'Propio con Frio'
        # df_embar_tprd.loc[mask_tipo, self.name_tip_embar] = 'Propio sin Frio'
        
        df_embar_tprd = self.get_1_cala(df_embar_tprd, self.df_prio_dest_copy)

        df_embar_tprd.sort_values(
            [self.name_fecha, self.name_sort_tipo_embarc, self.name_cbod],
            inplace=True,  ascending=[True, True, False]
        )

        df_embar_tprd['flag_termino'] = 0
        cuota_tasa = self.df_cuota_nacional_copy.loc[self.df_cuota_nacional_copy['temporada'] == temporada, 'Cuota Nacional'].item() * self.porc_cuota_tasa
        
        vol_tasa = np.minimum(cuota_tasa, df_embar_tprd.loc[(df_embar_tprd[self.id_embar].isin(self.ids_embar_prop + lista_barco_nuevo)) & (df_embar_tprd['flag_dia_adicional'] == 0), self.name_mat_prima_descar].sum())
        
        df_embar_tprd = df_embar_tprd.loc[~df_embar_tprd[self.id_embar].isin(matriculas_excluir), :]
        
        self.matricula_45 = '22029'
        # df_embar_tprd = df_embar_tprd.loc[~df_embar_tprd[self.id_embar].isin([self.matricula_45]), :]

        # df_embar_tprd.reset_index(inplace=True, drop=True)
        dates = df_embar_tprd[self.name_fecha].unique()
        df_status_planta = self.dict_update_plant[temporada].copy()
        # print("df_status_planta volumen actual call", temporada, df_status_planta['vol_actual'].sum())
        
        """Capacidad ampliada Malabrigo"""
        if self.cap_malabrigo > 1:
            cap_base_cap_malabrigo = df_status_planta.loc[df_status_planta['planta'] == 'Malabrigo', self.name_cap_max]
            df_status_planta.loc[df_status_planta['planta'] == 'Malabrigo', self.temporadas_elegidas + [self.name_cap_max]] = np.maximum(self.cap_malabrigo * df_status_planta.loc[df_status_planta['planta'] == 'Malabrigo', self.name_cap_max].head(1).item(), cap_base_cap_malabrigo)
        
        elif self.cap_malabrigo == 1:
            pass
        else:
            df_status_planta.loc[df_status_planta['planta'] == 'Malabrigo', self.temporadas_elegidas + [self.name_cap_max]] = np.maximum(self.cap_malabrigo, 0.000001)
            
        """Capacidad ampliada Chimbote"""
        if self.cap_chimbote > 1:
            cap_base_cap_chimbote = df_status_planta.loc[df_status_planta['planta'] == 'Chimbote', self.name_cap_max]
            df_status_planta.loc[df_status_planta['planta'] == 'Chimbote', self.temporadas_elegidas + [self.name_cap_max]] = np.maximum(self.cap_chimbote * df_status_planta.loc[df_status_planta['planta'] == 'Chimbote', self.name_cap_max].head(1).item(), cap_base_cap_chimbote)
        
        elif self.cap_chimbote == 1:
            pass
        else:
            df_status_planta.loc[df_status_planta['planta'] == 'Chimbote', self.temporadas_elegidas + [self.name_cap_max]] = np.maximum(self.cap_chimbote, 0.000001)

        """Capacidad ampliada Samanco"""
        if self.cap_samanco > 1:
            cap_base_cap_samanco = df_status_planta.loc[df_status_planta['planta'] == 'Samanco', self.name_cap_max]
            df_status_planta.loc[df_status_planta['planta'] == 'Samanco', self.temporadas_elegidas + [self.name_cap_max]] = np.maximum(self.cap_samanco * df_status_planta.loc[df_status_planta['planta'] == 'Samanco', self.name_cap_max].head(1).item(), cap_base_cap_samanco)
        
        elif self.cap_samanco == 1:
            pass
        else:
            df_status_planta.loc[df_status_planta['planta'] == 'Samanco', self.temporadas_elegidas + [self.name_cap_max]] = np.maximum(self.cap_samanco, 0.000001)

        """Capacidad ampliada Supe"""
        if self.cap_supe > 1:
            cap_base_cap_supe = df_status_planta.loc[df_status_planta['planta'] == 'Supe', self.name_cap_max]
            df_status_planta.loc[df_status_planta['planta'] == 'Supe', self.temporadas_elegidas + [self.name_cap_max]] = np.maximum(self.cap_supe * df_status_planta.loc[df_status_planta['planta'] == 'Supe', self.name_cap_max].head(1).item(), cap_base_cap_supe)
        
        elif self.cap_supe == 1:
            pass
        else:
            df_status_planta.loc[df_status_planta['planta'] == 'Supe', self.temporadas_elegidas + [self.name_cap_max]] = np.maximum(self.cap_supe, 0.000001)

        """Capacidad ampliada Vegueta"""
        if self.cap_vegueta > 1:
            cap_base_cap_vegueta = df_status_planta.loc[df_status_planta['planta'] == 'Vegueta', self.name_cap_max]
            df_status_planta.loc[df_status_planta['planta'] == 'Vegueta', self.temporadas_elegidas + [self.name_cap_max]] = np.maximum(self.cap_vegueta * df_status_planta.loc[df_status_planta['planta'] == 'Vegueta', self.name_cap_max].head(1).item(), cap_base_cap_vegueta)
        
        elif self.cap_vegueta == 1:
            pass
        else:
            df_status_planta.loc[df_status_planta['planta'] == 'Vegueta', self.temporadas_elegidas + [self.name_cap_max]] = np.maximum(self.cap_vegueta, 0.000001)

        """Capacidad ampliada Callao"""
        if self.cap_callao > 1:
            cap_base_cap_callao = df_status_planta.loc[df_status_planta['planta'] == 'Callao', self.name_cap_max]
            df_status_planta.loc[df_status_planta['planta'] == 'Callao', self.temporadas_elegidas + [self.name_cap_max]] = np.maximum(self.cap_callao * df_status_planta.loc[df_status_planta['planta'] == 'Callao', self.name_cap_max].head(1).item(), cap_base_cap_callao)
        
        elif self.cap_callao == 1:
            pass
        else:
            df_status_planta.loc[df_status_planta['planta'] == 'Callao', self.temporadas_elegidas + [self.name_cap_max]] = np.maximum(self.cap_callao, 0.000001)

        """Capacidad ampliada Pisco S"""
        if self.cap_pisco_s > 1:
            cap_base_cap_pisco_s = df_status_planta.loc[df_status_planta['planta'] == 'Pisco S', self.name_cap_max]
            df_status_planta.loc[df_status_planta['planta'] == 'Pisco S', self.temporadas_elegidas + [self.name_cap_max]] = np.maximum(self.cap_pisco_s * df_status_planta.loc[df_status_planta['planta'] == 'Pisco S', self.name_cap_max].head(1).item(), cap_base_cap_pisco_s)
        
        elif self.cap_pisco_s == 1:
            pass
        else:
            df_status_planta.loc[df_status_planta['planta'] == 'Pisco S', self.temporadas_elegidas + [self.name_cap_max]] = np.maximum(self.cap_pisco_s, 0.000001)
        
        if (self.cap_malabrigo + self.cap_chimbote + self.cap_samanco + self.cap_supe + self.cap_vegueta + self.cap_callao + self.cap_pisco_s) != 8:
            df_status_planta.loc[df_status_planta['planta'] == 'Pisco N', self.temporadas_elegidas + [self.name_cap_max]] = 0.000001
            df_embar_tprd = df_embar_tprd.loc[~df_embar_tprd[self.id_embar].isin([self.matricula_45]), :]

        df_embar_tprd.reset_index(inplace=True, drop=True)
        condition = False
        df_update_model_vol = self.dict_update_model_vol[temporada]
        df_update_model_vol.loc[:, self.name_mat_prima_descar] = 0
        # print("df_update_model_vol", temporada, df_update_model_vol["volumen"].sum())
        # print("df_status_planta volumen actual loop", temporada, df_status_planta['vol_actual'].sum())
        cont = df_status_planta.loc[:, self.name_contador] = 0
        for cd in dates:
            """Recorriendo para cada día"""
            mask0 = df_status_planta[self.name_fecha] == cd
            df_status_planta.loc[mask0, self.name_cap_max] = np.where(
                condition,
                df_status_planta.loc[mask0, 'capacidad_max']
                * self.df_porc_veloc_descanso[temporada].to_numpy(),
                df_status_planta.loc[mask0, self.name_cap_max]
            )

            df_cur_embar = df_embar_tprd[df_embar_tprd[self.name_fecha] == cd].reset_index(
                drop=True
            )
            n_cur_embar = df_cur_embar[self.id_embar].unique()

            for idb in n_cur_embar:
                """Recorriendo para cada embarcación"""
                df_status_planta_cur = df_status_planta[
                    df_status_planta[self.name_fecha] == cd
                ].set_index(self.name_planta)

                vol = df_cur_embar.loc[
                    df_cur_embar[self.id_embar] == idb, self.name_mat_prima_descar
                ].item()
                cols_prio_disp = drop_no_priorities(
                    df_cur_embar[self.cols_prio][df_cur_embar[self.id_embar] == idb]
                ).columns
                for prio_plan in cols_prio_disp:
                    """Recorriendo las prioridades disponibles"""
                    prio_cur = df_cur_embar.loc[
                        df_cur_embar[self.id_embar] == idb, prio_plan
                    ].item()
                    dispon = (
                        df_status_planta_cur.loc[prio_cur, self.name_disponibilidad]
                    ) & (
                        (df_status_planta_cur.loc[prio_cur, self.name_vol_actual]) + vol
                        < (df_status_planta_cur.loc[prio_cur, self.name_cap_max])
                    )
                    if dispon:
                        mask = (df_status_planta[self.name_fecha] == cd) & (
                            df_status_planta[self.name_planta] == prio_cur
                        )
                        df_status_planta.loc[mask, self.name_vol_actual] = (
                            df_status_planta.loc[mask, self.name_vol_actual] + vol
                        )
                        mask2 = (df_embar_tprd[self.name_fecha] == cd) & (
                            df_embar_tprd[self.id_embar] == idb
                        )
                        df_embar_tprd.loc[mask2, self.name_planta_sugerida] = prio_cur
                        df_status_planta = self.update_current_vol(df_status_planta)
                        break
                
            df_status_planta.loc[mask0, self.name_logical_diario] = (
                df_status_planta.loc[mask0, self.name_vol_actual]
                >= df_status_planta.loc[
                    mask0, temporada
                ]
                * self.ratio_cap_max
            )

            df_status_planta.loc[mask0, self.name_contador] = np.where(
                (
                    df_status_planta.loc[mask0, self.name_logical_diario].to_numpy()
                    == False
                ),
                0,
                cont
                + df_status_planta.loc[mask0, self.name_logical_diario]
                .astype(int)
                .values,
            )
            cont = df_status_planta.loc[mask0, self.name_contador]

            condition = (
                df_status_planta.loc[mask0, self.name_contador]
                == self.df_max_dias_pico[temporada].to_numpy()
            )
            mask_vol = df_update_model_vol[self.name_fecha] == cd
            mask_tasa = (df_embar_tprd[self.name_fecha] == cd) & (df_embar_tprd[self.name_planta_sugerida].notna()) & (df_embar_tprd[self.id_embar].isin(self.ids_embar_prop + lista_barco_nuevo))
            try:
                df_update_model_vol.loc[mask_vol, self.name_mat_prima_descar] = df_embar_tprd.loc[mask_tasa, [self.name_fecha, self.name_mat_prima_descar]].groupby(self.name_fecha)[self.name_mat_prima_descar].sum().item()
            except:
                df_update_model_vol.loc[mask_vol, self.name_mat_prima_descar] = 0
            vol_acum = df_update_model_vol.loc[:, self.name_mat_prima_descar].sum()
            if vol_acum > vol_tasa:
                mask_day = np.cumsum(df_embar_tprd.loc[mask_tasa, self.name_mat_prima_descar][::-1]) > (vol_acum - vol_tasa)
                changed_day = mask_day.lt(~mask_day).idxmin()
                mask_day.loc[changed_day] = False
                df_embar_tprd.loc[(df_embar_tprd.index > changed_day), self.name_planta_sugerida] = np.nan
                df_embar_tprd.loc[(df_embar_tprd.index > changed_day), 'flag_termino'] = 1
                # print("Primer Return")
                # print("df_update_model_vol", temporada, df_update_model_vol["volumen"].sum())
                # print("df_embar_tprd_actual", df_embar_tprd.shape, self.num_embar, temporada, df_embar_tprd['volumen'].sum())
                # print('Exclusivo', df_embar_tprd.loc[df_embar_tprd['sort_tipo_embarc'] == '3 Exclusivo', 'volumen'].sum())
                # print('3 Preferente', df_embar_tprd.loc[df_embar_tprd['sort_tipo_embarc'] == '3 Preferente', 'volumen'].sum())
                # print('3 Eventual', df_embar_tprd.loc[df_embar_tprd['sort_tipo_embarc'] == '3 Eventual', 'volumen'].sum())
                # print('3 Otros', df_embar_tprd.loc[df_embar_tprd['sort_tipo_embarc'] == '3 Otros', 'volumen'].sum())
                # print('Propio sin Frio', df_embar_tprd.loc[df_embar_tprd['sort_tipo_embarc'] == 'Propio sin Frio', 'volumen'].sum())
                # print('Propio con Frio', df_embar_tprd.loc[df_embar_tprd['sort_tipo_embarc'] == 'Propio con Frio', 'volumen'].sum())
                # print("------------------------------------------------------------------------------")
                # df_status_planta.to_excel(f'outputs/df_status_planta_{temporada}.xlsx')
                return df_embar_tprd
            df_update_model_vol.loc[mask_vol, 'vol_acum'] = df_update_model_vol.loc[:, self.name_mat_prima_descar].sum()
        # df_status_planta.to_excel(f'outputs/df_status_planta_{temporada}_2.xlsx')
        # print("Segundo Return")
        # print("df_update_model_vol", temporada, df_update_model_vol["volumen"].sum())
        # print("df_embar_tprd_actual", df_embar_tprd.shape, self.num_embar, temporada, df_embar_tprd['volumen'].sum())
        # print('Exclusivo', df_embar_tprd.loc[df_embar_tprd['sort_tipo_embarc'] == '3 Exclusivo', 'volumen'].sum())
        # print('3 Preferente', df_embar_tprd.loc[df_embar_tprd['sort_tipo_embarc'] == '3 Preferente', 'volumen'].sum())
        # print('3 Eventual', df_embar_tprd.loc[df_embar_tprd['sort_tipo_embarc'] == '3 Eventual', 'volumen'].sum())
        # print('3 Otros', df_embar_tprd.loc[df_embar_tprd['sort_tipo_embarc'] == '3 Otros', 'volumen'].sum())
        # print('Propio sin Frio', df_embar_tprd.loc[df_embar_tprd['sort_tipo_embarc'] == 'Propio sin Frio', 'volumen'].sum())
        # print('Propio con Frio', df_embar_tprd.loc[df_embar_tprd['sort_tipo_embarc'] == 'Propio con Frio', 'volumen'].sum())
        # print("------------------------------------------------------------------------------")
        return df_embar_tprd

    def preprocessing_df_money(self, df_money): # MARCO
        """Realiza el procesamiento de la data input para los kpis"""
        df_money_copy = df_money.copy()
        to_replace = {
            '01. Malabrigo':'Malabrigo',
            '02. Chimbote':'Chimbote',
            '03. Samanco':'Samanco',
            '04. Supe':'Supe',
            '05. Vegueta':'Vegueta',
            '06. Callao':'Callao',
            '08. Pisco Sur':'Pisco S',
        }

        df_money_copy.replace({'Planta':to_replace}, inplace=True)

        df_money_copy.rename(columns={'Temporada':'temporada', 'Planta':'planta_sugerida'}, inplace=True)

        to_replace = {
            '2018 - I':'2018-I',
            '2018 - II':'2018-II',
            '2019 - I':'2019-I',
            '2019 - II':'2019-II',
            '2020 - I':'2020-I',
            '2020 - II':'2020-II',
            '2021 - I':'2021-I',
            '2021 - II':'2021-II',
            '2022 - I':'2022-I',
        }

        df_money_copy.replace({'temporada':to_replace}, inplace=True)
        return df_money_copy
    
    def clean_extraccion(self, df_costo_extraccion): # MARCO
        df_costo_extraccion_copy = df_costo_extraccion.copy()
        to_replace = {
            '2018 - I':'2018-I',
            '2018 - II':'2018-II',
            '2019 - I':'2019-I',
            '2019 - II':'2019-II',
            '2020 - I':'2020-I',
            '2020 - II':'2020-II',
            '2021 - I':'2021-I',
            '2021 - II':'2021-II',
            '2022 - I':'2022-I',
        }

        df_costo_extraccion_copy.replace({'temporada':to_replace}, inplace=True)
        return df_costo_extraccion_copy

    def clean_revenue(self, df_revenue, name_revenue): # MARCO
        df_revenue_copy = df_revenue.reset_index().copy()
        df_revenue_copy.rename(columns={0:name_revenue}, inplace=True)

        to_replace = {
            '2018-I':'2018',
            '2018-II':'2018',
            '2019-I':'2019',
            '2019-II':'2019',
            '2020-I':'2020',
            '2020-II':'2020',
            '2021-I':'2021',
            '2021-II':'2021',
            '2022-I':'2022'
        }
        df_revenue_copy.replace({'temporada':to_replace}, inplace=True)

        df_revenue_copy.rename(columns={'temporada':'year'}, inplace=True)


        df_revenue_copy = df_revenue_copy.groupby('year', as_index=False)[name_revenue].sum()
        df_revenue_copy.reset_index(inplace=True)
        df_revenue_copy.rename(columns={'index':'time'}, inplace=True)
        df_revenue_copy['time'] = df_revenue_copy['time'] + 1
        mask = df_revenue_copy['time'] == 5
        df_revenue_copy.loc[mask, 'time'] = 4.5
        df_revenue_copy[f'{name_revenue}_pv'] = df_revenue_copy[name_revenue] / ((1 + self.r_ratio ) ** df_revenue_copy['time'])
        return df_revenue_copy

    def get_kpis(self, df_flagged_initial, df_money): # MARCO
        """
        Calculo los kpis para output final
        """
        # cols = [
        #     "fecha_produccion",
        #     "id_embarcacion",
        #     "volumen",
        #     "temporada",
        #     "zona_de_pesca_id",
        #     "flag_dia_adicional",
        #     "planta",
        #     "planta_sugerida",
        #     "Orden_de_Prioridad",
        #     # "Embarc",
        #     # "Tipo Tr",
        #     # "CBOD",
        #     "sort_tipo_embarc",
        #     "Zona de Pesca",
        #     "Prioridad 1",
        #     "Prioridad 2",
        #     "Prioridad 3",
        #     "Prioridad 4",
        #     "Prioridad 5",
        #     "Prioridad 6",
        #     "Prioridad 7",
        #     "Prioridad 8",
        #     "flag_termino"
        # ]
        # path = 'outputs/'
        # file = 'df_ultimo_dia_temporadas.xlsx'
        # df_ultimo_dia_temporadas = pd.read_excel(path + file)
        df_ultimo_dia_temporadas = self.df_ultimo_dia_temporadas
        
        if self.flg_barco_nuevo == 1:
            capex_barco_nuevo = 9000000
        else:
            capex_barco_nuevo = 0
        
        # print('a) Volumen Total', df_flagged_initial.loc[:, 'volumen'].sum(), df_flagged_initial.shape)
        df_flagged = pd.merge(df_flagged_initial, df_ultimo_dia_temporadas[['temporada', 'ultimo_dia']], on=['temporada'], how='left')
        df_flagged.to_excel('outputs/df_tabla_operativa_cuota_adicional.xlsx')
        mask = (df_flagged['fecha_produccion'] > df_flagged['ultimo_dia']) & (df_flagged['sort_tipo_embarc'].isin(['Propio con Frio', 'Propio sin Frio'])) & (df_flagged['planta_sugerida'].notna()) & (df_flagged['flag_termino'] == 0)
        # print('b) Volumen modelo Propio', df_flagged.loc[mask, 'volumen'].sum())
        
        df_flagged_resumen = df_flagged.loc[mask, :].groupby(['temporada']).agg(
            cant_dias_adicionales=('fecha_produccion', 'nunique'),
            cant_eps_adicionales=('id_embarcacion', 'nunique'),
            vol_total=('volumen', 'sum')
        )
        cant_dias_adicionales = df_flagged_resumen['cant_dias_adicionales'].to_dict()
        vol_total_adicional = df_flagged_resumen['vol_total'].to_dict()
        # df_flagged[cols].to_excel('outputs/df_base_restriccion_frios.xlsx')
        df_money_copy = self.preprocessing_df_money(df_money)
        df_costos_embar = self.df_costos_embar
        df_capex_frio = self.df_capex_frio
        df_resto_tasa = self.df_resto_tasa
        
        to_replace = {
            '2018 - I':'2018-I',
            '2018 - II':'2018-II',
            '2019 - I':'2019-I',
            '2019 - II':'2019-II',
            '2020 - I':'2020-I',
            '2020 - II':'2020-II',
            '2021 - I':'2021-I',
            '2021 - II':'2021-II',
            '2022 - I':'2022-I',
        }
        df_costos_embar.replace({'temporada':to_replace}, inplace=True)
        df_resto_tasa.replace({'temporada':to_replace}, inplace=True)

        # self.get_matriculas_excluir(self.df_emb_omitir_copy, self.df_master_eps)
        matriculas_excluir = self.get_matriculas_excluir(self.df_emb_omitir_copy, self.df_master_eps)
        matriculas_conver_frio, cbod_frio = self.get_matriculas_conversion_frio(self.df_conversion_frio_copy)
        self.matricula_45 = '22029'
        # matriculas_excluir = self.df_emb_omitir_copy[:(48 - self.num_embar)].loc[:, 'Matricula'].tolist()
        ahorro_omision = df_costos_embar[df_costos_embar['Matricula'].isin(matriculas_excluir + [self.matricula_45])].groupby('temporada', dropna=False)['Liquid. Mantto'].sum()
        if ahorro_omision.sum() == 0:
            ahorro_omision = 0
        
        matriculas_conver_frio = self.df_conversion_frio_copy[:(self.num_eps_frio - 19)].loc[:, 'Matricula'].tolist()

        """Cálculo de la Liquid. Mantto"""
        mtto_frio = df_costos_embar[df_costos_embar['Matricula'].isin(matriculas_conver_frio)].groupby('temporada')['Liquid. Mantto'].sum()
        if mtto_frio.sum() == 0:
            mtto_frio = 0

        mtto_exclusion = df_costos_embar[df_costos_embar['Matricula'].isin(matriculas_excluir + [self.matricula_45])].groupby('temporada')['Liquid. Mantto'].sum()
        if mtto_exclusion.sum() == 0:
            mtto_exclusion = 0

        # potencial_ep_excluion = self.df_emb_omitir_copy['Matricula'].unique()
        mtto_ep_actuales = df_costos_embar.groupby('temporada')['Liquid. Mantto'].sum() - mtto_exclusion
        
        capex_frio = df_capex_frio.loc[df_capex_frio['Matricula'].isin(matriculas_conver_frio), 'CAPEX FRIO'].sum()


        filtro_samanco = []
        if self.cap_samanco == 0:
            filtro_samanco = ['Samanco']

        filtro_supe = []
        if self.cap_supe == 0:
            filtro_supe = ['Supe']

        """Cálculo de la PPto Admin"""
        ppto_admin_exclusion = df_costos_embar[df_costos_embar['Matricula'].isin(matriculas_excluir + [self.matricula_45])].groupby('temporada')['Ppto - Admin'].sum()
        if ppto_admin_exclusion.sum() == 0:
            ppto_admin_exclusion = 0
        ppto_admin = df_costos_embar.groupby('temporada')['Ppto - Admin'].sum() - ppto_admin_exclusion

        """Cálculo de la depreciación"""
        depre_barc_elegidos = df_costos_embar[df_costos_embar['Matricula'].isin(matriculas_excluir + [self.matricula_45])].groupby('temporada')['Depreciacion'].sum()
        if depre_barc_elegidos.sum() == 0:
            depre_barc_elegidos = 0

        depreciacion_barcos = df_costos_embar.groupby('temporada')['Depreciacion'].sum() - depre_barc_elegidos
        if depreciacion_barcos.sum() == 0:
            depreciacion_barcos = 0

        depreciacion_tasa = df_resto_tasa.groupby('temporada')['Depreciacion'].sum()

        depreciacion = depreciacion_barcos + depreciacion_tasa + df_money_copy[~df_money_copy['planta_sugerida'].isin(filtro_samanco + filtro_supe)].groupby('temporada')['Depreciacion'].sum()

        """Cálculo del capex"""
        capex_barc_elegidos = df_costos_embar[df_costos_embar['Matricula'].isin(matriculas_excluir + [self.matricula_45])].groupby('temporada')['Capex'].sum()
        if capex_barc_elegidos.sum() == 0:
            capex_barc_elegidos = 0

        capex_barcos = df_costos_embar.groupby('temporada')['Capex'].sum() - capex_barc_elegidos
        if capex_barcos.sum() == 0:
            capex_barcos = 0
        capex_tasa = df_resto_tasa.groupby('temporada')['Capex'].sum()

        capex = capex_barcos + capex_tasa + df_money_copy[~df_money_copy['planta_sugerida'].isin(filtro_samanco + filtro_supe)].groupby('temporada')['Capex'].sum()

        # Flag tiene planta sugerida
        mask_plant_notna = (df_flagged['planta_sugerida'].notna())

        # Flag días de pesca adicionales
        # mask_adicional = (df_flagged['flag_dia_adicional'] == 1)

        # Flag barcos de TASA
        mask_prop = (df_flagged['Tipo Tr'].isin(['Propio con Frio', 'Propio sin Frio']))

        # Flag término de temporada con días de pesca adicional
        mask_fin_tempor = (df_flagged['flag_termino'] == 1)

        mask_model = mask_plant_notna & ~mask_fin_tempor

        # df_flagged[mask_model].to_excel('outputs/df_flagged_modelo.xlsx')
        
        # print('c) Modelo', df_flagged.loc[mask_model, 'volumen'].sum())
        mp_descargado_propios = pd.pivot_table(df_flagged[mask_model & mask_prop].groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['volumen'].sum(), 
        columns='temporada', values='volumen', index='planta_sugerida', dropna=False, aggfunc=np.sum)
        
        mp_descargado_terceros = pd.pivot_table(df_flagged[mask_model & ~mask_prop].groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['volumen'].sum(), 
        columns='temporada', values='volumen', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        mp_descargado_total = pd.pivot_table(df_flagged[mask_model].groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['volumen'].sum(), 
        columns='temporada', values='volumen', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_rend_hp = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Rend. HP'].sum(), 
        columns='temporada', values='Rend. HP', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_rend_ap = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Rend. AP'].sum(), 
        columns='temporada', values='Rend. AP', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        produccion_hp = mp_descargado_total / df_rend_hp

        produccion_ap = mp_descargado_total * df_rend_ap

        df_calidad_sp = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Calidad SP (%)'].sum(), 
        columns='temporada', values='Calidad SP (%)', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_calidad_pr = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Calidad P (%)'].sum(), 
        columns='temporada', values='Calidad P (%)', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_calidad_taiwan = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Calidad TW (%)'].sum(), 
        columns='temporada', values='Calidad TW (%)', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_calidad_thailand = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Calidad TH (%)'].sum(), 
        columns='temporada', values='Calidad TH (%)', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_calidad_std = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Calidad STD (%)'].sum(), 
        columns='temporada', values='Calidad STD (%)', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_calidad_aqua = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Calidad Aqua (%)'].sum(), 
        columns='temporada', values='Calidad Aqua (%)', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_calidad_omega = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Calidad Omega (%)'].sum(), 
        columns='temporada', values='Calidad Omega (%)', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_precio_sp = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Precio SP'].sum(), 
        columns='temporada', values='Precio SP', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_precio_pr = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Precio P'].sum(), 
        columns='temporada', values='Precio P', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_precio_taiwan = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Precio TW'].sum(), 
        columns='temporada', values='Precio TW', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_precio_thailand = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Precio TH'].sum(), 
        columns='temporada', values='Precio TH', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_precio_std = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Precio STD'].sum(), 
        columns='temporada', values='Precio STD', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_precio_aqua = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Precio Aqua'].sum(), 
        columns='temporada', values='Precio Aqua', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_precio_omega = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Precio Omega'].sum(), 
        columns='temporada', values='Precio Omega', index='planta_sugerida', dropna=False, aggfunc=np.sum)
    

        precio_hp = df_calidad_sp * df_precio_sp + df_calidad_pr * df_precio_pr + df_calidad_taiwan * df_precio_taiwan + df_calidad_thailand * df_precio_thailand + df_calidad_std * df_precio_std

        precio_ap = df_calidad_aqua * df_precio_aqua + df_calidad_omega * df_precio_omega

        df_costo_var_unit_hp = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Costo Variable Unitario HP'].sum(), 
        columns='temporada', values='Costo Variable Unitario HP', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_costo_extra_unit = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Costo Extraccion Unitario'].sum(), 
        columns='temporada', values='Costo Extraccion Unitario', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_costo_terc_unit = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Costo Terceros Unitario'].sum(), 
        columns='temporada', values='Costo Terceros Unitario', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        # self.get_matriculas_conversion_frio(self.df_conversion_frio_copy)
        
        df_propios_modelo = df_flagged[mask_model & mask_prop]
        mask_nuevos_frios = df_propios_modelo[self.id_embar].isin(matriculas_conver_frio)

        if len(df_propios_modelo[mask_nuevos_frios].index) > 0:
            df_nuevos_frios = pd.pivot_table(df_propios_modelo[mask_nuevos_frios].groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['volumen'].sum(), 
            columns='temporada', values='volumen', index='planta_sugerida', dropna=False, aggfunc=np.sum).fillna(0)

            produccion_hp_frio = df_nuevos_frios.fillna(0) / df_rend_hp
        else:
            df_nuevos_frios = 0
            produccion_hp_frio = 0
        """"CAPEX Malabrigo"""
        if (self.cap_malabrigo == 3760) | (self.cap_malabrigo == 1):
            # capex_malabrigo = 3057042.24
            capex_malabrigo = 0
        elif np.isclose(self.cap_malabrigo, 1.05263157894):
            capex_malabrigo = 2942000
        elif np.isclose(self.cap_malabrigo, 1.105263157894):
            capex_malabrigo = 4522000
        elif np.isclose(self.cap_malabrigo, 1.15789473684):
            capex_malabrigo = 6526000
        elif np.isclose(self.cap_malabrigo, 1.210526315789):
            capex_malabrigo = 7072000
        elif np.isclose(self.cap_malabrigo, 1.263157894736):
            capex_malabrigo = 9044000
        elif np.isclose(self.cap_malabrigo, 1.115):
            capex_malabrigo = 3057042.24

        """"CAPEX Chimbote"""
        if np.isclose(self.cap_chimbote, 3838.4210526315) | (self.cap_chimbote == 1):
            # capex_chimbote = 2357042.24
            capex_chimbote = 0
        elif np.isclose(self.cap_chimbote, 1.153846153846):
            capex_chimbote = 7917816.50 # Cambiar por NUEVO CAPEX
        elif np.isclose(self.cap_chimbote, 1.123):
            capex_chimbote = 2357042.24
        elif np.isclose(self.cap_chimbote, 1.123 * 1.15384615):
            capex_chimbote = 2357042.24 + 7917816.50 # Cambiar por NUEVO CAPEX 

        """"CAPEX Samanco"""
        # if self.cap_samanco == 1:
        #     capex_samanco = 0
        # else:
        capex_samanco = 0

        """"CAPEX Supe"""
        capex_supe = 0

        """"CAPEX Vegueta"""
        if np.isclose(self.cap_vegueta, 2900) | (self.cap_vegueta == 1):
            # capex_vegueta = 234000
            capex_vegueta = 0
        elif np.isclose(self.cap_vegueta, 1.18571428571):
            capex_vegueta = 2855000
        elif np.isclose(self.cap_vegueta, 1.2857142857142):
            capex_vegueta = 8990000
        elif np.isclose(self.cap_vegueta, 1.364285714285):
            capex_vegueta = 10915000
        elif np.isclose(self.cap_vegueta, 1.414285714285):
            capex_vegueta = 13680000
        elif np.isclose(self.cap_vegueta, 1.48571428571):
            capex_vegueta = 17294000
        elif np.isclose(self.cap_vegueta, 1.557142857142):
            capex_vegueta = 17430000

        """"CAPEX Callao"""
        capex_callao = 0

        """"CAPEX Pisco S"""
        capex_pisco_s = 0

        # Por subida de barcos de frío
        df_ingresos_frio = produccion_hp_frio * 50 * 3 # 50 DOLARES UPGRADE CALIDAD 1 TM DE HP | MULTIPLICADO POR 3 EFECTO CABECEO MP (2 TM NORMALES + 1 DE FRÍO)
        df_egresos_frio = df_nuevos_frios * 15 # $15 POR TM DE PESCA  - CONSIDERA 25 GALONES DE DIESEL POR HORA Y 4 DOLARES POR GALÓN


        # Por bajada de barcos de frío (Volumen de frío perdido)
        df_volumen_real = pd.merge(self.df_vol_propio_real, self.df_prio_embar_copy[[self.id_embar, 'Tipo Tr']], on='id_embarcacion', how='left')
        df_split_frio_sin_frio = df_volumen_real.groupby(['Tipo Tr'], as_index=False)['volumen'].sum()
        mask = df_split_frio_sin_frio["Tipo Tr"] == 'Propio con Frio'
        volumen_frio_real_mp = df_split_frio_sin_frio.loc[mask, 'volumen'].sum()
        # df_split_frio_sin_frio["ratio"] = df_split_frio_sin_frio["volumen"] / df_split_frio_sin_frio["volumen"].sum()

        df_current_volumen = df_flagged[mask_model & mask_prop].groupby(["sort_tipo_embarc"], as_index=False)["volumen"].sum()
        mask = df_current_volumen["sort_tipo_embarc"] == 'Propio con Frio'
        volumen_frio_modelo_mp = df_current_volumen.loc[mask, 'volumen'].sum()

        vol_frio_perdido_hp = (volumen_frio_real_mp - volumen_frio_modelo_mp) / 4 # RENDIMIENTO
        perdidas_por_calidad = (vol_frio_perdido_hp * 50 * 3) / 9 # Por temporada


        if isinstance(df_egresos_frio, pd.DataFrame):
            df_egresos_frio = df_egresos_frio.fillna(0).sum()

        df_ingresos = produccion_hp.fillna(0) * precio_hp + produccion_ap.fillna(0) * precio_ap + df_ingresos_frio

        if self.cap_samanco == 0:
            mask = df_money_copy['planta_sugerida'] == 'Samanco'
            df_money_copy.loc[mask, 'Ppto - Admin'] = 0
            df_money_copy.loc[mask, 'Nóminas'] = df_money_copy.loc[mask, 'Nóminas'] * 0.3
            df_money_copy.loc[mask, 'Liquid. Mantto'] = df_money_copy.loc[mask, 'Liquid. Mantto'] * 0.5

        if self.cap_supe == 0:
            mask = df_money_copy['planta_sugerida'] == 'Supe'
            df_money_copy.loc[mask, 'Ppto - Admin'] = 0
            df_money_copy.loc[mask, 'Nóminas'] = df_money_copy.loc[mask, 'Nóminas'] * 0.3
            df_money_copy.loc[mask, 'Liquid. Mantto'] = df_money_copy.loc[mask, 'Liquid. Mantto'] * 0.5

        df_money_copy['costos_fijos'] = df_money_copy['Liquid. Mantto'] + df_money_copy['Nóminas'] + df_money_copy['Ppto - Admin']

        df_costo_fijos = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['costos_fijos'].sum(), 
        columns='temporada', values='costos_fijos', index='planta_sugerida', dropna=False, aggfunc=np.sum).sum() + mtto_frio + mtto_ep_actuales + ppto_admin

        df_cost_var = produccion_hp.fillna(0) * df_costo_var_unit_hp

        df_costos_mp_extrac = mp_descargado_propios.fillna(0) * df_costo_extra_unit

        df_costos_mp_terc = mp_descargado_terceros.fillna(0) * df_costo_terc_unit

        inversion_total = capex_malabrigo + capex_chimbote + capex_samanco + capex_supe + capex_vegueta + capex_callao + capex_pisco_s + capex_frio
        
        nueva_depreciacion = (inversion_total / 10) / 2 + (capex_barco_nuevo / 20) / 2 # Por 10 años y por 2 temporadas # TODO: 
        # nueva_depreciacion = np.repeat((inversion_total / 10) / 2, len(df_ingresos.sum()))
        # nueva_depreciacion[-1] = nueva_depreciacion[-1] * 2
        # depreciacion_nuevo_barco = (capex_barco_nuevo / 20) / 2
        df_costo_extraccion = self.df_costo_extraccion.copy()
        df_costo_extraccion['temporada'] = df_costo_extraccion['TEMPORADA'].str.replace(" ", "")
        df_costo_extraccion_res = df_costo_extraccion.groupby(["temporada"])['EXTRACCION'].sum() * mp_descargado_propios.sum()

        df_master_eps = self.df_master_eps.copy()
        df_master_eps = df_master_eps.set_index('Matricula')

        df_eps_temporada_volumen = pd.pivot_table(df_flagged[mask_model & mask_prop].groupby(['temporada', 'id_embarcacion'], as_index=False, dropna=False)['volumen'].sum(), 
        columns='temporada', values='volumen', index='id_embarcacion', dropna=False, aggfunc=np.sum).fillna(0)
        df_costo_combus_inicial = pd.merge(df_eps_temporada_volumen, df_master_eps['GAL_X_TM_PROM'], left_index=True, right_index=True, how='left')

        df_costo_combus_inicial[self.temporadas_elegidas] = df_costo_combus_inicial[self.temporadas_elegidas] * df_costo_combus_inicial['GAL_X_TM_PROM'].to_numpy().reshape(-1, 1) * 4
        del df_costo_combus_inicial["GAL_X_TM_PROM"]
        # df_costo_combustible = mp_descargado_propios.sum() * 13 * 4
        df_costo_combustible = df_costo_combus_inicial.sum()

        df_ebit_cf = df_ingresos.sum() - (df_costo_fijos + df_cost_var.sum()  + df_costos_mp_terc.sum() + df_costo_extraccion_res + df_costo_combustible + df_egresos_frio + perdidas_por_calidad) - depreciacion - nueva_depreciacion

        # TODO: Colocar en el EBIT y CaSHFLOWanual
        tasa_impuestos = 29.5 / 100
        
        cash_flow = df_ebit_cf * (1 - tasa_impuestos) + depreciacion - capex + nueva_depreciacion

        df_ebit_report = self.clean_revenue(df_ebit_cf, 'ebit')
        # df_ebit_report_pv = df_ebit_report['ebit_pv'].sum()
        df_ebit_report_prom = df_ebit_report['ebit'].mean()


        df_cash_flow = self.clean_revenue(cash_flow, 'cash_flow')
        cash_flow_pv = df_cash_flow['cash_flow_pv'].sum()
        cash_flow_prom = df_cash_flow['cash_flow'].mean()
        cf_vr_year4 = cash_flow_prom * (1 + self.g_ratio) / (self.r_ratio - self.g_ratio)
        cf_vr_pv = cf_vr_year4 / ((1 + self.r_ratio) ** 4)
        
        cant_barcos_a_bajar = len(self.num_embar)

        # gastos_cierre = 1400000 * self.n_plant_cerradas + 100000 * (48 - cant_barcos_actuales)
        gastos_cierre = 1400000 * self.n_plant_cerradas + 100000 * (cant_barcos_a_bajar)

        cf_npv = cash_flow_pv + cf_vr_pv - inversion_total - gastos_cierre - capex_barco_nuevo

        market_share = (mp_descargado_propios.sum().mean() + mp_descargado_terceros.sum().mean()) / self.df_cuota_nacional_copy['Descarga Nacional'].mean()
        market_terceros = (mp_descargado_terceros.sum().mean()) / self.df_cuota_nacional_copy['Descarga Nacional'].mean()
        cumpli_cuota_propia = mp_descargado_propios.sum().mean() / (self.df_cuota_nacional_copy['Descarga Nacional'].mean() * self.porc_cuota_tasa)
        names = [self.num_embar, self.num_eps_frio, self.flg_45, self.estrategia, self.cap_malabrigo, self.cap_chimbote, self.cap_samanco, self.cap_supe, self.cap_vegueta, self.cap_callao, self.cap_pisco_s, self.flg_barco_nuevo]
        return names, df_ingresos.sum().sum(), np.sum(ahorro_omision), 0, 0, 0, produccion_hp.sum().sum(), produccion_ap.sum().sum(), df_costo_fijos.sum().sum(), df_cost_var.sum().sum(), df_costos_mp_extrac.sum().sum(), df_costos_mp_terc.sum().sum(), np.sum(mtto_frio), 0, inversion_total, gastos_cierre, cash_flow_prom, market_share, market_terceros, cumpli_cuota_propia, mp_descargado_propios.sum().sum(), mp_descargado_terceros.sum().sum(), cf_npv.sum(), cash_flow_pv.sum(), cf_vr_pv, depreciacion.sum(), capex.sum(), nueva_depreciacion, cant_dias_adicionales, vol_total_adicional, df_ebit_report_prom

    def get_kpis_cash_flow_year(self, df_flagged, df_money):
        """
        Calculo los kpis para output final
        """
        # df_flagged.to_excel('outputs/df_flagged_optimo.xlsx')
        df_money_copy = self.preprocessing_df_money(df_money)
        df_costos_embar = self.df_costos_embar
        df_capex_frio = self.df_capex_frio
        df_resto_tasa = self.df_resto_tasa
        
        to_replace = {
            '2018 - I':'2018-I',
            '2018 - II':'2018-II',
            '2019 - I':'2019-I',
            '2019 - II':'2019-II',
            '2020 - I':'2020-I',
            '2020 - II':'2020-II',
            '2021 - I':'2021-I',
            '2021 - II':'2021-II',
        }
        df_costos_embar.replace({'temporada':to_replace}, inplace=True)
        df_resto_tasa.replace({'temporada':to_replace}, inplace=True)
        matriculas_excluir = self.df_emb_omitir_copy[:(48 - self.num_embar)].loc[:, 'Matricula'].tolist()
        ahorro_omision = df_costos_embar[df_costos_embar['Matricula'].isin(matriculas_excluir)].groupby('temporada', dropna=False)['Liquid. Mantto'].sum()
        if ahorro_omision.sum() == 0:
            ahorro_omision = 0
        
        matriculas_conver_frio = self.df_conversion_frio_copy[:(self.num_eps_frio - 19)].loc[:, 'Matricula'].tolist()

        mtto_frio = df_costos_embar[df_costos_embar['Matricula'].isin(matriculas_conver_frio)].groupby('temporada')['Liquid. Mantto'].sum()

        capex_frio = df_capex_frio.loc[df_capex_frio['Matricula'].isin(matriculas_conver_frio), 'CAPEX FRIO'].sum()
        if mtto_frio.sum() == 0:
            mtto_frio = 0

        filtro_samanco = []
        if self.cap_samanco == 0:
            filtro_samanco = ['Samanco']

        filtro_supe = []
        if self.cap_supe == 0:
            filtro_supe = ['Supe']

        
        """Cálculo de la depreciación"""
        depre_barc_elegidos = df_costos_embar[df_costos_embar['Matricula'].isin(matriculas_conver_frio + matriculas_excluir)].groupby('temporada')['Depreciacion'].sum()
        if depre_barc_elegidos.sum() == 0:
            depre_barc_elegidos = 0

        depreciacion_barcos = df_costos_embar.groupby('temporada')['Depreciacion'].sum() - depre_barc_elegidos
        if depreciacion_barcos.sum() == 0:
            depreciacion_barcos = 0

        depreciacion_tasa = df_resto_tasa.groupby('temporada')['Depreciacion'].sum()

        depreciacion = depreciacion_barcos + depreciacion_tasa + df_money_copy[~df_money_copy['planta_sugerida'].isin(filtro_samanco + filtro_supe)].groupby('temporada')['Depreciacion'].sum()

        """Cálculo del capex"""
        capex_barc_elegidos = df_costos_embar[df_costos_embar['Matricula'].isin(matriculas_conver_frio + matriculas_excluir)].groupby('temporada')['Capex'].sum()
        if capex_barc_elegidos.sum() == 0:
            capex_barc_elegidos = 0

        capex_barcos = df_costos_embar.groupby('temporada')['Capex'].sum() - capex_barc_elegidos
        if capex_barcos.sum() == 0:
            capex_barcos = 0
        capex_tasa = df_resto_tasa.groupby('temporada')['Capex'].sum()

        capex = capex_barcos + capex_tasa + df_money_copy[~df_money_copy['planta_sugerida'].isin(filtro_samanco + filtro_supe)].groupby('temporada')['Capex'].sum()

        # Flag tiene planta sugerida
        mask_plant_notna = (df_flagged['planta_sugerida'].notna())

        # Flag días de pesca adicionales
        # mask_adicional = (df_flagged['flag_dia_adicional'] == 1)

        # Flag barcos de TASA
        mask_prop = (df_flagged['Tipo Tr'].isin(['Propio con Frio', 'Propio sin Frio']))

        # Flag término de temporada con días de pesca adicional
        mask_fin_tempor = (df_flagged['flag_termino'] == 1)

        mask_model = mask_plant_notna & ~mask_fin_tempor
        
        mp_descargado_propios = pd.pivot_table(df_flagged[mask_model & mask_prop].groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['volumen'].sum(), 
        columns='temporada', values='volumen', index='planta_sugerida', dropna=False, aggfunc=np.sum)
        # mp_descargado_propios.to_excel('outputs/df_mp_descargado_propios_base.xlsx')
        mp_descargado_terceros = pd.pivot_table(df_flagged[mask_model & ~mask_prop].groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['volumen'].sum(), 
        columns='temporada', values='volumen', index='planta_sugerida', dropna=False, aggfunc=np.sum)
        # mp_descargado_terceros.to_excel('outputs/df_mp_descargado_terceros_base.xlsx')
        mp_descargado_total = pd.pivot_table(df_flagged[mask_model].groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['volumen'].sum(), 
        columns='temporada', values='volumen', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_rend_hp = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Rend. HP'].sum(), 
        columns='temporada', values='Rend. HP', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_rend_ap = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Rend. AP'].sum(), 
        columns='temporada', values='Rend. AP', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        produccion_hp = mp_descargado_total / df_rend_hp

        produccion_ap = mp_descargado_total * df_rend_ap

        df_calidad_sp = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Calidad SP (%)'].sum(), 
        columns='temporada', values='Calidad SP (%)', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_calidad_pr = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Calidad P (%)'].sum(), 
        columns='temporada', values='Calidad P (%)', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_calidad_taiwan = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Calidad TW (%)'].sum(), 
        columns='temporada', values='Calidad TW (%)', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_calidad_thailand = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Calidad TH (%)'].sum(), 
        columns='temporada', values='Calidad TH (%)', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_calidad_std = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Calidad STD (%)'].sum(), 
        columns='temporada', values='Calidad STD (%)', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_calidad_aqua = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Calidad Aqua (%)'].sum(), 
        columns='temporada', values='Calidad Aqua (%)', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_calidad_omega = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Calidad Omega (%)'].sum(), 
        columns='temporada', values='Calidad Omega (%)', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_precio_sp = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Precio SP'].sum(), 
        columns='temporada', values='Precio SP', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_precio_pr = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Precio P'].sum(), 
        columns='temporada', values='Precio P', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_precio_taiwan = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Precio TW'].sum(), 
        columns='temporada', values='Precio TW', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_precio_thailand = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Precio TH'].sum(), 
        columns='temporada', values='Precio TH', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_precio_std = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Precio STD'].sum(), 
        columns='temporada', values='Precio STD', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_precio_aqua = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Precio Aqua'].sum(), 
        columns='temporada', values='Precio Aqua', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_precio_omega = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Precio Omega'].sum(), 
        columns='temporada', values='Precio Omega', index='planta_sugerida', dropna=False, aggfunc=np.sum)
    

        precio_hp = df_calidad_sp * df_precio_sp + df_calidad_pr * df_precio_pr + df_calidad_taiwan * df_precio_taiwan + df_calidad_thailand * df_precio_thailand + df_calidad_std * df_precio_std

        precio_ap = df_calidad_aqua * df_precio_aqua + df_calidad_omega * df_precio_omega

        df_costo_var_unit_hp = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Costo Variable Unitario HP'].sum(), 
        columns='temporada', values='Costo Variable Unitario HP', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_costo_extra_unit = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Costo Extraccion Unitario'].sum(), 
        columns='temporada', values='Costo Extraccion Unitario', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_costo_terc_unit = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Costo Terceros Unitario'].sum(), 
        columns='temporada', values='Costo Terceros Unitario', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        """"CAPEX Malabrigo"""
        if (self.cap_malabrigo == 3760) | (self.cap_malabrigo == 1):
            # capex_malabrigo = 3057042.24
            capex_malabrigo = 0
        elif np.isclose(self.cap_malabrigo, 1.05263157894):
            capex_malabrigo = 2942000
        elif np.isclose(self.cap_malabrigo, 1.105263157894):
            capex_malabrigo = 4522000
        elif np.isclose(self.cap_malabrigo, 1.15789473684):
            capex_malabrigo = 6526000
        elif np.isclose(self.cap_malabrigo, 1.210526315789):
            capex_malabrigo = 7072000
        elif np.isclose(self.cap_malabrigo, 1.263157894736):
            capex_malabrigo = 9044000
        elif np.isclose(self.cap_malabrigo, 1.115):
            capex_malabrigo = 3057042.24

        """"CAPEX Chimbote"""
        if np.isclose(self.cap_chimbote, 3838.4210526315) | (self.cap_chimbote == 1):
            # capex_chimbote = 2357042.24
            capex_chimbote = 0
        elif np.isclose(self.cap_chimbote, 1.153846153846):
            capex_chimbote = 5600000
        elif np.isclose(self.cap_chimbote, 1.123):
            capex_chimbote = 2357042.24
        elif np.isclose(self.cap_chimbote, 1.123 * 1.15384615):
            capex_chimbote = 2357042.24 + 5600000 

        """"CAPEX Samanco"""
        # if self.cap_samanco == 1:
        #     capex_samanco = 0
        # else:
        capex_samanco = 0

        """"CAPEX Supe"""
        capex_supe = 0

        """"CAPEX Vegueta"""
        if np.isclose(self.cap_vegueta, 2900) | (self.cap_vegueta == 1):
            # capex_vegueta = 234000
            capex_vegueta = 0
        elif np.isclose(self.cap_vegueta, 1.18571428571):
            capex_vegueta = 2855000
        elif np.isclose(self.cap_vegueta, 1.2857142857142):
            capex_vegueta = 8990000
        elif np.isclose(self.cap_vegueta, 1.364285714285):
            capex_vegueta = 10915000
        elif np.isclose(self.cap_vegueta, 1.414285714285):
            capex_vegueta = 13680000
        elif np.isclose(self.cap_vegueta, 1.48571428571):
            capex_vegueta = 17294000
        elif np.isclose(self.cap_vegueta, 1.557142857142):
            capex_vegueta = 17430000

        """"CAPEX Callao"""
        capex_callao = 0

        """"CAPEX Pisco S"""
        capex_pisco_s = 0

        df_ingresos = produccion_hp * precio_hp + produccion_ap * precio_ap

        if self.cap_samanco == 0:
            mask = df_money_copy['planta_sugerida'] == 'Samanco'
            df_money_copy.loc[mask, 'Ppto - Admin'] = 0
            # df_money_copy.loc[mask, ['Ppto - Admin', 'Nóminas']] = 0
            df_money_copy.loc[mask, 'Nóminas'] = df_money_copy.loc[mask, 'Nóminas'] * 0.3
            df_money_copy.loc[mask, 'Liquid. Mantto'] = df_money_copy.loc[mask, 'Liquid. Mantto'] * 0.5

        if self.cap_supe == 0:
            mask = df_money_copy['planta_sugerida'] == 'Supe'
            df_money_copy.loc[mask, 'Ppto - Admin'] = 0
            # df_money_copy.loc[mask, ['Ppto - Admin', 'Nóminas']] = 0
            df_money_copy.loc[mask, 'Nóminas'] = df_money_copy.loc[mask, 'Nóminas'] * 0.3
            df_money_copy.loc[mask, 'Liquid. Mantto'] = df_money_copy.loc[mask, 'Liquid. Mantto'] * 0.5

        df_money_copy['costos_fijos'] = df_money_copy['Liquid. Mantto'] + df_money_copy['Nóminas'] + df_money_copy['Ppto - Admin']

        df_costo_fijos = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['costos_fijos'].sum(), 
        columns='temporada', values='costos_fijos', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_cost_var = produccion_hp * df_costo_var_unit_hp

        df_costos_mp_extrac = mp_descargado_propios * df_costo_extra_unit

        df_costos_mp_terc = mp_descargado_terceros * df_costo_terc_unit

        inversion_total = capex_malabrigo + capex_chimbote + capex_samanco + capex_supe + capex_vegueta + capex_callao + capex_pisco_s + capex_frio
        
        nueva_depreciacion = inversion_total / 10
        df_ebit = df_ingresos.sum() + ahorro_omision - (df_costo_fijos.sum() + df_cost_var.sum() + df_costos_mp_extrac.sum() + df_costos_mp_terc.sum() + mtto_frio)

        df_ebit_cf = df_ingresos.sum() + ahorro_omision - (df_costo_fijos.sum() + df_cost_var.sum() + df_costos_mp_extrac.sum() + df_costos_mp_terc.sum() + mtto_frio) - depreciacion - nueva_depreciacion

        # TODO: 
        tasa_impuestos = 29.5 / 100
        cash_flow = df_ebit_cf * (1 - tasa_impuestos) + depreciacion - capex + nueva_depreciacion

        df_ebit_cf = df_ebit_cf.reset_index()
        df_ebit_cf.rename(columns={0:'ebit'}, inplace=True)

        cash_flow = cash_flow.reset_index()
        cash_flow.rename(columns={0:'cash_flow'}, inplace=True)

        if np.sum(ahorro_omision) == 0:
            ahorro_omision = [0] * 8

        if np.sum(depreciacion) == 0:
            depreciacion = [0] * 8

        if np.sum(nueva_depreciacion) == 0:
            nueva_depreciacion = [0] * 8
        else:
            nueva_depreciacion = [nueva_depreciacion] * 8

        if np.sum(capex) == 0:
            capex = [0] * 8
        # df_ebit = self.clean_revenue(df_ebit, 'ebit')
        # df_cash_flow = self.clean_revenue(cash_flow, 'cash_flow')
        
        # ebit_pv = df_ebit['ebit_pv'].sum()
        # ebit_prom = df_ebit['ebit'].mean()
        # cash_flow_pv = df_cash_flow['cash_flow_pv'].sum()
        # cash_flow_prom = df_cash_flow['cash_flow'].mean()

        # vr_year4 = ebit_prom * (1 + self.g_ratio) / (self.r_ratio - self.g_ratio)
        # vr_pv = vr_year4 / ((1 + self.r_ratio) ** 4)

        # cf_vr_year4 = cash_flow_prom * (1 + self.g_ratio) / (self.r_ratio - self.g_ratio)
        # cf_vr_pv = cf_vr_year4 / ((1 + self.r_ratio) ** 4)

        
        # gastos_cierre = 1400000 * self.n_plant_cerradas + 100000 * (48 - self.num_embar)

        # npv = ebit_pv + vr_pv - inversion_total - gastos_cierre

        # cf_npv = cash_flow_pv + cf_vr_pv - inversion_total - gastos_cierre

        # market_share = (mp_descargado_propios.sum().mean() + mp_descargado_terceros.sum().mean()) / self.df_cuota_nacional_copy['Descarga Nacional'].mean()
        # cumpli_cuota_propia = mp_descargado_propios.sum().mean() / (self.df_cuota_nacional_copy['Descarga Nacional'].mean() * self.porc_cuota_tasa)

        return df_ebit_cf['ebit'].to_numpy(), cash_flow['cash_flow'].to_numpy(), depreciacion.to_numpy(), nueva_depreciacion, capex.to_numpy(), df_ingresos.sum().to_numpy(), np.array(ahorro_omision), produccion_hp.sum().to_numpy(), produccion_ap.sum().to_numpy(), (produccion_hp * precio_hp).sum().to_numpy(), (produccion_ap * precio_ap).sum().to_numpy(), df_costo_fijos.sum().to_numpy(), df_cost_var.sum().to_numpy(), df_costos_mp_extrac.sum().to_numpy(), df_costos_mp_terc.sum().to_numpy(), mtto_frio, mp_descargado_propios.sum().to_numpy(), mp_descargado_terceros.sum().to_numpy()

    def get_kpis_year(self, df_flagged, df_money):
        """
        Calculo los kpis para output final
        """
        # df_flagged.to_excel('outputs/df_flagged_in_.xlsx')
        df_money_copy = self.preprocessing_df_money(df_money)
        df_costos_embar = self.df_costos_embar
        df_capex_frio = self.df_capex_frio
        
        to_replace = {
            '2018 - I':'2018-I',
            '2018 - II':'2018-II',
            '2019 - I':'2019-I',
            '2019 - II':'2019-II',
            '2020 - I':'2020-I',
            '2020 - II':'2020-II',
            '2021 - I':'2021-I',
            '2021 - II':'2021-II',
            '2022 - I':'2021-I',
        }
        df_costos_embar.replace({'temporada':to_replace}, inplace=True)
        matriculas_excluir = self.df_emb_omitir_copy[:(48 - self.num_embar)].loc[:, 'Matricula'].tolist()
        ahorro_omision = df_costos_embar[df_costos_embar['Matricula'].isin(matriculas_excluir)].groupby('temporada', dropna=False)['CAPEX'].sum()
        if ahorro_omision.sum() == 0:
            ahorro_omision = 0
        
        matriculas_conver_frio = self.df_conversion_frio_copy[:(self.num_eps_frio - 19)].loc[:, 'Matricula'].tolist()
        mtto_frio = df_costos_embar[df_costos_embar['Matricula'].isin(matriculas_conver_frio)].groupby('temporada')['CAPEX'].sum()
        
        capex_frio = df_capex_frio.loc[df_capex_frio['Matricula'].isin(matriculas_conver_frio), 'CAPEX FRIO'].sum()
        if mtto_frio.sum() == 0:
            mtto_frio = 0

        # Flag tiene planta sugerida
        mask_plant_notna = (df_flagged['planta_sugerida'].notna())

        # Flag días de pesca adicionales
        # mask_adicional = (df_flagged['flag_dia_adicional'] == 1)

        # Flag barcos de TASA
        mask_prop = (df_flagged['Tipo Tr'].isin(['Propio con Frio', 'Propio sin Frio']))

        # Flag término de temporada con días de pesca adicional
        mask_fin_tempor = (df_flagged['flag_termino'] == 1)

        mask_model = mask_plant_notna & ~mask_fin_tempor
        
        mp_descargado_propios = pd.pivot_table(df_flagged[mask_model & mask_prop].groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['volumen'].sum(), 
        columns='temporada', values='volumen', index='planta_sugerida', dropna=False, aggfunc=np.sum)
        # mp_descargado_propios.to_excel('outputs/df_mp_descargado_propios_base.xlsx')
        mp_descargado_terceros = pd.pivot_table(df_flagged[mask_model & ~mask_prop].groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['volumen'].sum(), 
        columns='temporada', values='volumen', index='planta_sugerida', dropna=False, aggfunc=np.sum)
        # mp_descargado_terceros.to_excel('outputs/df_mp_descargado_terceros_base.xlsx')
        mp_descargado_total = pd.pivot_table(df_flagged[mask_model].groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['volumen'].sum(), 
        columns='temporada', values='volumen', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_rend_hp = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Rend. HP'].sum(), 
        columns='temporada', values='Rend. HP', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_rend_ap = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Rend. AP'].sum(), 
        columns='temporada', values='Rend. AP', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        produccion_hp = mp_descargado_total / df_rend_hp

        produccion_ap = mp_descargado_total * df_rend_ap

        df_calidad_sp = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Calidad SP (%)'].sum(), 
        columns='temporada', values='Calidad SP (%)', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_calidad_pr = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Calidad P (%)'].sum(), 
        columns='temporada', values='Calidad P (%)', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_calidad_taiwan = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Calidad TW (%)'].sum(), 
        columns='temporada', values='Calidad TW (%)', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_calidad_thailand = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Calidad TH (%)'].sum(), 
        columns='temporada', values='Calidad TH (%)', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_calidad_std = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Calidad STD (%)'].sum(), 
        columns='temporada', values='Calidad STD (%)', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_calidad_aqua = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Calidad Aqua (%)'].sum(), 
        columns='temporada', values='Calidad Aqua (%)', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_calidad_omega = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Calidad Omega (%)'].sum(), 
        columns='temporada', values='Calidad Omega (%)', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_precio_sp = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Precio SP'].sum(), 
        columns='temporada', values='Precio SP', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_precio_pr = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Precio P'].sum(), 
        columns='temporada', values='Precio P', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_precio_taiwan = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Precio TW'].sum(), 
        columns='temporada', values='Precio TW', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_precio_thailand = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Precio TH'].sum(), 
        columns='temporada', values='Precio TH', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_precio_std = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Precio STD'].sum(), 
        columns='temporada', values='Precio STD', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_precio_aqua = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Precio Aqua'].sum(), 
        columns='temporada', values='Precio Aqua', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_precio_omega = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Precio Omega'].sum(), 
        columns='temporada', values='Precio Omega', index='planta_sugerida', dropna=False, aggfunc=np.sum)
    

        precio_hp = df_calidad_sp * df_precio_sp + df_calidad_pr * df_precio_pr + df_calidad_taiwan * df_precio_taiwan + df_calidad_thailand * df_precio_thailand + df_calidad_std * df_precio_std

        precio_ap = df_calidad_aqua * df_precio_aqua + df_calidad_omega * df_precio_omega

        df_costo_var_unit_hp = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Costo Variable Unitario HP'].sum(), 
        columns='temporada', values='Costo Variable Unitario HP', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_costo_extra_unit = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Costo Extraccion Unitario'].sum(), 
        columns='temporada', values='Costo Extraccion Unitario', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_costo_terc_unit = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Costo Terceros Unitario'].sum(), 
        columns='temporada', values='Costo Terceros Unitario', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        """"CAPEX Malabrigo"""
        if (self.cap_malabrigo == 3760) | (self.cap_malabrigo == 1):
            # capex_malabrigo = 3057042.24
            capex_malabrigo = 0
        elif np.isclose(self.cap_malabrigo, 1.05263157894):
            capex_malabrigo = 2942000
        elif np.isclose(self.cap_malabrigo, 1.105263157894):
            capex_malabrigo = 4522000
        elif np.isclose(self.cap_malabrigo, 1.15789473684):
            capex_malabrigo = 6526000
        elif np.isclose(self.cap_malabrigo, 1.210526315789):
            capex_malabrigo = 7072000
        elif np.isclose(self.cap_malabrigo, 1.263157894736):
            capex_malabrigo = 9044000

        """"CAPEX Chimbote"""
        if np.isclose(self.cap_chimbote, 3838.4210526315) | (self.cap_chimbote == 1):
            # capex_chimbote = 2357042.24
            capex_chimbote = 0
        elif np.isclose(self.cap_chimbote, 1.153846153846):
            capex_chimbote = 5600000

        """"CAPEX Samanco"""
        # if self.cap_samanco == 1:
        #     capex_samanco = 0
        # else:
        capex_samanco = 0

        """"CAPEX Supe"""
        capex_supe = 0

        """"CAPEX Vegueta"""
        if np.isclose(self.cap_vegueta, 2900) | (self.cap_vegueta == 1):
            # capex_vegueta = 234000
            capex_vegueta = 0
        elif np.isclose(self.cap_vegueta, 1.18571428571):
            capex_vegueta = 2855000
        elif np.isclose(self.cap_vegueta, 1.2857142857142):
            capex_vegueta = 8990000
        elif np.isclose(self.cap_vegueta, 1.364285714285):
            capex_vegueta = 10915000
        elif np.isclose(self.cap_vegueta, 1.414285714285):
            capex_vegueta = 13680000
        elif np.isclose(self.cap_vegueta, 1.48571428571):
            capex_vegueta = 17294000
        elif np.isclose(self.cap_vegueta, 1.557142857142):
            capex_vegueta = 17430000

        """"CAPEX Callao"""
        capex_callao = 0

        """"CAPEX Pisco S"""
        capex_pisco_s = 0

        df_ingresos = produccion_hp * precio_hp + produccion_ap * precio_ap

        if self.cap_samanco == 0:
            mask = df_money_copy['planta_sugerida'] == 'Samanco'
            df_money_copy.loc[mask, ['Ppto - Admin', 'Nóminas']] = 0
            df_money_copy.loc[mask, 'Liquid. Mantto'] = df_money_copy.loc[mask, 'Liquid. Mantto'] * 0.5

        if self.cap_supe == 0:
            mask = df_money_copy['planta_sugerida'] == 'Supe'
            df_money_copy.loc[mask, ['Ppto - Admin', 'Nóminas']] = 0
            df_money_copy.loc[mask, 'Liquid. Mantto'] = df_money_copy.loc[mask, 'Liquid. Mantto'] * 0.5

        df_money_copy['costos_fijos'] = df_money_copy['Liquid. Mantto'] + df_money_copy['Nóminas'] + df_money_copy['Ppto - Admin']

        df_costo_fijos = pd.pivot_table(df_money_copy.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['costos_fijos'].sum(), 
        columns='temporada', values='costos_fijos', index='planta_sugerida', dropna=False, aggfunc=np.sum)

        df_cost_var = produccion_hp * df_costo_var_unit_hp

        df_costos_mp_extrac = mp_descargado_propios * df_costo_extra_unit

        df_costos_mp_terc = mp_descargado_terceros * df_costo_terc_unit

        df_ebit = df_ingresos.sum() + ahorro_omision - (df_costo_fijos.sum() + df_cost_var.sum() + df_costos_mp_extrac.sum() + df_costos_mp_terc.sum() + mtto_frio)
        df_ebit = df_ebit.reset_index()
        df_ebit.rename(columns={0:'ebit'}, inplace=True)

        if np.sum(ahorro_omision) == 0:
            ahorro_omision = [0] * 8
        return df_ebit['ebit'].to_numpy(), df_ingresos.sum().to_numpy(), np.array(ahorro_omision), produccion_hp.sum().to_numpy(), produccion_ap.sum().to_numpy(), (produccion_hp * precio_hp).sum().to_numpy(), (produccion_ap * precio_ap).sum().to_numpy(), df_costo_fijos.sum().to_numpy(), df_cost_var.sum().to_numpy(), df_costos_mp_extrac.sum().to_numpy(), df_costos_mp_terc.sum().to_numpy(), mtto_frio, mp_descargado_propios.sum().to_numpy(), mp_descargado_terceros.sum().to_numpy()

    def paralelizar_temporada(self): # CESAR
        """
        Ejecuta el método predict_plant de forma paralela.
        """
        # lst_final = Parallel(n_jobs=8)(delayed(self.predict_plant)(i) for i in self.temporadas_elegidas)
        # lst_final = []
        # for tempo in self.temporadas_elegidas:
        #     lst_final.append(self.predict_plant(tempo))
        lst_final = [self.predict_plant(i) for i in self.temporadas_elegidas]
        df_flagged = pd.concat(lst_final)
        names, ingresos, ahorro_omision, ebit_pv, vr_pv, vr_year4, produccion_hp, produccion_ap, costos_fij, cost_var, costos_mp_extrac, costos_mp_terc, capex_frio, npv, capex_total, gastos_cierre, ebit_prom, market_share, market_terceros, cumpli_cuota_propia, mp_descargado_propios, mp_descargado_terceros, cash_flow, cash_flow_pv, cf_vr_pv, depreciacion, capex, nueva_depreciacion, cant_dias_adicionales, vol_total_adicional, df_ebit_report_prom = self.get_kpis(df_flagged, self.df_money_copy)
        
        return names, ingresos, ahorro_omision, ebit_pv, vr_pv, vr_year4, produccion_hp, produccion_ap, costos_fij, cost_var, costos_mp_extrac, costos_mp_terc, capex_frio, npv, capex_total, gastos_cierre, ebit_prom, market_share, market_terceros, cumpli_cuota_propia, mp_descargado_propios, mp_descargado_terceros, cash_flow, cash_flow_pv, cf_vr_pv, depreciacion, capex, nueva_depreciacion, cant_dias_adicionales, vol_total_adicional, df_ebit_report_prom

    def paralelizar_temporada_year(self):
        """
        Ejecuta el método predict_plant de forma paralela.
        """
        lst_final = Parallel(n_jobs=8)(delayed(self.predict_plant)(i) for i in self.temporadas_elegidas)
        df_flagged = pd.concat(lst_final)
        ebit, df_ingresos, ahorro_omision, produccion_hp, produccion_ap, precio_hp, precio_ap, df_costo_fijos, df_cost_var, df_costos_mp_extrac, df_costos_mp_terc, mtto_frio, mp_descargado_propios, mp_descargado_terceros = self.get_kpis_year(df_flagged, self.df_money_copy)
        
        return ebit, df_ingresos, ahorro_omision, produccion_hp, produccion_ap, precio_hp, precio_ap, df_costo_fijos, df_cost_var, df_costos_mp_extrac, df_costos_mp_terc, mtto_frio, mp_descargado_propios, mp_descargado_terceros

    def paralelizar_temporada_cf_year(self):
        """
        Ejecuta el método predict_plant de forma paralela.
        """
        lst_final = Parallel(n_jobs=8)(delayed(self.predict_plant)(i) for i in self.temporadas_elegidas)
        df_flagged = pd.concat(lst_final)
        ebit, cash_flow, depreciacion, nueva_depreciacion, capex, df_ingresos, ahorro_omision, produccion_hp, produccion_ap, precio_hp, precio_ap, df_costo_fijos, df_cost_var, df_costos_mp_extrac, df_costos_mp_terc, mtto_frio, mp_descargado_propios, mp_descargado_terceros = self.get_kpis_cash_flow_year(df_flagged, self.df_money_copy)
        
        return ebit, cash_flow, depreciacion, nueva_depreciacion, capex, df_ingresos, ahorro_omision, produccion_hp, produccion_ap, precio_hp, precio_ap, df_costo_fijos, df_cost_var, df_costos_mp_extrac, df_costos_mp_terc, mtto_frio, mp_descargado_propios, mp_descargado_terceros

    # @functools.cached_property
    def run(self, num_embar, num_eps_frio, flg_45, estrategia, cap_malabrigo, cap_chimbote, cap_samanco, cap_supe, cap_vegueta, cap_callao, cap_pisco_s, flg_barco_nuevo): # CESAR
        """
        Corre los escenaios.
        """
        print("Corriendo escenario")
        self.num_embar = num_embar
        self.num_eps_frio = num_eps_frio
        self.flg_45 = flg_45
        self.estrategia = estrategia
        self.cap_malabrigo = cap_malabrigo
        self.cap_chimbote = cap_chimbote
        self.cap_samanco = cap_samanco
        self.cap_supe = cap_supe
        self.cap_vegueta = cap_vegueta
        self.cap_callao = cap_callao
        self.cap_pisco_s = cap_pisco_s
        self.flg_barco_nuevo = flg_barco_nuevo
        self.n_plant_cerradas = 7 - np.count_nonzero([cap_malabrigo, cap_chimbote, cap_samanco, cap_supe, cap_vegueta, cap_callao, cap_pisco_s])
        return self.paralelizar_temporada()

    def execute_running(self, row): # CESAR
        """
        Corre los escenaios.
        """
        self.num_embar = [ep for ep in row["num_embar"] if ep is not None]
        # self.num_embar = row["num_embar"][row["num_embar"] != None]
        self.num_eps_frio = row["num_eps_frio"]
        self.flg_45 = row["flg_45"]
        self.estrategia = row["estrategia"]
        self.cap_malabrigo = row["cap_malabrigo"]
        self.cap_chimbote = row["cap_chimbote"]
        self.cap_samanco = row["cap_samanco"]
        self.cap_supe = row["cap_supe"]
        self.cap_vegueta = row["cap_vegueta"]
        self.cap_callao = row["cap_callao"]
        self.cap_pisco_s = row["cap_pisco_s"]
        self.flg_barco_nuevo = row["flg_barco_nuevo"]
        self.n_plant_cerradas = 7 - np.count_nonzero([self.cap_malabrigo, self.cap_chimbote, self.cap_samanco, self.cap_supe, self.cap_vegueta, self.cap_callao, self.cap_pisco_s])
        return self.paralelizar_temporada()

    def execute_running_paralelo(self, row): # CESAR
        """
        Corre los escenaios.
        """
        return row.apply(self.execute_running, axis=1)

    def run_year(self, num_embar, num_eps_frio, flg_45, estrategia, cap_malabrigo, cap_chimbote, cap_samanco, cap_supe, cap_vegueta, cap_callao, cap_pisco_s):
        """
        Corre los escenaios.
        """
        self.num_embar = num_embar
        self.num_eps_frio = num_eps_frio
        self.flg_45 = flg_45
        self.estrategia = estrategia
        self.cap_malabrigo = cap_malabrigo
        self.cap_chimbote = cap_chimbote
        self.cap_samanco = cap_samanco
        self.cap_supe = cap_supe
        self.cap_vegueta = cap_vegueta
        self.cap_callao = cap_callao
        self.cap_pisco_s = cap_pisco_s
        self.n_plant_cerradas = 7 - np.count_nonzero([cap_malabrigo, cap_chimbote, cap_samanco, cap_supe, cap_vegueta, cap_callao, cap_pisco_s])
        return self.paralelizar_temporada_year()

    def run_year_cf(self, num_embar, num_eps_frio, flg_45, estrategia, cap_malabrigo, cap_chimbote, cap_samanco, cap_supe, cap_vegueta, cap_callao, cap_pisco_s):
        """
        Corre los escenaios.
        """
        self.num_embar = num_embar
        self.num_eps_frio = num_eps_frio
        self.flg_45 = flg_45
        self.estrategia = estrategia
        self.cap_malabrigo = cap_malabrigo
        self.cap_chimbote = cap_chimbote
        self.cap_samanco = cap_samanco
        self.cap_supe = cap_supe
        self.cap_vegueta = cap_vegueta
        self.cap_callao = cap_callao
        self.cap_pisco_s = cap_pisco_s
        self.n_plant_cerradas = 7 - np.count_nonzero([cap_malabrigo, cap_chimbote, cap_samanco, cap_supe, cap_vegueta, cap_callao, cap_pisco_s])
        return self.paralelizar_temporada_cf_year()
    # def run_fast(self, np__scenarios):
    #     self.num_embar = num_embar
    #     self.num_eps_frio = num_eps_frio
    #     self.flg_45 = flg_45
    #     self.estrategia = estrategia
    #     self.cap_malabrigo = cap_malabrigo
    #     self.cap_chimbote = cap_chimbote
    #     self.cap_samanco = cap_samanco
    #     self.cap_supe = cap_supe
    #     self.cap_vegueta = cap_vegueta
    #     self.cap_callao = cap_callao
    #     self.cap_pisco_s = cap_pisco_s
    #     self.n_plant_cerradas = 7 - np.count_nonzero([cap_malabrigo, cap_chimbote, cap_samanco, cap_supe, cap_vegueta, cap_callao, cap_pisco_s])
    #     return self.paralelizar_temporada()

    def to_pickle(self, filename):
        with open(filename, "wb") as output:
            pickle.dump(self, output, pickle.HIGHEST_PROTOCOL)
