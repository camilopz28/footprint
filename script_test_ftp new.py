import pandas as pd
import numpy as np

from source.engine.FTP_engine import FTPModel as FTP

from datetime import datetime

path = 'datasets/'

# Data de embarcaciones (descargas)
file = 'df_embar.xlsx'
df_embar = pd.read_excel(path + file, converters={'Matricula':str})

# Data de prioridad de embarcaciones
file = 'df_prio_embar.xlsx'
df_prio_embar = pd.read_excel(path + file, converters={'Matricula':str})

# Data de prioridad de destinos
file = 'df_prio_dest.csv'
df_prio_dest = pd.read_csv(path + file, sep=';')

# Data de capacidad de planta
file = 'df_cap_plant_all.csv'
df_cap_plant = pd.read_csv(path + file, sep=';')

# Data de máximo de días con capacida máxima
file = 'df_max_dias_pico.csv'
df_max_dias_pico = pd.read_csv(path + file, sep=';')
# A dataframe that contains the maximum number of days that a plant can work at maximum capacity.
df_max_dias_pico.fillna(0, inplace=True)

# Data de porcentaje de velocidad en dia de descanso
file = 'df_porc_veloc_descanso.csv'
df_porc_veloc_descanso = pd.read_csv(path + file, sep=';')
df_porc_veloc_descanso.fillna(0, inplace=True)

# Registro de carenas
file = 'df_carenas.xlsx'
df_carenas = pd.read_excel(path + file, converters={'Matricula':str})

# Mal tiempo
file = 'df_cierre_puertos.csv'
df_mal_tiempo = pd.read_csv(path + file, sep=';')
df_mal_tiempo = df_mal_tiempo[df_mal_tiempo['TIEMPO CERRADO (horas)'] > 24]
df_mal_tiempo['CIERRE'] = pd.to_datetime(df_mal_tiempo['CIERRE'])

# Cbod competencia
file = 'df_cbod_competencia.xlsx'
df_cbod_competencia = pd.read_excel(path + file, converters={'Matricula':str})
df_cbod_competencia.loc[:, 'Matricula'] = (
            df_cbod_competencia['Matricula']
            .str.split(r"-", n=-1, expand=True)
            .loc[:, 1]
            .to_numpy())
df_cbod_competencia.rename(columns={'C.Bod':'CBOD'}, inplace=True)

# Descarga Competencia
file = 'df_descarga_competencia.xlsx'
df_descarga_competencia = pd.read_excel(path + file, converters={'Matricula':str})
df_descarga_competencia.loc[:, 'Matricula'] = (
            df_descarga_competencia['Matricula']
            .str.split(r"-", n=-1, expand=True)
            .loc[:, 1]
            .to_numpy())

df_descarga_competencia = df_descarga_competencia[df_descarga_competencia['ZONA'] == 'Zona Norte']
df_descarga_competencia = df_descarga_competencia[df_descarga_competencia['Fecha'] > '26/01/2018']

df_descarga_competencia['Fecha'] = pd.to_datetime(
            df_descarga_competencia.loc[:, 'Fecha'], format="%Y/%m/%d"
        )

df_descarga_competencia.sort_values('Fecha', inplace=True)
df_descarga_competencia["diferences"] = np.c_[
    np.array([0]),
    np.diff(df_descarga_competencia['Fecha'].dt.date.to_numpy())
    .astype("timedelta64[s]")
    .reshape(1, -1),
].flatten()
name_dif_min = "dif_minutes"
df_descarga_competencia.loc[:, name_dif_min] = df_descarga_competencia["diferences"].dt.total_seconds() / 60

name_flg_tempo = "flg_temporada"
df_descarga_competencia[name_flg_tempo] = 0
mask = df_descarga_competencia[name_dif_min] > 43200
df_descarga_competencia.loc[mask, name_flg_tempo] = 1

tempo_names = [
    "2018-I",
    "2018-II",
    "2019-I",
    "2019-II",
    "2020-I",
    "2020-II",
    "2021-I",
    "2021-II",
]
vals = np.arange(0, len(tempo_names))

flgs = df_descarga_competencia[name_flg_tempo].to_numpy()
flgs = np.cumsum(flgs)
df_descarga_competencia[name_flg_tempo] = flgs
conditions = [df_descarga_competencia[name_flg_tempo] == i for i in vals]
df_descarga_competencia['Temporada'] = np.select(conditions, tempo_names)

df_descarga_competencia = pd.merge(df_descarga_competencia, df_cbod_competencia, on=['Matricula', 'Temporada'], how='left')
df_descarga_competencia = df_descarga_competencia[~(df_descarga_competencia['Planta'] == 'TECNOLOGICA DE ALIMENTOS S.A.')]

cols = ['Puerto', 'TM Descargadas', 'Temporada', 'CBOD', 'Fecha']
df_descarga_competencia = df_descarga_competencia[cols]
df_descarga_competencia['rendimiento'] = df_descarga_competencia['TM Descargadas'] / df_descarga_competencia['CBOD']


to_replace = {
    'BAYOVAR':2,
    'COISHCO':3,
    'CALLAO':5,
    'MALABRIGO':2,
    'CHIMBOTE':3,
    'CHANCAY':5,    
    'PARACAS (Pisco)':6,
    'CARQUIN':4,
    'VEGUETA':4,
    'SUPE':4,
    'TAMBO DE MORA':6,
}
df_descarga_competencia.replace({'Puerto':to_replace}, inplace=True)
df_descarga_competencia.rename(columns={'Puerto':'zona_de_pesca_id'}, inplace=True)

df_rendi_competencia = df_descarga_competencia.groupby(['Temporada', 'zona_de_pesca_id'], as_index=False).agg(
    rendi_prom_zona=('rendimiento', np.mean),
    ultimo_dia=('Fecha', 'max'),
    )


temporadas = ['2018-I', '2018-II', '2019-I', '2019-II', '2020-I', '2020-II', '2021-I', '2021-II']

df_porc_veloc_descanso[temporadas] = df_porc_veloc_descanso[temporadas].clip(upper=0.8999)

ftp_model = FTP(name_escenario_prio_embar='A', temporadas_elegidas=temporadas)

# ftp_model.to_pickle('models/FTPModel.pkl')

start_time = datetime.now()
ftp_model.predict(
    df_embar=df_embar,
    df_prio_embar=df_prio_embar,
    df_prio_dest=df_prio_dest,
    df_cap_plant=df_cap_plant,
    df_carenas=df_carenas,
    df_mal_tiempo=df_mal_tiempo,
    df_max_dias_pico=df_max_dias_pico,
    df_porc_veloc_descanso=df_porc_veloc_descanso,
    df_rendi_competencia=df_rendi_competencia 
)

df_embar_flagged = ftp_model.paralelizar_temporada()
time_elapsed = datetime.now() - start_time

print(f'Time elapsed (hh:mm:ss.ms) {time_elapsed}')
print("Proceso finalizado.")
df_embar_flagged.to_excel('outputs/output_sugerido_49.xlsx')