from dash import Dash, dash_table, dcc, html
from dash.dependencies import Input, Output, State
from dash.exceptions import PreventUpdate
import dash_bootstrap_components as dbc

import plotly.express as px
import plotly.graph_objects as go

from source.engine.FTP_engine import FTPModel as FTP

import pandas as pd

path = "datasets/"

# Data de embarcaciones (descargas)
# file = 'df_embar.xlsx'
# df_embar = pd.read_excel(path + file)

# Data de prioridad de embarcaciones
# file = "df_prio_embar.csv"
# df_prio_embar = pd.read_csv(path + file, sep=";")
file = 'df_prio_embar.xlsx'
df_prio_embar = pd.read_excel(path + file, converters={'Matricula':str})

# Data de prioridad de destinos
file = "df_prio_dest.csv"
df_prio_dest = pd.read_csv(path + file, sep=";")

# Data de capacidad de planta
file = "df_cap_plant_all.csv"
df_cap_plant = pd.read_csv(path + file, sep=";")


app = Dash(__name__, title="Footprint-Dev", external_stylesheets=[dbc.themes.SPACELAB])

row_content = [
    dbc.Col(html.Div("One of two columns"), lg=4),
    dbc.Col(html.Div("One of two columns"), lg=4),
]

controls = dbc.Card(
    [
        html.Div(
            [
                dbc.Label("Escenario embarcaciones:"),
                dcc.Dropdown(
                    ["A", "B", "C", "D", "E"], "A", id="esce-dropdown", searchable=False
                ),
                dbc.Label("Temporada:"),
                dcc.Dropdown(
                    ["2018-I",
            "2018-II",
            "2019-I",
            "2019-II",
            "2020-I",
            "2020-II",
            "2021-I",
            "2021-II"], ["2018-I"], id="tempo-dropdown", searchable=False, multi=True
                ),
            ]
        ),
        # html.Br(),
        # html.Div(
        #     [
        #         dcc.Upload(
        #             id='upload-data',
        #             children=html.Div([
        #                 'Ingrese Data'
        #             ]),
        #             style={
        #                 'width': '100%',
        #                 'height': '60px',
        #                 'lineHeight': '60px',
        #                 'borderWidth': '1px',
        #                 'borderStyle': 'dashed',
        #                 'borderRadius': '5px',
        #                 'textAlign': 'center',
        #                 'margin': '10px'
        #             },
        #             multiple=False
        #         ),
        #     ]
        # ),
        html.Br(),
        html.Hr(),
        html.Br(),
        html.Div(
            [
                dcc.Loading(
                    [dbc.Button("Calcular", color="primary", id="bt-calculate")]
                ),
            ],
            className="d-grid gap-2 col-6 mx-auto",
        ),
        html.Div([]),
    ],
    body=True,
)


app.layout = dbc.Container(
    [
        html.Br(),
        html.H1(
            "Footprint Model",
            style={
                "textAlign": "center",
                "font-weight": "bold",
                "font-family": "Times New Roman",
            },
        ),
        html.Hr(),
        html.Br(),
        dbc.Row(
            [
                dbc.Col(controls, md=2),
                # html.r(),
                dbc.Col(
                    dbc.Card(
                        [
                            html.H4(
                                "Características del Destino",
                                style={
                                    "textAlign": "center",
                                    # "font-weight": "bold",
                                    # "font-family": "Times New Roman",
                                },
                            ),
                            dash_table.DataTable(
                                id="datatable-df_cap_plant",
                                columns=[
                                    {"name": i, "id": i, "selectable": True}
                                    for i in df_cap_plant.columns
                                ],
                                data=df_cap_plant.to_dict("records"),
                                editable=True,
                                sort_action="native",
                                sort_mode="multi",
                                selected_columns=[],
                                selected_rows=[],
                                page_action="native",
                                page_current=0,
                                page_size=10,
                            ),
                        ],
                        body=True,
                    ),
                    md=10,
                    lg=10,
                ),
            ],
            align="center",
            justify="around",
        ),
        html.Hr(),
        # html.Div(id="test-container", children='Hola'),
        dbc.Spinner(
            children=[dcc.Graph(id="loading-output")],
            spinner_style={"width": "6rem", "height": "6rem"},
            color="info",
            type="grow",
            fullscreen=True,
        ),
    ],
    fluid=True,
)


@app.callback(
    # Output('test-container', 'children'),
    Output("loading-output", "figure"),
    Input("bt-calculate", "n_clicks"),
    State("datatable-df_cap_plant", "data"),
    State("esce-dropdown", "value"),
    State("tempo-dropdown", "value"),
)
def update_output(n_clicks, i_df_cap_plant, scenario, temporada):
    print(temporada)
    # if len(temporada) ==  1:
    #     temporada = [temporada]
    print("El numero de clicks es", n_clicks)
    if n_clicks is None:
        raise PreventUpdate
    file = "df_embar.xlsx"
    print(scenario)
    df_embar = pd.read_excel(path + file, converters={'Matricula':str})
    file = 'df_prio_embar.xlsx'
    df_prio_embar = pd.read_excel(path + file, converters={'Matricula':str})
    file = "df_prio_dest.csv"
    df_prio_dest = pd.read_csv(path + file, sep=";")
    df_cap_plant_u = pd.DataFrame(i_df_cap_plant)
    # years = ["Velocidad" + " " + str(i) for i in range(2018, 2022 + 1)]
    # df_cap_plant_u.loc[:, years] = df_cap_plant_u.loc[:, years].clip(lower=0.00001)
    print(df_cap_plant_u.head())
    ftp_model = FTP(name_escenario_prio_embar=scenario, temporadas_elegidas=temporada)
    df_embar_flagged, df_update_planta = ftp_model.predict(
        df_embar=df_embar,
        df_prio_embar=df_prio_embar,
        df_prio_dest=df_prio_dest,
        df_cap_plant=df_cap_plant_u,
    )
    mask = df_embar_flagged[ftp_model.name_planta_sugerida].notna()
    vol_est_desc = df_embar_flagged.loc[mask, ftp_model.name_mat_prima_descar].sum()
    vol_real_desc = df_embar_flagged.loc[:, ftp_model.name_mat_prima_descar].sum()

    n_desc_est = df_embar_flagged.loc[mask, ftp_model.id_embar].count()
    n_desc_real = df_embar_flagged.loc[:, ftp_model.id_embar].count()

    fig = go.Figure()

    fig.add_trace(go.Indicator(
        mode = "number+delta+gauge",
        value = vol_est_desc,
        gauge = {
            'axis': {'visible': False}},
        title = {'text': "Volumen Total"},
        delta = {'reference': vol_real_desc, 'relative': True},
        domain = {'row': 0, 'column': 0}))

    fig.add_trace(go.Indicator(
        value = 120,
        gauge = {
            'shape': "bullet",
            'axis' : {'visible': False}},
        domain = {'x': [0.05, 0.5], 'y': [0.15, 0.35]}))

    fig.add_trace(go.Indicator(
        mode = "number+delta",
        value = n_desc_est,
        gauge = {
            'shape': "bullet",
            'axis' : {'visible': False}},
        delta = {'reference': n_desc_real, 'relative': True},
        title = {'text': "Cantidad de descargas Total"},
        domain = {'row': 0, 'column': 1}))

    fig.add_trace(go.Indicator(
        mode = "delta",
        value = 40,
        domain = {'row': 1, 'column': 1}))

    fig.update_layout(
        grid = {'rows': 2, 'columns': 2, 'pattern': "independent"},
        # template = {'data' : {'indicator': [{
        #     'title': {'text': "Speed"},
        #     'mode' : "number+delta+gauge",
        #     'delta' : {'reference': 90}}]
        #                     }}
                            )
    return fig


if __name__ == "__main__":
    app.run_server(debug=True)
