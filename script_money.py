# %% [markdown]
# # ***Import Libraries***

# %%
import pandas as pd
import numpy as np

import plotly.express as px
import matplotlib.pyplot as plt
import seaborn as sns
import plotly.graph_objects as go

from source.engine.FTP_engine import FTPModel as FTP

# %%
from IPython.core.display import display, HTML
display(HTML("<style>.container { width:100% !important; }</style>"))
pd.set_option('display.expand_frame_repr', False)
pd.set_option('display.max_columns', 500)
import matplotlib as mpl
mpl.rcParams['agg.path.chunksize'] = 100000

# %% [markdown]
# # ***Load Data***

# %% [markdown]
# ## ***Data Modelo***

# %%
path = "outputs/"
file = "output_sugerido_39.xlsx"

df = pd.read_excel(path + file, converters={'id_embarcacion':str})

name_fecha = 'fecha_produccion'
df.loc[:, name_fecha] = pd.to_datetime(
            df.loc[:, name_fecha], format="%Y/%m/%d"
        )
# df['y-m'] = df[name_fecha].apply(lambda x: '{0}-{1}'.format(x.year, x.isocalendar()[1]))

name_tempo = 'temporada'


df[name_tempo] =  pd.Categorical(
            df[name_tempo],
            ['2018-I', '2018-II', '2019-I', '2019-II', '2020-I', '2020-II', '2021-I', '2021-II'],
        )

df.loc[df['planta'].isna(), 'planta'] = 'Planta NA'

df.head()

# %% [markdown]
# ## ***Flags***

# %%
# Flag tiene planta sugerida
mask_plant_notna = (df['planta_sugerida'].notna())

# Flag días de pesca adicionales
mask_adicional = (df['flag_dia_adicional'] == 1)

# Flag barcos de TASA
mask_prop = (df['Ind Propiedad'] == 'Propia')

# Flag término de temporada con días de pesca adicional
mask_fin_tempor = (df['flag_termino'] == 1)

mask_model = mask_plant_notna & ~mask_fin_tempor

# %% [markdown]
# ### ***Modelo***

# %% [markdown]
# ## ***Consideraciones***
# 
# 1. Aparecen Nan debido a los registros creados de pesca adicional (no tienen porqué hacer match ya que fueron creados.)
#    
# 2. Aparecen 'MATARANI' y 'ATICO' debido a que los registros creados de pesca adicional hacen match con pesca real que se tuvo en otras plantas.
# 

# %% [markdown]
# ## ***Volumen global Real (TASA + Terceros)***

# %%
vol_all = pd.pivot_table(df[~mask_adicional].groupby(['temporada', 'planta'], as_index=False)['volumen'].sum(), 
columns='temporada', values='volumen', index='planta' )
vol_all

# %% [markdown]
# ## ***Volumen TASA Real (propios)***

# %%
vol_tasa_real = pd.pivot_table(df[mask_prop & ~mask_adicional].groupby(['temporada', 'planta'], as_index=False, dropna=False)['volumen'].sum(), 
columns='temporada', values='volumen', index='planta')

temp = vol_tasa_real.head(1).copy()
temp.index = ['Planta NA']
temp.loc[:, :] = 0.0
vol_tasa_real = pd.concat([vol_tasa_real, temp], ignore_index=False)

vol_tasa_real

# %%
vol_tasa_real.sum(axis=0)

# %% [markdown]
# ## ***Volumen Real Terceros***

# %%
vol_terceros = pd.pivot_table(df[(~mask_prop) & (~mask_adicional)].groupby(['temporada', 'planta'], as_index=False)['volumen'].sum(), 
columns='temporada', values='volumen', index='planta')
vol_terceros

# %%
vol_terceros.sum(axis=0)

# %% [markdown]
# ## ***Volumen Modelo (All)***

# %%
vol_model = pd.pivot_table(df[mask_model].groupby(['temporada', 'planta'], as_index=False, dropna=False)['volumen'].sum(), 
columns='temporada', values='volumen', index='planta', dropna=False, aggfunc=np.sum)
vol_model

# %%
vol_model.sum(axis=0)

# %% [markdown]
# ## ***Volumen TASA Modelo***

# %%
vol_model_tasa = pd.pivot_table(df[mask_model & mask_prop].groupby(['temporada', 'planta'], as_index=False, dropna=False)['volumen'].sum(), 
columns='temporada', values='volumen', index='planta', dropna=False, aggfunc=np.sum)
vol_model_tasa

# %% [markdown]
# ## ***Diferencia Tasa VS Model***

# %%
(vol_tasa_real - vol_model_tasa).sum(axis=0)

# %% [markdown] TODO
# <center><font face="Times" size="10px" color="#FFFFF0">Conversión del Modelo a Dinero</font></center>

# %% [markdown]
# ## ***MP Descargada Propios***

# %%
mp_descargado_propios = pd.pivot_table(df[mask_model & mask_prop].groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['volumen'].sum(), 
columns='temporada', values='volumen', index='planta_sugerida', dropna=False, aggfunc=np.sum)
mp_descargado_propios

# %% [markdown]
# ## ***MP Descargada Terceros***

# %%
mp_descargado_terceros = pd.pivot_table(df[mask_model & ~mask_prop].groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['volumen'].sum(), 
columns='temporada', values='volumen', index='planta_sugerida', dropna=False, aggfunc=np.sum)
mp_descargado_terceros

# %% [markdown]
# ## ***MP Descargada Total***

# %%
mp_descargado_total = pd.pivot_table(df[mask_model].groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['volumen'].sum(), 
columns='temporada', values='volumen', index='planta_sugerida', dropna=False, aggfunc=np.sum)

mp_descargado_total

# %% [markdown]
# ### ***Input planta***

# %%
path = 'money/'
file = 'input_planta.xlsx'

df_money = pd.read_excel(path + file)

to_replace = {
    '01. Malabrigo':'Malabrigo',
    '02. Chimbote':'Chimbote',
    '03. Samanco':'Samanco',
    '04. Supe':'Supe',
    '05. Vegueta':'Vegueta',
    '06. Callao':'Callao',
    '08. Pisco Sur':'Pisco S',
}

df_money.replace({'Planta':to_replace}, inplace=True)

df_money.rename(columns={'Temporada':'temporada', 'Planta':'planta_sugerida'}, inplace=True)

to_replace = {
    '2018 - I':'2018-I',
    '2018 - II':'2018-II',
    '2019 - I':'2019-I',
    '2019 - II':'2019-II',
    '2020 - I':'2020-I',
    '2020 - II':'2020-II',
    '2021 - I':'2021-I',
    '2021 - II':'2021-II',
}

df_money.replace({'temporada':to_replace}, inplace=True)

df_money.head()

# %% [markdown]
# ## ***Rendimiento HP***

# %%
df_rend_hp = pd.pivot_table(df_money.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Rend. HP'].sum(), 
columns='temporada', values='Rend. HP', index='planta_sugerida', dropna=False, aggfunc=np.sum)
df_rend_hp

# %% [markdown]
# ## ***Rendimiento AP***

# %%
df_rend_ap = pd.pivot_table(df_money.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Rend. AP'].sum(), 
columns='temporada', values='Rend. AP', index='planta_sugerida', dropna=False, aggfunc=np.sum)
df_rend_ap

# %% [markdown]
# ## ***Producción HP***

# %%
produccion_hp = mp_descargado_total / df_rend_hp
produccion_hp

# %% [markdown]
# ## ***Producción AP***

# %%
produccion_ap = mp_descargado_total / df_rend_ap
produccion_ap

# %% [markdown]
# ## ***Calidad SP***

# %%
df_money.head()

# %%
df_money[:1]

# %%
df_calidad_sp = pd.pivot_table(df_money.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Calidad SP (%)'].sum(), 
columns='temporada', values='Calidad SP (%)', index='planta_sugerida', dropna=False, aggfunc=np.sum)
df_calidad_sp

# %% [markdown]
# ## ***Calidad Prime***

# %%
df_calidad_pr = pd.pivot_table(df_money.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Calidad P (%)'].sum(), 
columns='temporada', values='Calidad P (%)', index='planta_sugerida', dropna=False, aggfunc=np.sum)
df_calidad_pr

# %% [markdown]
# ## ***Calidad Taiwan***

# %%
df_calidad_taiwan = pd.pivot_table(df_money.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Calidad TW (%)'].sum(), 
columns='temporada', values='Calidad TW (%)', index='planta_sugerida', dropna=False, aggfunc=np.sum)
df_calidad_taiwan

# %% [markdown]
# ## ***Calidad Thailand***

# %%
df_calidad_thailand = pd.pivot_table(df_money.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Calidad TH (%)'].sum(), 
columns='temporada', values='Calidad TH (%)', index='planta_sugerida', dropna=False, aggfunc=np.sum)
df_calidad_thailand

# %% [markdown]
# ## ***Calidad Standar***

# %%
df_calidad_std = pd.pivot_table(df_money.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Calidad STD (%)'].sum(), 
columns='temporada', values='Calidad STD (%)', index='planta_sugerida', dropna=False, aggfunc=np.sum)
df_calidad_std

# %% [markdown]
# ## ***Calidad Aqua***

# %%
df_calidad_aqua = pd.pivot_table(df_money.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Calidad Aqua (%)'].sum(), 
columns='temporada', values='Calidad Aqua (%)', index='planta_sugerida', dropna=False, aggfunc=np.sum)
df_calidad_aqua

# %% [markdown]
# ## ***Calidad Omega***

# %%
df_calidad_omega = pd.pivot_table(df_money.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Calidad Omega (%)'].sum(), 
columns='temporada', values='Calidad Omega (%)', index='planta_sugerida', dropna=False, aggfunc=np.sum)
df_calidad_omega

# %% [markdown]
# ## ***Precio SP***

# %%
df_precio_sp = pd.pivot_table(df_money.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Precio SP'].sum(), 
columns='temporada', values='Precio SP', index='planta_sugerida', dropna=False, aggfunc=np.sum)
df_precio_sp

# %% [markdown]
# ## ***Precio Prime***

# %%
df_precio_pr = pd.pivot_table(df_money.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Precio P'].sum(), 
columns='temporada', values='Precio P', index='planta_sugerida', dropna=False, aggfunc=np.sum)
df_precio_pr

# %% [markdown]
# ## ***Precio Taiwan***

# %%
df_precio_taiwan = pd.pivot_table(df_money.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Precio TW'].sum(), 
columns='temporada', values='Precio TW', index='planta_sugerida', dropna=False, aggfunc=np.sum)
df_precio_taiwan

# %% [markdown]
# ## ***Precio Thailand***

# %%
df_precio_thailand = pd.pivot_table(df_money.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Precio TH'].sum(), 
columns='temporada', values='Precio TH', index='planta_sugerida', dropna=False, aggfunc=np.sum)
df_precio_thailand

# %% [markdown]
# ## ***Precio Standar***

# %%
df_precio_std = pd.pivot_table(df_money.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Precio STD'].sum(), 
columns='temporada', values='Precio STD', index='planta_sugerida', dropna=False, aggfunc=np.sum)
df_precio_std

# %% [markdown]
# ## ***Precio Aqua***

# %%
df_precio_aqua = pd.pivot_table(df_money.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Precio Aqua'].sum(), 
columns='temporada', values='Precio Aqua', index='planta_sugerida', dropna=False, aggfunc=np.sum)
df_precio_aqua

# %% [markdown]
# ## ***Precio Omega***

# %%
df_precio_omega = pd.pivot_table(df_money.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Precio Omega'].sum(), 
columns='temporada', values='Precio Omega', index='planta_sugerida', dropna=False, aggfunc=np.sum)
df_precio_omega

# %% [markdown]
# ## ***Precio HP***

# %%
precio_hp = df_calidad_sp * df_precio_sp + df_calidad_pr * df_precio_pr + df_calidad_taiwan * df_precio_taiwan + df_calidad_thailand * df_precio_thailand + df_calidad_std * df_precio_std
precio_hp

# %% [markdown]
# ## ***Precio AP***

# %%
precio_ap = df_calidad_aqua * df_precio_aqua + df_calidad_omega * df_precio_omega
precio_ap

# %% [markdown]
# ## ***Costo Variable Unitario HP***

# %%
df_costo_var_unit_hp = pd.pivot_table(df_money.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Costo Variable Unitario HP'].sum(), 
columns='temporada', values='Costo Variable Unitario HP', index='planta_sugerida', dropna=False, aggfunc=np.sum)
df_costo_var_unit_hp

# %% [markdown]
# ## ***Costo Extracción Unitario***

# %%
df_costo_extra_unit = pd.pivot_table(df_money.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Costo Extraccion Unitario'].sum(), 
columns='temporada', values='Costo Extraccion Unitario', index='planta_sugerida', dropna=False, aggfunc=np.sum)
df_costo_extra_unit

# %% [markdown]
# ## ***Costos Terceros Unitario***

# %%
df_costo_terc_unit = pd.pivot_table(df_money.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['Costo Terceros Unitario'].sum(), 
columns='temporada', values='Costo Terceros Unitario', index='planta_sugerida', dropna=False, aggfunc=np.sum)
df_costo_terc_unit

# %% [markdown]
# ## ***Ingresos***

# %%
df_ingresos = produccion_hp * precio_hp + produccion_ap * precio_ap
df_ingresos

# %% [markdown]
# ## ***Costos Fijos***

# %%
df_money['costos_fijos'] = df_money['Liquid. Mantto'] + df_money['Nóminas'] + df_money['Ppto - Admin']

df_costo_fijos = pd.pivot_table(df_money.groupby(['temporada', 'planta_sugerida'], as_index=False, dropna=False)['costos_fijos'].sum(), 
columns='temporada', values='costos_fijos', index='planta_sugerida', dropna=False, aggfunc=np.sum)
df_costo_fijos

# %% [markdown]
# ## ***Costos Variables***

# %%
df_cost_var = produccion_hp * df_costo_var_unit_hp
df_cost_var

# %% [markdown]
# ## ***Costos MP Extracción***

# %%
df_costos_mp_extrac = mp_descargado_propios * df_costo_extra_unit
df_costos_mp_extrac

# %% [markdown]
# ## ***Costos MP Terceros***

# %%
df_costos_mp_terc = mp_descargado_terceros * df_costo_terc_unit
df_costos_mp_terc

# %% [markdown]
# ## ***EBIT***

# %%
df_ebit = df_ingresos - (df_costo_fijos + df_cost_var + df_costos_mp_extrac + df_costos_mp_terc)
df_ebit

# %%
df_ebit.sum()

# %%


# %%


# %%

data = df[~mask_fin_tempor & ~mask_plant_notna]
# formatter = FuncFormatter(thousands)
fig, ax = plt.subplots(2, 4, figsize=(26, 20))
values = ['2018-I', '2018-II', '2019-I', '2019-II', '2020-I', '2020-II', '2021-I', '2021-II']
for i in range(2):
    for j in range(4):
        k = 2*i + j
        sns.barplot(ax=ax[i, j], data=data[data['temporada'] == values[k]], x='sort_tipo_embarc', y='volumen', estimator=sum, ci=None)
        # ax[i, j].yaxis.set_major_formatter(formatter)
        ax[i, j].set_title(values[k])
        # ax[i, j].bar_label(ax.containers[0])
        # ax[i, j].xticks(rotation=45)
        ax[i, j].tick_params(axis='x', labelrotation=45)
        ax[i, j].set_xlabel('Tipo de Embarcación')     

# fig.set_xticklabels(rotation=45)   
plt.show()

# %%
df.columns

# %%
cols = ['temporada', 'planta']

mask = (df['planta_sugerida'].isna())
df_grouped = df[mask].groupby(cols, as_index=False)['volumen'].sum()

fig = px.histogram(df_grouped, x="temporada", y="volumen",
             color='planta', barmode='group',
            #  histfunc='avg',
             height=400)
fig.update_layout(
    template='plotly_dark'
)
fig.show()

# %%
cols = ['temporada', 'planta']

mask = (df['planta_sugerida'].isna())
df_grouped = df[mask].groupby(cols, as_index=False)['volumen'].sum()

fig = px.histogram(df_grouped, x="temporada", y="volumen",
             color='planta', barmode='group',
            #  histfunc='avg',
             height=400)
fig.update_layout(
    template='plotly_dark'
)
fig.show()

# %%
cols = ['temporada', 'planta']

mask = (df['planta_sugerida'].isna())
df_grouped = df[mask].groupby(cols, as_index=False)['volumen'].sum()

fig = px.histogram(df_grouped, x="temporada", y="volumen",
             color='planta', barmode='group',
            #  histfunc='avg',
             height=400)
fig.update_layout(
    template='plotly_dark'
)
fig.show()

# %%
cols = ['temporada', 'Zona de Pesca']

mask = (df['planta_sugerida'].isna())
df_grouped = df[mask].groupby(cols, as_index=False)['volumen'].sum()

fig = px.histogram(df_grouped, x="temporada", y="volumen",
             color='Zona de Pesca', barmode='group',
            #  histfunc='avg',
             height=400)
fig.show()

# %%


# %%
fig = px.bar(df, x="planta", y="volumen", color="planta",
  animation_frame="y-m", animation_group="Zona de Pesca", )
fig.show()

# %%
mask = df['Tipo Tr'] == 'Terceros'
all_vol = df[~mask].groupby(['temporada'])['volumen'].sum()
all_vol

# %%
mask = df['planta_sugerida'].isna()
togo_vol = df[mask].groupby(['temporada'])['volumen'].sum()
togo_vol

# %%
(togo_vol / all_vol) * 100

# %%
df[~mask].groupby(['fecha_produccion'])['volumen'].sum()

# %%
6250 / 227

# %%
(6250 / 469620.26) * 100

# %%
df['rend'].min()

# %%
df.groupby(['y-m', 'Embarc', 'Zona de Pesca'], as_index=False).agg(
    volumen=('volumen', 'sum'),
    cantidad=('id', 'count'),
    rendimiento_prom=('rend', 'mean')
)

# %%
data = df[df['temporada'] == '2018-I']

fig = px.scatter(data, x="CBOD", y="volumen", animation_frame="y-m", animation_group="Embarc",
           size="cant", color="Zona de Pesca", #hover_name="country",
        #    log_x=True, 
           size_max=55, 
        #    range_x=[100,100000], range_y=[25,90]
           )

fig.show()

# %%
dff = px.data.gapminder()
fig = px.scatter(dff, x="gdpPercap", y="lifeExp", animation_frame="year", animation_group="country",
           size="pop", color="continent", hover_name="country",
           log_x=True, size_max=55, range_x=[100,100000], range_y=[25,90])
fig.show()

# %%
df.head()

# %%
df.groupby(['Zona de Pesca', 'sort_tipo_embarc','y-m'], as_index=False).agg(
    volumen_prom=('volumen', 'mean'),
    volumen_tot=('volumen', 'sum'),
    cantidad=('Embarc', 'count'),
    rendimiento_prom=('rend', 'mean')
)

# %%
dff.head()

# %%
df[name_tempo].value_counts(dropna=False)

# %%
name_dif_min = 'diferencias'
df[name_dif_min].describe(percentiles=np.arange(0, 1, 0.01))

# %%
mask = df['planta_sugerida'].notna()
df.loc[mask, 'volumen'].sum()

# %%
757 / len(df.index)

# %%
path = 'outputs/'

file = 'data_test_last_cala.csv'

df_adic = pd.read_csv(path + file)
df_adic.head()

# %%
df_adic['dias_faltantes'] = df_adic['dias_faltantes'].str[0:2].astype(int)
df_adic.head()

# %%
from matplotlib.pyplot import figure

figure(figsize=(30, 10))

sns.histplot(data=df_adic, x='dias_faltantes', bins=20)
plt.show()

# %%
from matplotlib.pyplot import figure

figure(figsize=(30, 10))

sns.kdeplot(data=df_adic, x='dias_faltantes', hue='zona_de_pesca_id', palette="Spectral", fill=True, common_norm=False)
plt.show()

# %%



