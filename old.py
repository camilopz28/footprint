# --------------------------------------------------------------------------------------
from ast import arguments
from cgi import print_arguments
from black import out
import pandas as pd
import numpy as np
from tqdm import tqdm
# from pyrsistent import T
import copy

from source.engine.FTP_engine import FTPModel as FTP
from source.engine import utils as ut
from datetime import datetime

from sklearn.utils.extmath import cartesian
tqdm.pandas(desc=r'% de avance')
# df_scenarios = pd.read_excel('datasets/escenarios/df_escenarios_automaticos.xlsx').iloc[[0]]#, 1, 2, 3]]#.iloc[[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]]#.iloc[[0, 1, 2, 3, 4, 5]]

df_scenarios = pd.read_parquet("datasets/df_combis_total.parquet").loc[[1, 0]]
# df_scenarios["cap_chimbote"] = 1
# df_scenarios["cap_supe"] = 1
# df_scenarios["cap_vegueta"] = 1
# df_scenarios["cap_samanco"] = 1
# df_scenarios["num_embar"] = [[None, None, None, None]]

print(df_scenarios)
# 6. IMPORTACIÓN DEL CEREBRO
# --------------------------------------------------------------------------------------

ftp_model = ut.read_pickle('models/FTP_MODEL_VERSION_11.pkl')

def run_ftp(path):
    ftp_model = ut.read_pickle(path)
    return ftp_model.execute_running

# vect_run = np.vectorize(ftp_model.run)
def aplicar_modelo_a_fila(fila):
    # ftp_model = ut.read_pickle('models/FTP_MODEL_VERSION_11.pkl')
    ftp_model_current = copy.deepcopy(ftp_model)
    resultado = ftp_model_current.execute_running(fila)
    return resultado

# 7. RUNNING
# --------------------------------------------------------------------------------------

start_time = datetime.now()
# kpis = [] # CHECK
# for index, row in df_scenarios.iterrows():
#     print('Escenario', index)
#     ftpt_model = ut.read_pickle('models/FTP_MODEL_VERSION_11.pkl')
#     kpis.append(ftpt_model.execute_running(row))

kpis = df_scenarios.apply(aplicar_modelo_a_fila, axis=1)
# kpis = df_scenarios.progress_apply(lambda x: ftp_model.run(x['num_embar'], x['num_eps_frio'], x['flg_45'], x['estrategia'], x['cap_malabrigo'], x['cap_chimbote'], x['cap_samanco'], x['cap_supe'], x['cap_vegueta'], x['cap_callao'], x['cap_pisco_s'], x['flg_barco_nuevo']), axis=1)

print(kpis)
# kpis = df_scenarios.apply(run_ftp('models/FTP_MODEL_VERSION_11.pkl'), axis=1)

# kpis = list(map(ftp_model.run, df_scenarios['num_embar'], df_scenarios['num_eps_frio'], df_scenarios['flg_45'], df_scenarios['estrategia'], df_scenarios['cap_malabrigo'], df_scenarios['cap_chimbote'], df_scenarios['cap_samanco'], df_scenarios['cap_supe'], df_scenarios['cap_vegueta'], df_scenarios['cap_callao'], df_scenarios['cap_pisco_s'], df_scenarios['flg_barco_nuevo']))
# kpis = list(vect_run(df_scenarios['num_embar'], df_scenarios['num_eps_frio'], df_scenarios['flg_45'], df_scenarios['estrategia'], df_scenarios['cap_malabrigo'], df_scenarios['cap_chimbote'], df_scenarios['cap_samanco'], df_scenarios['cap_supe'], df_scenarios['cap_vegueta'], df_scenarios['cap_callao'], df_scenarios['cap_pisco_s'], df_scenarios['flg_barco_nuevo']))

# kpis = []

now = datetime.now()
time_elapsed = now - start_time
print(f'Time elapsed (hh:mm:ss.ms) {time_elapsed}')

tuples_kpis = [i for i in kpis]
df_kpis = pd.DataFrame(tuples_kpis, columns=['names', 'ingresos', 'mtto_omision', 'ebit_pv', 'vr_pv', 'vr_year4', 'produccion_hp', 'produccion_ap', 'CF', 'CV', 'costos_mp_extrac', 'costos_mp_terc', 'mtto_frio', 'dd', 'capex_total', 'gastos_cierre', 'cf_prom', 'market_share', 'market_terceros', 'cumpli_cuota_propia', 'mp_descargado_propios', 'mp_descargado_terceros', 'npv', 'cash_flow_pv', 'cf_vr_pv', 'depreciacion', 'capex', 'nueva_depreciacion', 'cant_dias_adicionales', 'vol_total_adicional', 'ebit_prom'])

df_kpis.index = df_scenarios.index
df_scenarios = pd.merge(df_scenarios, df_kpis, left_index=True, right_index=True)

df_scenarios.sort_values('npv', inplace=True, ascending=False)

df_scenarios['ID_ESCENARIO'] = np.arange(0, len(df_scenarios.index), 1) + 1

# 8. EXPORTACIÓN DE RESULTADOS
# --------------------------------------------------------------------------------------

print(df_kpis)
df_scenarios.to_excel(f'outputs/Resumen_Escenarios_{now.strftime("%Y_%m_%d-%I_%M_%S_%p")}_validation.xlsx')