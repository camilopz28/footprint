import pandas as pd

from source.engine import utils as ut

from datetime import datetime

path = 'datasets/'

# Data de embarcaciones (descargas)
file = 'df_embar_new.csv'
df_embar = pd.read_csv(path + file, sep=';')

# Data de prioridad de embarcaciones
file = 'df_prio_embar.csv'
df_prio_embar = pd.read_csv(path + file, sep=';')

# Data de prioridad de destinos
file = 'df_prio_dest.csv'
df_prio_dest = pd.read_csv(path + file, sep=';')

# Data de capacidad de planta
file = 'df_cap_plant.csv'
df_cap_plant = pd.read_csv(path + file, sep=';')

ftp_model = ut.read_pickle('models/FTPModel.pkl')

start_time = datetime.now()
df_embar_flagged, df_update_planta = ftp_model.predict(
    df_embar=df_embar,
    df_prio_embar=df_prio_embar,
    df_prio_dest=df_prio_dest,
    df_cap_plant=df_cap_plant,
)

time_elapsed = datetime.now() - start_time  
print(f'Time elapsed (hh:mm:ss.ms) {time_elapsed}')
print("Proceso finalizado.")

df_embar_flagged.to_csv('output_sugerido.csv', index=False)
df_update_planta.to_csv('update_planta.csv', index=False)