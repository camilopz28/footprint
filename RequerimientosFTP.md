# ***Flow Footprint***

1. Identificar de la primera embarcación del día.
2. Identificar del tipo de embarcación y la zona de su primera cala.
3. Encontrar de las prioridades de planta de acuerdo a la zona de su primera cala.
4. Definir si la planta de primera prioridad puede recibirlo.
    * 4.1 Si -> Descargo 
    * 4.2 No- > Siguiente prioridad
5. Actualizar capacidad disponible de las plantas (Capacidad máxima del día - Total descarga hasta el momento)
6. Continuar con el siguiente barco y repetir el proceso hasta terminar todos los barcos del día.
7. Actualizar la capacidad máxima de plantas para el día siguiente.
8. Continuar con el primer barco del día siguiente y repetir el proceso.

## ***Consideraciones***

1. Ir día por día
2. Suponer que todas las plantas arrancan con cero de descarga.
3. En vez de la hora voy por el orden de prioridad.


# ***Update***

Para los días de descanso:
1. Primer día 14
2. Segundo día 18
3. Tercer día 19