# 0. Libraries
# --------------------------------------------------------------------------------------
from ast import arguments
from cgi import print_arguments
from black import out
import pandas as pd
import numpy as np
from tqdm import tqdm
# from pyrsistent import T

from source.engine.FTP_engine import FTPModel as FTP
from source.engine import utils as ut
from datetime import datetime

from sklearn.utils.extmath import cartesian

# %load_ext Cython


# 1. DATA INPUT
# --------------------------------------------------------------------------------------
path = 'datasets/'


# 2. DATA PARA ESCENARIOS
# --------------------------------------------------------------------------------------
path = "datasets/escenarios/"


# 3. DATA VARIABLE
# --------------------------------------------------------------------------------------

# 3.1. Número de EPS (Flota TASA) --> 48, 47, ..., 42
file = "df_num_embar.csv"
df_num_embar = pd.read_csv(path + file, sep=";")

# 3.2. Número de EPS con Frío (float con frío) --> 19, 20, 21, ..., 23
file = "df_num_eps_frio.csv"
df_num_eps_frio = pd.read_csv(path + file, sep=";")

# 3.3. Flag TASA 45
FLG_45 = [0]

# 3.4. Escenario Estrategia de Terceros (A, B, ..., F)
file = "df_scenario_estrategia_terceros.csv"
df_scenario_estrategia_terceros = pd.read_csv(path + file, sep=";")

# 3.5. Capacidades para probar (por planta)
path = "money/"
file = "df_new_capacidades.xlsx"
df_scenario_cap = pd.read_excel(path + file)

# 4. TABLA PARA CONVERSION A DINERO
# --------------------------------------------------------------------------------------

path = 'money/'
file = 'input_planta.xlsx'



# 5. TABLA CON TODOS LOS ESCENARIOS
# --------------------------------------------------------------------------------------


lst_vel_malabrigo = (
    df_scenario_cap.loc[df_scenario_cap["Malabrigo"].notna(), "Malabrigo"]
    .unique()
    .tolist()
)
lst_vel_chimbote = (
    df_scenario_cap.loc[df_scenario_cap["Chimbote"].notna(), "Chimbote"]
    .unique()
    .tolist()
)
lst_vel_samanco = (
    df_scenario_cap.loc[df_scenario_cap["Samanco"].notna(), "Samanco"].unique().tolist()
)
lst_vel_supe = (
    df_scenario_cap.loc[df_scenario_cap["Supe"].notna(), "Supe"].unique().tolist()
)
lst_vel_vegueta = (
    df_scenario_cap.loc[df_scenario_cap["Vegueta"].notna(), "Vegueta"].unique().tolist()
)
lst_vel_callao = (
    df_scenario_cap.loc[df_scenario_cap["Callao"].notna(), "Callao"].unique().tolist()
)
lst_vel_pisco_s = (
    df_scenario_cap.loc[df_scenario_cap["Pisco S"].notna(), "Pisco S"].unique().tolist()
)

df_all_vel = pd.DataFrame(
    cartesian(
        (
            lst_vel_malabrigo,
            lst_vel_chimbote,
            lst_vel_samanco,
            lst_vel_supe,
            lst_vel_vegueta,
            lst_vel_callao,
            lst_vel_pisco_s,
        )
    ),
    columns=[
        "cap_malabrigo",
        "cap_chimbote",
        "cap_samanco",
        "cap_supe",
        "cap_vegueta",
        "cap_callao",
        "cap_pisco_s",
    ],
)

df_scenarios = df_all_vel.copy()

lst_estrat_terce = (
    df_scenario_estrategia_terceros["Estrategia Terceros"].unique().tolist()
)

# Estrategia Terceros
df_scenarios = pd.concat(
        [df_scenarios] * (len(lst_estrat_terce)), ignore_index=True
    )
df_scenarios["estrategia"] = np.repeat(
        lst_estrat_terce, len(df_scenarios.index) / len(lst_estrat_terce)
    )

# Cantidad de embarcaciones de TASA
df_scenarios = pd.concat([df_scenarios] * (len(df_num_embar.index)), ignore_index=True)
df_scenarios["num_embar"] = np.repeat(df_num_embar['Cantidad'].to_numpy(), len(df_scenarios.index) / len(df_num_embar.index))

# Embarcaciones para conversión a frío
df_scenarios = pd.concat([df_scenarios] * (len(df_num_eps_frio.index)), ignore_index=True)
df_scenarios["num_eps_frio"] = np.repeat(
    df_num_eps_frio['Cantidad'].to_numpy(), len(df_scenarios.index) / len(df_num_eps_frio.index)
)

# TASA 45
df_scenarios = pd.concat([df_scenarios] * (len(FLG_45)), ignore_index=True)
df_scenarios["flg_45"] = np.repeat(
    FLG_45, len(df_scenarios.index) / len(FLG_45)
)

# TODO: Aquí modificar
tqdm.pandas(desc=r'% de avance')
df_scenarios = df_scenarios.iloc[[
0,
1,
29,
43,
50,
2066,
4082,
6098,
8114,
10130
]]
df_scenarios = pd.read_excel('datasets/escenarios/df_escenarios_manuales.xlsx').iloc[[1]]

# 6. IMPORTACIÓN DEL CEREBRO
# --------------------------------------------------------------------------------------

ftp_model = ut.read_pickle('models/FTPModel_in.pkl')


# 7. RUNNING
# --------------------------------------------------------------------------------------

start_time = datetime.now()

kpis = df_scenarios.progress_apply(lambda x: ftp_model.run_year_cf(x['num_embar'], x['num_eps_frio'], x['flg_45'], x['estrategia'], x['cap_malabrigo'], x['cap_chimbote'], x['cap_samanco'], x['cap_supe'], x['cap_vegueta'], x['cap_callao'], x['cap_pisco_s']), axis=1)

now = datetime.now()
time_elapsed = now - start_time
print(f'Time elapsed (hh:mm:ss.ms) {time_elapsed}')

tuples_kpis = [i for i in kpis]
df_kpis = pd.DataFrame(tuples_kpis, columns=['ebit_nuevo', 'cash_flow', 'depreciacion', 'nueva_depreciacion', 'capex', 'ingresos', 'ahorro_omision', 'produccion_hp', 'produccion_ap', 'ingresos_hp', 'ingresos_ap', 'costo_fijos', 'cost_var', 'costos_mp_extrac', 'costos_mp_terc', 'mtto_frio', 'mp_descargado_propios', 'mp_descargado_terceros'])

df_kpis.index = df_scenarios.index

cols = ['ebit_nuevo', 'cash_flow', 'depreciacion', 'nueva_depreciacion', 'capex', 'ingresos', 'ahorro_omision', 'produccion_hp', 'produccion_ap', 'ingresos_hp', 'ingresos_ap', 'costo_fijos', 'cost_var', 'costos_mp_extrac', 'costos_mp_terc', 'mp_descargado_propios', 'mp_descargado_terceros']

df_kpis = df_kpis.explode(cols)
df_kpis['temporada'] = np.tile(ftp_model.temporadas_elegidas, len(np.unique(df_kpis.index)))

to_replace = {
    '2018-I':'2018',
    '2018-II':'2018',
    '2019-I':'2019',
    '2019-II':'2019',
    '2020-I':'2020',
    '2020-II':'2020',
    '2021-I':'2021',
    '2021-II':'2021'
}
df_kpis.replace({'temporada':to_replace}, inplace=True)

df_kpis.rename(columns={'temporada':'year'}, inplace=True)

df_kpis.reset_index(inplace=True)
df_kpis.rename(columns={'index':'indice'}, inplace=True)
df_kpis = df_kpis.groupby(
    ['indice', 'year'], as_index=False
).agg(
    ebit_nuevo=('ebit_nuevo', 'sum'),
    cash_flow=('cash_flow', 'sum'),
    depreciacion=('depreciacion', 'sum'),
    nueva_depreciacion=('nueva_depreciacion', 'sum'),
    capex=('capex', 'sum'),
    ingresos=('ingresos', 'sum'),
    ahorro_omision=('ahorro_omision', 'sum'),
    produccion_hp=('produccion_hp', 'sum'),
    produccion_ap=('produccion_ap', 'sum'),
    ingresos_hp=('ingresos_hp', 'sum'),
    ingresos_ap=('ingresos_ap', 'sum'),
    costo_fijos=('costo_fijos', 'sum'),
    cost_var=('cost_var', 'sum'),
    costos_mp_extrac=('costos_mp_extrac', 'sum'),
    costos_mp_terc=('costos_mp_terc', 'sum'),
    mp_descargado_propios=('mp_descargado_propios', 'sum'),
    mp_descargado_terceros=('mp_descargado_terceros', 'sum')
)
df_scenarios = pd.merge(df_scenarios, df_kpis, left_index=True, right_on='indice')

# df_scenarios.sort_values('npv', inplace=True, ascending=False)

df_scenarios['ID_ESCENARIO'] = np.repeat(np.arange(0, len(np.unique(df_kpis['indice']))) + 1, 4)
# 8. EXPORTACIÓN DE RESULTADOS
# --------------------------------------------------------------------------------------

print(df_kpis)
df_scenarios.to_excel(f'outputs/Resumen_Escenarios_{now.strftime("%Y_%m_%d-%I_%M_%S_%p")}_validation.xlsx')